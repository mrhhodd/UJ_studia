/*
 * Experiment.cpp
 *
 */

#include<stdlib.h>
#include<iostream>

#include "Experiment.h"
#include "Distribution.h"

#define DEBUG_ON_

using namespace std;

Experiment::Experiment(int balls, int drawsNumber) {
	this->firstRun = false;
	this->localExperiments = -1;
	this->balls = balls;
	this->drawsNumber = drawsNumber;
// najgorszy scenariusz - losowanie ze zwracaniem i 
// wylosowano tylko kule o najwyzszym numerze.
	hmax = this->drawsNumber * (balls-1);

	histogram = new long[hmax];
	tmpHistogram = new long[hmax];

	used = new bool[balls];
	forbidden = new bool[ balls ];
	for ( int i = 0; i < balls; i++ )
		forbidden[ i ] = false;
}

void Experiment::setSamplingWithReplacement() {
	withReplacement = true;
}

void Experiment::setSamplingWithoutReplacement() {
	withReplacement = false;
}

void Experiment::setForbidden( int ball ) {
	#ifdef DEBUG_ON
		cout << "Zablokowano mozliwosc uzywania kuli " << ball << endl;
	#endif
	forbidden[ ball ] = true;
}

void Experiment::setAllowed( int ball ) {
	forbidden[ ball ] = false;
}

void Experiment::clearHistogram() {
	for (long i = 0; i < hmax; i++) {
		//histogram[i] = 0;
		tmpHistogram[i] = 0;
	}

}

void Experiment::allBallsToUrn() {
	if ( withReplacement ) return;
	for (int i = 0; i < balls; i++)
		used[i] = false;
}

void Experiment::ballIsDrawn( int ball ) {
	if ( withReplacement ) return;

	#ifdef DEBUG_ON
			cout << "Kula o numerze " << ball << " nie moze juz byc ponownie wybrana" << endl;
	#endif

	used[ball] = true;
}

bool Experiment::isAllowed( int ball ) {
	if ( forbidden[ ball ] ) return false; // tej kuli nie mozna uzywac

	if ( withReplacement ) return true; // kule sa zwracane, wiec mozna ich ponownie uzywac

	return ! used[ ball ];
}

void Experiment::setMyMPIModule(MyMPI *mpi) {
	this->myMPI = mpi;
	this->myMPI->MPI_Comm_rank( MPI_COMM_WORLD, &process);
	this->myMPI->MPI_Comm_size( MPI_COMM_WORLD, &processes);
}

long Experiment::singleExperimentResult() {
	long sum = 0;
	int ball;
	double p;

	allBallsToUrn();
	for (int i = 0; i < drawsNumber;) {
		ball = (int) (((double) balls * rand()) / ( RAND_MAX + 1.0)); // rand losuje od 0 do RAND_MAX wlacznie

#ifdef DEBUG_ON
		cout << "Propozycja " << ball << endl;
#endif

		if ( ! isAllowed( ball ) ) {
#ifdef DEBUG_ON
		    cout << "Propozycja - ta kula nie moze byc uzyta " << ball << endl;
#endif
			continue; // jeszcze raz losujemy 
		} else {
#ifdef DEBUG_ON
		    cout << "Propozycja - OK " << ball << endl;			
#endif
		}

		p = Distribution::getProbability(i + 1, ball); // pobieramy prawdopodobienstwo 
		// wybrania tej kuli

		if ((rand() / ( RAND_MAX + 1.0)) < p) // akceptacja wyboru kuli z zadanym prawdopodobienstwem
		{
			ballIsDrawn(ball);
			sum += ball; 
			i++;
		}
	}

	return sum;
}

void Experiment::setNumberOfExperiments( long experiments ) {
	this->experiments = experiments;
}

long Experiment::getLocalExperiments(){
    double t1, t2;
	long result = 0;
	long total_result = 0;
    double reversed_weight = 0;
	double sumed_reversed_weight = 0;
    t1 = MPI_Wtime();
	doMultipleExperiments(30);
    t2 = MPI_Wtime();
    double time_local = (t2 - t1);
    double shortest_time = -1;
	this->myMPI->MPI_Barrier(MPI_COMM_WORLD);
    this->myMPI->MPI_Allreduce(&time_local, &shortest_time, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
	
    reversed_weight = (double) 1 / time_local;
		
	this->myMPI->MPI_Barrier(MPI_COMM_WORLD);
    this->myMPI->MPI_Allreduce(&reversed_weight, &sumed_reversed_weight, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
	double ratio = reversed_weight / sumed_reversed_weight;
    result = (long)(experiments * ratio);
		
	this->myMPI->MPI_Barrier(MPI_COMM_WORLD);
    this->myMPI->MPI_Allreduce(&result, &total_result, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);

	int is_fastest[processes];
	int is_fastest_final[processes];
	for (int i = 0; i < processes; i++)
		is_fastest[i] = -1;
    if (time_local == shortest_time) 
		is_fastest[process] = processes;
		
	this->myMPI->MPI_Barrier(MPI_COMM_WORLD);
	this->myMPI->MPI_Allreduce(is_fastest, is_fastest_final, processes, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	for (int i = 0; i < processes; i++){
		if (is_fastest_final[i] > 0 && i == process){
			result += experiments - total_result;
		}
	}
    return result;
}

void Experiment::calc() {
	alignExperiments();
	if (!this->firstRun){
		this->localExperiments = getLocalExperiments();
		srand(localExperiments * process * processes);
		this->firstRun = true;
		doMultipleExperiments(localExperiments - 30);
	}
	else
		doMultipleExperiments(localExperiments);
	
	this->myMPI->MPI_Reduce(tmpHistogram, histogram, hmax, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
}



void Experiment::doMultipleExperiments(long experimentCount){
	for (long l = 0; l < experimentCount; l++) {
		tmpHistogram[singleExperimentResult()]++;
		//histogram[singleExperimentResult()]++;
	}
}

void Experiment::alignExperiments(){
	this->myMPI->MPI_Bcast(&experiments, 1, MPI_LONG, 0, MPI_COMM_WORLD);
	this->myMPI->MPI_Bcast(forbidden, balls, MPI_CHAR, 0, MPI_COMM_WORLD);
	this->myMPI->MPI_Bcast(&withReplacement, 1, MPI_CHAR, 0, MPI_COMM_WORLD);
}

long Experiment::getHistogramSize() {
	return hmax;
}

long *Experiment::getHistogram() {
	return histogram;
}

Experiment::~Experiment() {
	delete[] histogram;
	delete[] used;
	delete[] forbidden;
}
