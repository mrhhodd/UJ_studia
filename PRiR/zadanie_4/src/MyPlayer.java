import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class MyPlayer {
    private long playerID;
    private GameInterface game;
    private Radar radar = new Radar();

    private volatile ConcurrentHashMap<Ship, Integer> myFleet= new ConcurrentHashMap<>();
    private volatile ConcurrentHashMap<GameInterface.Position, Integer> fleetPositions = new ConcurrentHashMap<>();
    private PriorityBlockingQueue<TimedPositionAndCourse> detectedEnemies = new PriorityBlockingQueue<>();

    private ReentrantLock colisionLock = new ReentrantLock();
    private ReentrantLock enemiesDetectionLock = new ReentrantLock();

    MyPlayer(String ip){
        try {
            game = (GameInterface)LocateRegistry.getRegistry(ip).lookup("GAME");
            String PLAYER_NAME = "Mateusz Wnetrzak";
            playerID = game.register(PLAYER_NAME);
            game.waitForStart(playerID);

        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
        this.mapShips();
        this.startPlaying();
    }

    private void mapShips(){
        try {
            int maxNumberOfShips = game.getNumberOfAvaiablewarships(playerID);
            for (int warshipId = 0; warshipId < maxNumberOfShips; warshipId++){
                GameInterface.Position myShipPosition = game.getPosition(playerID, warshipId);
                GameInterface.Course myShipCourse = game.getCourse(playerID, warshipId);
                myFleet.put(new Ship(warshipId, myShipPosition, myShipCourse), warshipId);
                fleetPositions.put(myShipPosition, warshipId);
            }
        } catch (RemoteException e) { e.printStackTrace(); }
    }

    private void startPlaying(){
        LinkedList<Thread> threadsTable = new LinkedList<>();
        Thread radarThread = new Thread(radar);
        myFleet.keySet().forEach(s-> {
            Thread thread = new Thread(s);
            threadsTable.add(thread);
            thread.start();
        });
        radarThread.run();
    }

    class TimedPositionAndCourse implements Comparable<TimedPositionAndCourse>{
        GameInterface.Course course;
        GameInterface.Position position;
        Long time;

        TimedPositionAndCourse(GameInterface.PositionAndCourse positionAndCourse){
            this.course = positionAndCourse.getCourse();
            this.position = positionAndCourse.getPosition();
            this.time = positionAndCourse.getTime();
        }
        @Override
        public int compareTo(TimedPositionAndCourse o) {
            return o.time.compareTo(this.time);
        }
    }

    class Radar implements Runnable{
        GameInterface.PositionAndCourse newDetectedEnemy;

        @Override
        public void run() {
            try {
                while(true) {
                    if (myFleet.keySet().stream().noneMatch(Ship::isAlive))
                        break;
                    newDetectedEnemy = game.getMessage(playerID);
                    enemiesDetectionLock.lock();

                    detectedEnemies.offer(new TimedPositionAndCourse(spreadDetection(newDetectedEnemy)));
                    enemiesDetectionLock.unlock();
                }
            } catch (RemoteException e) {e.printStackTrace();}
        }

        private GameInterface.PositionAndCourse spreadDetection(GameInterface.PositionAndCourse enemy){
            double distanceFromEnemy = getSmallestDistance(enemy.getPosition());
            if (distanceFromEnemy <= game.SPOT_DISTANCE_IN_REST)
                return getTargetBasedOnWeights(enemy, 0.8);
            else if (distanceFromEnemy <= game.SPOT_DISTANCE_IN_MOTION)
                return getTargetBasedOnWeights(enemy, 0.3);
            else
                return getTargetBasedOnWeights(enemy, 0.5);
        }

        private GameInterface.PositionAndCourse getTargetBasedOnWeights(GameInterface.PositionAndCourse enemy,
                                                                        double w1){
            double randomNumber = new Random(enemy.getTime()).nextDouble();
            if (randomNumber <= w1) {
                return enemy;
            }
            else {
                return new GameInterface.PositionAndCourse(enemy.getCourse().next(enemy.getPosition()), enemy.getCourse());
            }
        }

        double getSmallestDistance(GameInterface.Position enemyPosition){
            double smallestDistance = Double.MAX_VALUE;
            for (Ship ship: myFleet.keySet()) {
                if (ship.isAlive() && getDistance(ship.position, enemyPosition) < smallestDistance)
                    smallestDistance = getDistance(ship.position, enemyPosition);
            }
            return smallestDistance;
        }

        double getDistance(GameInterface.Position pos1, GameInterface.Position pos2){
            return Math.sqrt(Math.pow(pos1.getRow() - pos2.getRow(), 2) +
                    Math.pow(pos1.getCol() - pos2.getCol(), 2));
        }
    }

    class Ship implements Runnable{
        TimedPositionAndCourse enemyPosition = null;

        GameInterface.Course enemyDirection = GameInterface.Course.NORTH;
        int id;
        volatile AtomicBoolean isAlive = new AtomicBoolean(true);
        volatile GameInterface.Position position;
        volatile GameInterface.Course course;
        Ship(int id, GameInterface.Position position, GameInterface.Course course){
            this.id = id;
            this.position = position;
            this.course = course;
        }

        boolean isAlive(){ return isAlive.get();}

        @Override
        public void run() {
            try {
                do {
                    enemiesDetectionLock.lock();
                    enemyPosition = detectedEnemies.poll();
                    enemiesDetectionLock.unlock();
                    if (enemyPosition != null && (System.currentTimeMillis() - enemyPosition.time < game.DELAY)) {
                        fireAtEnemy();
                    }
                    else {
                        moveTowardEnemy();
                    }
                } while (this.isAlive());
            } catch (RemoteException e) {
                if (e.getMessage().contains("Okręt zatopiony")) {
                    this.isAlive.set(false);
                }
                else
                    e.printStackTrace();}
        }

        void fireAtEnemy() throws RemoteException{
            if (!this.isAlive())
                return;
            colisionLock.lock();
            boolean willCollide = willCollide(enemyPosition.position);
            colisionLock.unlock();
            if (willCollide)
                return;
            game.fire(playerID, this.id, enemyPosition.position);
            sleep( game.FIRE_DELAY_HALF);
            enemyPosition = null;
            moveAwayAfterShooting();
        }

        void moveAwayAfterShooting() throws RemoteException{
            double randomNumber = new Random(this.id).nextDouble();
            if (randomNumber < 0.6){
                move();
            }
            else if (randomNumber < 0.9){
                turnRandomly();
                move();
            }
            else {
                sleep((long) (game.DELAY));
            }
        }

        void moveTowardEnemy() throws RemoteException{
            switch (this.course){
                case NORTH:
                    if (enemyDirection == GameInterface.Course.NORTH)
                        move();
                    else
                        turnRandomly();
                    break;
                case SOUTH:
                    if (enemyDirection == GameInterface.Course.SOUTH)
                        move();
                    else
                        turnRandomly();
                case WEST:
                    if (enemyDirection == GameInterface.Course.NORTH)
                        turnRight();
                    else
                        turnLeft();
                    break;
                case EAST:
                    if (enemyDirection == GameInterface.Course.SOUTH)
                        turnRight();
                    else
                        turnLeft();
                    break;
            }
        }

        void move() throws RemoteException{
            if (!this.isAlive())
                return;
            colisionLock.lock();
            boolean willCollide = this.willCollide(this.course.next(this.position));
            if ( ! willCollide){
                fleetPositions.put(this.course.next(this.position), this.id);
            }
            colisionLock.unlock();
            if ( ! willCollide) {
                game.move(playerID, this.id);
                fleetPositions.remove(this.position);
                this.position = this.course.next(this.position);
                sleep( game.MOVE_DELAY_HALF);
            }
            else {
                turnRandomly();
            }

        }

        boolean willCollide(GameInterface.Position newPosition) throws RemoteException{
            if (newPosition.getCol() >= game.WIDTH - 1) {
                turnLeft();
                return true;
            }
            if (newPosition.getCol() <= -1) {
                turnRight();
                return true;
            }
            if (newPosition.getRow() >= game.HIGHT) {
                enemyDirection = GameInterface.Course.SOUTH;
                turnRandomly();
                return true;
            }
            if (newPosition.getRow() <= -1) {
                enemyDirection = GameInterface.Course.NORTH;
                turnRandomly();
                return true;
            }
            return isNewPositionToCloseToOtherShips(newPosition);
        }

        private boolean isNewPositionToCloseToOtherShips(GameInterface.Position newPosition){
            boolean result = fleetPositions.keySet().contains(newPosition);
            return result && fleetPositions.get(newPosition) != this.id;
        }

        void turnLeft() throws RemoteException{
            game.turnLeft(playerID, this.id);
            this.course = this.course.afterTurnToLeft();
            sleep( game.TURN_DELAY_HALF);
        }

        void turnRight() throws RemoteException{
            game.turnRight(playerID, this.id);
            this.course = this.course.afterTurnToRight();
            sleep( game.TURN_DELAY_HALF);
        }

        void turnRandomly() throws RemoteException{
            if (!this.isAlive())
                return;
            if (Math.random() < 0.5)
                turnRight();
            else
                turnLeft();
        }

        void sleep(long millis) {
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) { e.printStackTrace(); }
        }
    }
}

class Start {
    public static void main(String[] args){
        new MyPlayer(args[0]);
    }
}