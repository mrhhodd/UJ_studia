import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class ConversionManagement implements ConversionManagementInterface {
    private volatile AtomicInteger nextId = new AtomicInteger(1);
    private volatile int availableCoresCount ;
    private ConversionReceiverInterface conversionReceiver;
    private ConverterInterface converter;
    private PriorityBlockingQueue<RawData> rawDataQueue = new PriorityBlockingQueue<>();
    private ConcurrentHashMap<Integer, RawConversionResult> rawConversionResults = new ConcurrentHashMap<>();
    private ConvertScheduler scheduler;
    private Semaphore maximumThreadsSemaphore = new Semaphore(0, true);
    private Semaphore schedulerSemaphore = new Semaphore(0, true);
    private Semaphore resultSenderSemaphore = new Semaphore(0, true);
    private ReentrantLock saveDataLock = new ReentrantLock();

    private class RawData implements Comparable<RawData>{
        int id;
        ConverterInterface.Channel channelId;

        RawData(ConverterInterface.DataPortionInterface data){
            this.id = data.id();
            this.channelId = data.channel();
        }

        @Override
        public int compareTo(RawData o) {
            return Integer.compare(this.id, o.id);
        }
    }

    private class RawConversionResult {
        ConverterInterface.DataPortionInterface leftChannelData;
        ConverterInterface.DataPortionInterface rightChannelData;
        Long leftChannelConversionResult;
        Long rightChannelConversionResult;

        ConversionResult convertToFinalResult(){
            return new ConversionResult(this.leftChannelData, this.rightChannelData,
                    this.leftChannelConversionResult, this.rightChannelConversionResult);
        }
    }

    @Override
    public synchronized void setCores(int cores) {
        availableCoresCount = cores;
        if (scheduler == null) {
            scheduler = new ConvertScheduler();
            Sender resultSender = new Sender();
            Thread schedulerThread = new Thread(scheduler);
            schedulerThread.start();
            Thread senderThread = new Thread(resultSender);
            senderThread.start();
        }
    }

    @Override
    public synchronized void setConverter(ConverterInterface converter) {
        this.converter = converter;
    }

    @Override
    public synchronized void setConversionReceiver(ConversionReceiverInterface receiver) {
        this.conversionReceiver = receiver;
    }

    @Override
    public void addDataPortion(ConverterInterface.DataPortionInterface data) {
        saveDataLock.lock();
        RawConversionResult currentRawConversionResult;
        if (!rawConversionResults.containsKey(data.id())){
            currentRawConversionResult = new RawConversionResult();
            rawConversionResults.put(data.id(), currentRawConversionResult);
        }
        else {
            currentRawConversionResult = rawConversionResults.get(data.id());
        }
        if (data.channel() == ConverterInterface.Channel.RIGHT_CHANNEL)
            currentRawConversionResult.rightChannelData = data;
        else
            currentRawConversionResult.leftChannelData = data;
        rawDataQueue.add(new RawData(data));
        saveDataLock.unlock();
        schedulerSemaphore.release(1);
    }

    class ConvertScheduler implements Runnable {
        AtomicInteger runningThreads = new AtomicInteger(0);
        public void run(){
            while(true) {
                try {
                    schedulerSemaphore.acquire(1);
                    while (runningThreads.get() >= ConversionManagement.this.availableCoresCount) {
                        maximumThreadsSemaphore.acquire(1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Thread t1 = new Thread(new ResultComputer());
                runningThreads.getAndIncrement();
                t1.start();
            }
        }
    }

    class ResultComputer implements Runnable{
        volatile RawData currentData;
        volatile RawConversionResult currentRawConversionResult;
        volatile long conversionResult;
        volatile boolean isRightChannel;
        @Override
        public void run(){
            currentData = rawDataQueue.poll();
            currentRawConversionResult = rawConversionResults.get(currentData.id);
            isRightChannel = currentData.channelId == ConverterInterface.Channel.RIGHT_CHANNEL;
            conversionResult = isRightChannel ?
                    converter.convert(currentRawConversionResult.rightChannelData) :
                    converter.convert(currentRawConversionResult.leftChannelData);

            saveDataLock.lock();
            if (isRightChannel)
                currentRawConversionResult.rightChannelConversionResult = conversionResult;
            else
                currentRawConversionResult.leftChannelConversionResult = conversionResult;
            saveDataLock.unlock();

            try {
                scheduler.runningThreads.getAndDecrement();
                resultSenderSemaphore.release(1);
                maximumThreadsSemaphore.release(1);
            } catch (IllegalMonitorStateException e) {e.printStackTrace();}
        }
    }

    class Sender implements Runnable{
        volatile RawConversionResult currentRawConversionResult;
        @Override
        public void run() {
            try {
                while (true) {
                    resultSenderSemaphore.acquire(1);
                    if (rawConversionResults.isEmpty())
                        resultSenderSemaphore.acquire(1);
                    do {
                        if (rawConversionResults.containsKey(nextId.get()))
                            currentRawConversionResult = rawConversionResults.get(nextId.get());
                    } while ((sendResultsIfReady(currentRawConversionResult)));
                }
            } catch (Exception e) {e.printStackTrace();}
        }

        private boolean sendResultsIfReady(RawConversionResult rawResult){
            if (rawResult == null || rawResult.rightChannelConversionResult == null || rawResult.leftChannelConversionResult == null)
                return false;
            Integer currentId = rawResult.leftChannelData.id();
            if (nextId.get() == currentId) {
                conversionReceiver.result(rawResult.convertToFinalResult());
                rawConversionResults.remove(currentId);
                nextId.getAndIncrement();
                return true;
            }
            return false;
        }
    }
}
