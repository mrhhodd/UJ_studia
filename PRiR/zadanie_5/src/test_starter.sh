#!/bin/bash

test_multiplier=200
let "total_test= $test_multiplier"

echo "" > output_result.txt
javac8 Start.java
sleep 5
ps | grep java | xargs kill -9 &> /dev/null
sleep 5
java8 Start -ORBInitialPort 9999 &

clear

for (( c=1; c<=$test_multiplier; c++ )); 
do
    java8 PMO_Start -ORBInitialPort 9999 > out.txt
    error_counter=$(cat out.txt | grep BLAD | wc -l) 
    if [ $error_counter -gt 0 ]
        then
            echo -e "$c FAIL"
	else
	    echo -e "$c PASS"
    fi
    cat out.txt >> output_result.txt
done

passed=$(cat output_result.txt | grep BLEDU | wc -l)
let "failed = $total_test - $passed"
echo "TOTAL: $total_test PASSED: $passed FAILED: $failed" #> output_result.txt 
echo "TOTAL: $total_test PASSED: $passed FAILED: $failed" >> output_result.txt 

