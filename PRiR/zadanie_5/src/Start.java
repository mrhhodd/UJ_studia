import org.omg.CORBA.IntHolder;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

class myServer extends ServerInterfacePOA{
    private int maximumResources;
    volatile private static AtomicInteger nextTaskId = new AtomicInteger(0);
    volatile private static AtomicInteger nextUserId = new AtomicInteger(0);
    volatile private ConcurrentHashMap<Integer, TaskPack> allTasks = new ConcurrentHashMap<>();
    volatile private ConcurrentHashMap<Integer, Integer> runnningTasks = new ConcurrentHashMap<>();
    volatile private NamingContext namingContext;

    private Semaphore runnerSemaphore = new Semaphore(0);
    private TaskRunner taskRunner = new TaskRunner();
    private Thread runnerThread = new Thread(taskRunner);

    { runnerThread.start(); }

    void setNamingContext(NamingContext nc){
        this.namingContext = nc;
    }

    @Override
    public void done(int taskID) {
        TaskPack task = allTasks.get(runnningTasks.get(taskID));
        task.activeTasks--;
        runnningTasks.remove(taskID);
        runnerSemaphore.release();
    }

    @Override
    public void setResources(int cores) {
        this.maximumResources = cores;
    }

    @Override
    public void cancel(int userID) {
        allTasks.get(userID).cancelAllTasks();
    }

    @Override
    public void submit(String[] urls, int tasks, int parallelTasks, IntHolder userID) {
        userID.value = getNextUserId();
        allTasks.put(userID.value, new TaskPack(urls, parallelTasks));
        runnerSemaphore.release();
    }

    private Integer getNextUserId(){
        return nextUserId.getAndIncrement();
    }

    private Integer getNextTaskId(){
        return nextTaskId.getAndIncrement();
    }

    private class TaskRunner implements Runnable{
        @Override
        public void run() {
            while (true){
                try {
                    runnerSemaphore.acquire();
                    runNextTaskGroup();
                } catch (InterruptedException e) {e.printStackTrace();}
            }
        }

        private void runNextTaskGroup(){
            Integer currentUserId;
            SingleTask currentTask;
            TaskPack currentTaskPack;
            while (runnningTasks.size() <= maximumResources) {
                currentUserId = getCurrentUserId();
                if (currentUserId == -1)
                    break;

                currentTaskPack = allTasks.get(currentUserId);
                for (int i = 0; i < currentTaskPack.parallelTasks; i++) {
                    currentTask = currentTaskPack.tasks.getFirst();
                    currentTaskPack.tasks.remove(currentTask);
                    runnningTasks.put(currentTask.id, currentUserId);
                    new Thread(currentTask).start();
                }
                currentTaskPack.lastExecution = System.currentTimeMillis();
                currentTaskPack.activeTasks = currentTaskPack.parallelTasks;
            }
        }

        private int getCurrentUserId(){
            List<Integer> sortedTaskPacks = new LinkedList<>();
            allTasks.entrySet().stream().
                    sorted(Comparator.comparing(e2 -> e2.getValue().lastExecution)).
                    forEach(e -> sortedTaskPacks.add(e.getKey()));
            return  sortedTaskPacks.stream().
                    filter(e -> allTasks.get(e).parallelTasks <= maximumResources - runnningTasks.size()).
                    filter(e -> allTasks.get(e).activeTasks == 0).
                    filter(e -> allTasks.get(e).tasks.size() != 0).
                    findFirst().orElse(-1);
        }

    }

    private class TaskPack{
        LinkedList<SingleTask> tasks = new LinkedList<>();
        volatile int activeTasks = 0;
        int parallelTasks;
        Long lastExecution;

        TaskPack(String[] urls, int parallelTasks){
            Arrays.stream(urls).forEach(this::addNewTask);
            this.parallelTasks = parallelTasks;
            this.lastExecution = System.currentTimeMillis();
        }

        void addNewTask(String taskUrl){
            tasks.add(new SingleTask(myServer.this.getNextTaskId(), taskUrl));
        }

        void cancelAllTasks(){
            tasks = new LinkedList<>();
        }
    }

    private class SingleTask implements Runnable{
        String url;
        int id;
        SingleTask(int id, String url) {
            this.id = id;
            this.url = url;
        }

        @Override
        public void run() {
            try {
                NameComponent[] names = {new NameComponent(this.url, "Object")};
                org.omg.CORBA.Object taskRawObj = namingContext.resolve(names);
                TaskInterfaceHelper.narrow(taskRawObj).start(this.id);
            }
            catch ( NotFound | CannotProceed | org.omg.CosNaming.NamingContextPackage.InvalidName e) {e.printStackTrace();}
        }
    }
}

public class Start {
    static public void main(String args[]) {
        try {
            ORB orb = ORB.init(args, null);
            POA rootpoa = (POA) orb.resolve_initial_references("RootPOA");
            rootpoa.the_POAManager().activate();
            myServer myServer = new myServer();
            org.omg.CORBA.Object serverRef = rootpoa.servant_to_reference(myServer);
            org.omg.CORBA.Object namingContextObj = orb.resolve_initial_references("NameService");
            NamingContext nCont = NamingContextHelper.narrow(namingContextObj);
            myServer.setNamingContext(nCont);
            NameComponent[] path = {new NameComponent("SERVER", "Object")};
            nCont.rebind(path, serverRef);
            orb.run();
        }
        catch (AdapterInactive | WrongPolicy | ServantNotActive | NotFound | CannotProceed |
                InvalidName | org.omg.CosNaming.NamingContextPackage.InvalidName e)
        {e.printStackTrace();}
    }
}
