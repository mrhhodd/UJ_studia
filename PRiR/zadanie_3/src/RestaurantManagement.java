import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class RestaurantManagement implements RestaurantManagementInterface{
    private KitchenInterface kitchen;
    private OrderPoint orderPoint = new OrderPoint();
    private ReceiverInterface orderReceiver = new OrderReceiver();
    volatile private ConcurrentHashMap<WaiterInterface, Boolean> waiterSet = new ConcurrentHashMap<>();
    private PriorityBlockingQueue<Order> rawMealQueue = new PriorityBlockingQueue<>();
    private ConcurrentHashMap<Integer, Order> mealsReady = new ConcurrentHashMap<>();
    private AtomicInteger preparedMealsCounter = new AtomicInteger(0);
    private AtomicInteger mealsInPreparationCounter = new AtomicInteger(0);
    private Semaphore tooManyMealsPreparedSemaphore = new Semaphore(0);
    private Semaphore noWaitersReadySemaphore = new Semaphore(0);
    private Semaphore waiterSetEmptySemaphore = new Semaphore(0);

    private class Order implements Comparable<Order>{
        Integer orderId;
        Integer tableId;
        WaiterInterface assignedWaiter;
        Order(Integer orderId, Integer tableId){
            this.orderId = orderId;
            this.tableId = tableId;
        }
        public void assignWaiter(WaiterInterface waiter){
            assignedWaiter = waiter;
        }

        @Override
        public int compareTo(Order o) {
            return Integer.compare(this.orderId, o.orderId);
        }
    }

    private class OrderPoint implements OrderInterface, Runnable{
        private Semaphore newMealSemaphore = new Semaphore(0);
        Semaphore kitchenBusySemaphore = new Semaphore(0);
        volatile Order currentOrder;

        @Override
        public void newOrder(int orderID, int tableID) {
            rawMealQueue.add(new Order(orderID, tableID));
            newMealSemaphore.release(1);
        }

        @Override
        public void orderComplete(int orderID, int tableID) {
            WaiterInterface freedWaiter = mealsReady.get(orderID).assignedWaiter;

            waiterSet.put(freedWaiter, true);
            tooManyMealsPreparedSemaphore.release(1);
            kitchenBusySemaphore.release(1);
            noWaitersReadySemaphore.release(1);
        }
        @Override
        public void run() {
            try {
                if (waiterSet.size() == 0)
                    waiterSetEmptySemaphore.acquire();
                while (true) {
                    newMealSemaphore.acquire(1);

                    currentOrder = rawMealQueue.poll();
                    while (preparedMealsCounter.get() >= waiterSet.size()) {
                        tooManyMealsPreparedSemaphore.acquire(1);
                    }
                    prepareMeals();
                    mealsReady.put(currentOrder.orderId, new Order(currentOrder.orderId, currentOrder.tableId));
                    preparedMealsCounter.getAndIncrement();
                }
            } catch (InterruptedException e) {e.printStackTrace();}
        }

        private void prepareMeals() throws InterruptedException{
            while (mealsInPreparationCounter.get() >= kitchen.getNumberOfParallelTasks()) {
                kitchenBusySemaphore.acquire(1);
            }
            mealsInPreparationCounter.getAndIncrement();
            kitchen.prepare(currentOrder.orderId);
            tooManyMealsPreparedSemaphore.release(1);
        }
    }

    private class OrderReceiver implements ReceiverInterface{
        WaiterInterface currentWaiter;
        Integer tableId;
        @Override
        synchronized public void mealReady(int orderID) {
            try {
                while (waiterSet.entrySet().stream().noneMatch(Map.Entry::getValue))
                    noWaitersReadySemaphore.acquire(1);
            } catch (InterruptedException e) {e.printStackTrace();}
            finally {
                currentWaiter = waiterSet.entrySet().stream().filter(Map.Entry::getValue).findFirst().orElse(null).getKey();
            }
            tableId = mealsReady.get(orderID).tableId;
            waiterSet.put(currentWaiter, false);
            mealsReady.get(orderID).assignWaiter(currentWaiter);
            currentWaiter.go(orderID, tableId);
            mealsInPreparationCounter.getAndDecrement();
            preparedMealsCounter.getAndDecrement();
            tooManyMealsPreparedSemaphore.release(1);
        }
    }

    @Override
    public void setKitchen(KitchenInterface kitchen) {
        this.kitchen = kitchen;
        this.kitchen.registerReceiver(orderReceiver);
        Thread kitchenThread = new Thread(orderPoint);
        kitchenThread.start();
    }

    @Override
    public void addWaiter(WaiterInterface waiter) {
        waiter.registerOrder(orderPoint);
        waiterSet.put(waiter, true);
        waiterSetEmptySemaphore.release();
    }

    @Override
    public void removeWaiter(WaiterInterface waiter) {
        waiterSet.remove(waiter);
    }
}
