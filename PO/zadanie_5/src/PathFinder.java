import java.util.*;
import java.util.stream.Stream;

public class PathFinder implements PathFinderInterface {

    class Path {
        int stepCount = 0;
        int totalCost = 0;
        List<LabyrinthInterface.Direction> allSteps = new LinkedList<>();
        List<LabyrinthInterface.Direction> reversedSteps = new LinkedList<>();

        Path(int newCost, LabyrinthInterface.Direction newDirection){
            this.totalCost = newCost;
            this.stepCount = 1;
            updateStepsLists(newDirection);
        }

        Path(Path originPath, int newCost, LabyrinthInterface.Direction newDirection) throws LabyrinthInterface.YouShallNotPassException{
            this.totalCost = originPath.totalCost + newCost;
            this.stepCount = originPath.stepCount + 1;
            this.allSteps.addAll(originPath.allSteps);
            this.reversedSteps.addAll(originPath.reversedSteps);
            updateStepsLists(newDirection);
            this.checkForDoubleLoops();
        }

        private void updateStepsLists(LabyrinthInterface.Direction newDirection){
            this.allSteps.add(newDirection);
            this.reversedSteps.add(0, getReversedDirection(newDirection));
        }

        private void checkForDoubleLoops() throws LabyrinthInterface.YouShallNotPassException{
            List<LabyrinthInterface.Direction> tmpList = new LinkedList<>();

            tmpList.addAll(this.allSteps);
            do {
                if (!isShifted(tmpList))
                    throw new LabyrinthInterface.YouShallNotPassException();
                tmpList.remove(0);
            } while (tmpList.size() > 1);
        }

        private boolean isShifted(List<LabyrinthInterface.Direction> directionList) {
            long verticalShiftCounter = directionList.stream().filter(dir -> dir == LabyrinthInterface.Direction.NORTH).count() -
                    directionList.stream().filter(dir -> dir == LabyrinthInterface.Direction.SOUTH).count();
            long horizontalShiftCounter = directionList.stream().filter(dir -> dir == LabyrinthInterface.Direction.EAST).count() -
                    directionList.stream().filter(dir -> dir == LabyrinthInterface.Direction.WEST).count();
            return (verticalShiftCounter != 0 || horizontalShiftCounter != 0);
        }
    }


    LabyrinthInterface labyrinth = null;
    Set<Path> unfinishedPaths = new HashSet<>();
    Set<Path> finishedPaths = new HashSet<>();
    Set<Path> temporaryPaths = new HashSet<>();

    private static LabyrinthInterface.Direction getReversedDirection(LabyrinthInterface.Direction direction){
        LabyrinthInterface.Direction reversedDirection = null;
        switch(direction){
            case EAST:
                reversedDirection = LabyrinthInterface.Direction.WEST;
                break;
            case WEST:
                reversedDirection = LabyrinthInterface.Direction.EAST;
                break;
            case NORTH:
                reversedDirection = LabyrinthInterface.Direction.SOUTH;
                break;
            case SOUTH:
                reversedDirection = LabyrinthInterface.Direction.NORTH;
        }
        return reversedDirection;
    }

    @Override
    public Set<List<LabyrinthInterface.Direction>> getShortestPaths() {
        int minimumStepCount = finishedPaths.stream().mapToInt(path -> path.stepCount).min().getAsInt();
        Set<List<LabyrinthInterface.Direction>> shortestPaths = new HashSet<>();
        finishedPaths.stream()
                .filter(path -> path.stepCount == minimumStepCount)
                .forEach(path -> shortestPaths.add(path.allSteps));
        return shortestPaths;
    }

    @Override
    public Set<List<LabyrinthInterface.Direction>> getCheapestPaths() {
        int minimumStepCount = finishedPaths.stream().mapToInt(path -> path.totalCost).min().getAsInt();
        return getPathsOfCost(minimumStepCount);
    }

    @Override
    public Map<Integer, Set<List<LabyrinthInterface.Direction>>> getPathsLessExpensiveThan(int maxCost) {
        Map<Integer, Set<List<LabyrinthInterface.Direction>>> mapOfAllPaths = new HashMap<>();
        finishedPaths.stream()
                .filter(path -> path.totalCost < maxCost)
                .forEach(path -> mapOfAllPaths.put(path.totalCost, getPathsOfCost(path.totalCost)));
        return mapOfAllPaths;
    }

    private Set<List<LabyrinthInterface.Direction>> getPathsOfCost(int cost) {
        Set<List<LabyrinthInterface.Direction>> paths = new HashSet<>();
        finishedPaths.stream()
                .filter(path -> path.totalCost == cost)
                .forEach(path -> paths.add(path.allSteps));
        return paths;
    }

    @Override
    public void exploreLabyrinth(LabyrinthInterface labyrinth) {
        this.labyrinth = labyrinth;
        initializePaths();
        while (true){
            this.unfinishedPaths.forEach(this::discoverNewSteps);
            this.updatePaths();
            if (unfinishedPaths.size() == 0)
                  break;
        }
    }

    void initializePaths(){
        Stream.of(LabyrinthInterface.Direction.values()).forEach(direction -> {
            try {
                unfinishedPaths.add(new Path (this.labyrinth.getCost(direction), direction));
            }
            catch (LabyrinthInterface.YouShallNotPassException e){}
        });
        assert unfinishedPaths.size() != 0 : "Can't go anywhere from initial position!";
    }

    private void moveToEndOfPath(Path path){
        path.allSteps.forEach(labyrinth::move);
    }

    private void goBackToStart(Path path){
        path.reversedSteps.forEach(labyrinth::move);
    }

    private void updatePaths(){
        this.unfinishedPaths = this.temporaryPaths;
        this.temporaryPaths = new HashSet<>();
    }

    private void discoverNewSteps(Path oldPath){
        LabyrinthInterface.Direction backDirection = oldPath.reversedSteps.get(0);
        moveToEndOfPath(oldPath);
        Stream.of(LabyrinthInterface.Direction.values())
                .filter(dir -> dir != backDirection).forEach(direction -> {
            try {
                if (this.labyrinth.getCost(direction) != 0)
                    temporaryPaths.add(new Path (oldPath, this.labyrinth.getCost(direction), direction));
                else
                    finishedPaths.add(new Path (oldPath, this.labyrinth.getCost(direction), direction));
            }
            catch (LabyrinthInterface.YouShallNotPassException e){}
        });
        goBackToStart(oldPath);
    }
}
