
public interface LabyrinthInterface {
	/**
	 * Typ wyliczeniowy reprezentujacy dostepne kierunki przemieszczania sie w
	 * labiryncie.
	 */
	public enum Direction {
		SOUTH, NORTH, WEST, EAST;
	}

	/**
	 * Wyjatek informujacy o braku mozliwosci przejscia w ta strone.
	 */
	class YouShallNotPassException extends Exception {
		private static final long serialVersionUID = 4399365499586330374L;

	}

	/**
	 * Metoda zwraca koszt zajecia pozycji znajdujacej sie w direction od aktualnej.
	 * 
	 * @param direction
	 *            kierunek, dla ktorego wykonywany jest test kosztu.
	 * @return dodatni koszt zajmowania nastepnej pozycji lub 0 jesli nastepna
	 *         pozycja to start lub meta.
	 * @throws YouShallNotPassException
	 *             zakaz ruchu w danym kierunku (sciana).
	 */
	int getCost(Direction direction) throws YouShallNotPassException;

	/**
	 * Metoda zleca wykonanie pojedynczego kroku w zadanym kierunku.
	 * 
	 * @param direction
	 *            kierunek ruchu
	 * @return true - udalo sie zmienic aktualna pozycje, false - pozycja nie
	 *         zostala zmieniona, bo pozycja docelowa jest sciana.
	 */
	boolean move(Direction direction);

	/**
	 * Metoda zwraca doglebna kopie obiektu implementujacego LabyrinthInterface
	 * w stanie z chwili wykonania clone()
	 * 
	 * @return kopia obiektu
	 */
    public LabyrinthInterface clone();
}
