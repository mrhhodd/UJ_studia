import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class PathFinderTest {

    int[][] simpleMapTwoPaths = {{-1,-1,-1,-1,-1,-1}, {-1,1,1,0,5,-1}, {-1,1,-1,-1,3,-1},{-1,1,1,0,4,-1},{-1,-1,-1,-1,-1,-1}};

    int[][] mapWithLoop = {
            {-1,-1,-1,-1,-1,-1,-1,-1},
            {-1, 0, 1, 1, 1, 1, 1,-1},
            {-1, 1,-1,-1,-1,-1, 1,-1},
            {-1, 1,-1, 1, 1, 1, 1,-1},
            {-1, 1, 1, 1,-1,-1,-1,-1},
            {-1, 1,-1, 1, 1, 1, 1,-1},
            {-1, 1,-1,-1,-1,-1, 1,-1},
            {-1, 1,-1, 1, 1, 1, 1,-1},
            {-1, 1,-1,-1,-1, 1,-1,-1},
            {-1, 1,-1,-1, 1, 1, 1,-1},
            {-1, 1,-1,-1, 1,-1, 1,-1},
            {-1, 0, 1, 1, 1, 1, 1,-1},
            {-1,-1,-1,-1,-1,-1,-1,-1},
    };

    int[][] mapWithLoop2 = {
            {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
            {-1,-1,-1,-1, 0,-1, 1, 1, 1,-1},
            {-1,-1,-1,-1, 1,-1, 1,-1, 1,-1},
            {-1, 5, 5, 5, 1, 1, 1, 1,1,-1},
            {-1, 5,-1,-1,-1,-1, 1,-1,-1,-1},
            {-1, 5,-1,-1,-1,-1, 1, 1, 1,-1},
            {-1, 5,-1,-1,-1,-1,-1,-1, 1,-1},
            {-1, 5,-1,-1,-1,-1,-1,-1, 1,-1},
            {-1, 5, 5, 5, 5, 5, 5, 5, 0,-1},
            {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},

    };
    DebugLabyrinth labyrinth = new DebugLabyrinth(simpleMapTwoPaths, 1,3);
    PathFinder pf = new PathFinder();
    DebugLabyrinth labyrinth2 = new DebugLabyrinth(mapWithLoop, 1, 1);
    DebugLabyrinth labyrinth3 = new DebugLabyrinth(mapWithLoop2, 1, 4);


    @Test(timeout = 1000)
    public void pathsAreInitializedCorrectlyForSimpleMapTwoPaths(){
        pf.labyrinth = labyrinth;
        pf.initializePaths();
        pf.unfinishedPaths.forEach(path -> System.out.print(path.allSteps));
        assert pf.unfinishedPaths.size() == 2;
    }

    @Test(timeout = 1000)
    public void pathsAreCorrectlyFoundForSimpleMapTwoPaths(){
        pf.exploreLabyrinth(labyrinth);
        pf.finishedPaths.forEach(path -> System.out.println(path.allSteps));
        assert pf.unfinishedPaths.size() == 0;
        assert pf.finishedPaths.size() == 2;
    }

    @Test(timeout = 1000)
    public void canGetShortestPathForSimpleMapTwoPaths(){
        pf.exploreLabyrinth(labyrinth);
        Set<List<LabyrinthInterface.Direction>> shortestPaths =  pf.getShortestPaths();
        assert shortestPaths.size() == 1;
        shortestPaths.forEach(System.out::println);
    }

    @Test(timeout = 1000)
    public void canGetCheapestPathForSimpleMapTwoPaths(){
        pf.exploreLabyrinth(labyrinth);
        Set<List<LabyrinthInterface.Direction>> cheapestPaths =  pf.getCheapestPaths();
        assert cheapestPaths.size() == 1;
        cheapestPaths.forEach(System.out::println);
    }

    @Test(timeout = 1000)
    public void canGetCorrectAmmountOfPathsForgetPathsLessExpensiveThanForSimpleMapTwoPaths(){
        pf.exploreLabyrinth(labyrinth);
        Map<Integer, Set<List<LabyrinthInterface.Direction>>> paths = pf.getPathsLessExpensiveThan(5);
        assert paths.size() == 0;
        paths = pf.getPathsLessExpensiveThan(6);
        assert paths.size() == 1;
        paths = pf.getPathsLessExpensiveThan(13);
        assert paths.size() == 2;
    }


    @Test(timeout = 1000)
    public void pathsAreCorrectlyFoundForLoopMap(){
        pf.exploreLabyrinth(labyrinth2);
        pf.finishedPaths.forEach(path -> System.out.println(path.allSteps));

    }

    @Test(timeout = 1000)
    public void canGetCorrectAmmountOfPathsForgetPathsLessExpensiveThanForLoopMap(){
        DebugLabyrinth.showLabyrinthMap(labyrinth3);
        pf.exploreLabyrinth(labyrinth3);
        Map<Integer, Set<List<LabyrinthInterface.Direction>>> paths = pf.getPathsLessExpensiveThan(50);
        paths.forEach((key,path) -> System.out.println(path));
    }
}
