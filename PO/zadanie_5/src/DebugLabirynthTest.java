import org.junit.Test;

public class DebugLabirynthTest {

    int[][] simpleMapTwoPaths = {{-1,-1,-1,-1,-1,-1}, {-1,1,1,0,5,-1}, {-1,1,-1,-1,3,-1},{-1,1,1,0,4,-1},{-1,-1,-1,-1,-1,-1}};
    DebugLabyrinth labyrinth = new DebugLabyrinth(simpleMapTwoPaths, 0,0);

    @Test
    public void canShowLabyrinthMap(){
        DebugLabyrinth.showLabyrinthMap(labyrinth);
    }

    @Test
    public void initialPositionIsAsExpected(){
        DebugLabyrinth labi = new DebugLabyrinth(simpleMapTwoPaths, 1,3);
        assert !labi.move(LabyrinthInterface.Direction.EAST) && !labi.move(LabyrinthInterface.Direction.WEST);
        try {
            assert labi.getCost(LabyrinthInterface.Direction.NORTH) == 5;
            assert labi.getCost(LabyrinthInterface.Direction.SOUTH) == 1;
        } catch (LabyrinthInterface.YouShallNotPassException e) {}

    }

    @Test
    public void getCostThrowsExceptions(){
        boolean eastWasThrown = false;
        boolean westWasThrown = false;
        DebugLabyrinth labi = new DebugLabyrinth(simpleMapTwoPaths, 1,3);
        try {
            labi.getCost(LabyrinthInterface.Direction.EAST);
        } catch (LabyrinthInterface.YouShallNotPassException e) {
            eastWasThrown = true;
        }
        assert eastWasThrown;

        try {
            labi.getCost(LabyrinthInterface.Direction.WEST);
        } catch (LabyrinthInterface.YouShallNotPassException e) {
            westWasThrown = true;
        }
        assert westWasThrown;
    }

    @Test
    public void canMoveAndEverythingWorks(){
        DebugLabyrinth labi = new DebugLabyrinth(simpleMapTwoPaths, 1,3);
        assert labi.move(LabyrinthInterface.Direction.NORTH);
        assert !labi.move(LabyrinthInterface.Direction.NORTH) && !labi.move(LabyrinthInterface.Direction.WEST);
        try {
            assert labi.getCost(LabyrinthInterface.Direction.EAST) == 3;
            assert labi.getCost(LabyrinthInterface.Direction.SOUTH) == 0;
        } catch (LabyrinthInterface.YouShallNotPassException e) {}

        boolean northWasThrown = false;
        boolean westWasThrown = false;
        try {
            labi.getCost(LabyrinthInterface.Direction.NORTH);
        } catch (LabyrinthInterface.YouShallNotPassException e) {
            northWasThrown = true;
        }
        assert northWasThrown;

        try {
            labi.getCost(LabyrinthInterface.Direction.WEST);
        } catch (LabyrinthInterface.YouShallNotPassException e) {
            westWasThrown = true;
        }
        assert westWasThrown;
    }
}
