public class DebugLabyrinth implements LabyrinthInterface {

    private int[][] labyrinthMap;
    private int currentPositionX, currentPositionY;

    DebugLabyrinth(int[][] labyrinthMap, int initialPositionX,  int initialPositionY){
        this.labyrinthMap = labyrinthMap;
        this.currentPositionX = initialPositionX;
        this.currentPositionY = initialPositionY;
    }

    @Override
    public int getCost(Direction direction) throws YouShallNotPassException {
        Integer cost = null;
        switch (direction){
            case EAST:
                cost = labyrinthMap[currentPositionX + 1][currentPositionY];
                break;
            case WEST:
                cost = labyrinthMap[currentPositionX  - 1][currentPositionY];
                break;
            case NORTH:
                cost = labyrinthMap[currentPositionX][currentPositionY + 1];
                break;
            case SOUTH:
                cost = labyrinthMap[currentPositionX][currentPositionY - 1];
        }
        if (cost < 0) throw new YouShallNotPassException();

        return cost;
    }

    @Override
    public boolean move(Direction direction) {
        boolean canMove;
        int newX = currentPositionX;
        int newY = currentPositionY;
        switch (direction) {
            case EAST:
                newX += 1;
                break;
            case WEST:
                newX -= 1;
                break;
            case NORTH:
                newY += 1;
                break;
            case SOUTH:
                newY -= 1;
        }
        canMove = this.canMove(direction);
        if (canMove){
            this.currentPositionY = newY;
            this.currentPositionX = newX;
        }

        return canMove;
    }

    private boolean canMove(Direction direction) {
        Boolean canMove = null;
        switch (direction){
            case EAST:
                canMove = labyrinthMap[currentPositionX + 1][currentPositionY] >= 0;
                break;
            case WEST:
                canMove = labyrinthMap[currentPositionX - 1][currentPositionY] >= 0;
                break;
            case NORTH:
                canMove = labyrinthMap[currentPositionX][currentPositionY + 1] >= 0;
                break;
            case SOUTH:
                canMove = labyrinthMap[currentPositionX][currentPositionY - 1] >= 0;
        }
        return canMove;
    }

    @Override
    public LabyrinthInterface clone() {
        return null;
    }

    public static void showLabyrinthMap(DebugLabyrinth lab){
        int[][] labyrinth = lab.labyrinthMap;
        for (int i = labyrinth[0].length - 1; i >= 0; i--) {
            StringBuilder sb = new StringBuilder();
            for (int[] aLabyrinth : labyrinth) {
                sb.append(aLabyrinth[i] >= 0 ? aLabyrinth[i] : "X");
                sb.append(" ");
            }
            System.out.println(sb);
        }
    }
}
