import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Interfejs systemu przeszukiwania labiryntu.
 * @author oramus
 *
 */
public interface PathFinderInterface {
	/**
	 * Metoda przekazuje labirynt, który należy zbadac.
	 * 
	 * @param labyrinth
	 *            labirynt do rozpoznania.
	 */
	public void exploreLabyrinth(LabyrinthInterface labyrinth);

	/**
	 * Metoda zwraca zbior tras, ktore sa najkrotsze (wymagaja najmniejszej liczby
	 * krokow do przejscia).
	 * 
	 * @return zbior najkrotszych tras.
	 */
	public Set<List<LabyrinthInterface.Direction>> getShortestPaths();

	/**
	 * Metoda zwraca zbior najtanszych tras, czyli trasy, ktore prowadza przez
	 * zajtansze pozycje w labiryncie.
	 * 
	 * @return zbior najtanszych tras.
	 */
	public Set<List<LabyrinthInterface.Direction>> getCheapestPaths();

	/**
	 * Metoda zwraca mape zawierajaca zbiory tras o koszcie mniejszym niż podane.
	 * Kluczem w mapie jest koszt trasy.
	 * 
	 * @param maxCost
	 *            maksymalny koszt, trasy umieszczone w wyniku maja byc od niego
	 *            tansze
	 * @return trasy tansze od maxCost
	 */
	public Map<Integer, Set<List<LabyrinthInterface.Direction>>> getPathsLessExpensiveThan(int maxCost);
}
