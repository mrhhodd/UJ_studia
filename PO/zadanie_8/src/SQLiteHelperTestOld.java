import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;


public class SQLiteHelperTestOld {

    SQLiteHelper sqlh = new SQLiteHelper();

    @Test
    public void createTableForSimpleIntegerWorks(){
        class ExampleClass{
            public int parameter;
        }

        ExampleClass exampleClassInstance = new ExampleClass();
        String expectedResult = "CREATE TABLE IF NOT EXISTS ExampleClass(parameter INTEGER);";
        Assert.assertEquals(expectedResult, sqlh.createTable(exampleClassInstance));
    }

    @Test
    public void createTableForMultiElementClassWorks(){
        class ExampleClass2{
            public int paramint;
            public long paramlong;
            public Integer paramInteger;
            public Long paramLong;
            public float paramfloat;
            public double paramcouble;
            public Float paramFloat;
            public Double paramDouble;
            public String paramString;
            public boolean paramboolean;
            public Boolean paramBoolean;
        }

        ExampleClass2 exampleClass2Instance = new ExampleClass2();
        String expectedResult = "CREATE TABLE IF NOT EXISTS ExampleClass2" +
                "(paramint INTEGER,paramlong INTEGER,paramInteger INTEGER,paramLong INTEGER," +
                "paramfloat REAL,paramcouble REAL,paramFloat REAL,paramDouble REAL," +
                "paramString TEXT,paramboolean INTEGER,paramBoolean INTEGER);";
        Assert.assertEquals(expectedResult, sqlh.createTable(exampleClass2Instance));
    }

    @Test
    public void createTableIgnoresNonPublicElements(){
        class ExampleClass3 {
            public int paramint;
            public Double paramDouble;
            private String paramInteger;
            Float paramLong;
            protected double paramfloat;
        }
        ExampleClass3 exampleClass3Instance = new ExampleClass3();
        String expectedResult = "CREATE TABLE IF NOT EXISTS ExampleClass3(paramint INTEGER,paramDouble REAL);";
        Assert.assertEquals(expectedResult, sqlh.createTable(exampleClass3Instance));
    }

    @Test
    public void createTableIgnoresOtherClassesElements(){
        class ExampleClass4{
            public int paramint;
            public Double paramDouble;
            public LinkedList<String> paramLinkedList;
            public Integer[] paramIntegerArray;
        }
        ExampleClass4 exampleClass4Instance = new ExampleClass4();
        String expectedResult = "CREATE TABLE IF NOT EXISTS ExampleClass4(paramint INTEGER,paramDouble REAL);";
        Assert.assertEquals(expectedResult, sqlh.createTable(exampleClass4Instance));
    }

    @Test
    public void insertForSimpleIntegerWorks(){
        class ExampleClass5{
            public int parameter = 2137;
        }

        ExampleClass5 exampleClassInstance = new ExampleClass5();
        String expectedResult = "INSERT INTO ExampleClass5(parameter) VALUES(2137);";
        Assert.assertEquals(expectedResult, sqlh.insert(exampleClassInstance));
    }


    @Test
    public void insertForMultiElementClassWorks(){
        class ExampleClass2{
            public int paramint = 12;
            public long paramlong = 1234567890L;
            public Integer paramInteger = 14;
            public Long paramLong = 1234567899L;
            public float paramfloat = 0.1f;
            public double paramcouble = 0.22d;
            public Float paramFloat = -0.1235f;
            public Double paramDouble = 1/5d;
            public String paramString = "siala baba mak";
            public boolean paramboolean = true;
            public Boolean paramBoolean = false;
        }

        ExampleClass2 exampleClass2Instance = new ExampleClass2();
        String expectedResult = "INSERT INTO ExampleClass2(" +
                "paramint,paramlong,paramInteger,paramLong," +
                "paramfloat,paramcouble,paramFloat,paramDouble," +
                "paramString,paramboolean,paramBoolean" +
                ") VALUES(" +
                "12,1234567890,14,1234567899," +
                "\"0,1\",\"0,22\",\"-0,1235\",\"0,2\"," +
                "\"siala baba mak\",1,0);";
        Assert.assertEquals(expectedResult, sqlh.insert(exampleClass2Instance));
    }

    @Test
    public void insertIgnoresNonPublicElements(){
        class ExampleClass3 {
            public int paramint = 12;
            public Double paramDouble = -0.7d;
            private String paramInteger = "mnie nie ma";
            Float paramLong = 1f;
            protected double paramfloat = 0.99999d;
        }
        ExampleClass3 exampleClass3Instance = new ExampleClass3();
        String expectedResult = "INSERT INTO ExampleClass3(paramint,paramDouble) VALUES(12,\"-0,7\");";
        Assert.assertEquals(expectedResult, sqlh.insert(exampleClass3Instance));
    }


    @Test
    public void insertIgnoresOtherClassesElements(){
        class ExampleClass4{
            public int paramint = 12;
            public Double paramDouble = -0.7d;
            public LinkedList<String> paramLinkedList = new LinkedList<>();
            {paramLinkedList.add("raz dwa trzy");}
            public Integer[] paramIntegerArray = {1,2};
        }
        ExampleClass4 exampleClass4Instance = new ExampleClass4();
        String expectedResult = "INSERT INTO ExampleClass4(paramint,paramDouble) VALUES(12,\"-0,7\");";
        Assert.assertEquals(expectedResult, sqlh.insert(exampleClass4Instance));
    }
    
    @Test
    public void canCopeWithStaticFields(){
        ExampleClass5 exampleClass5Instance = new ExampleClass5();
        String expectedInsertResult = "INSERT INTO ExampleClass5(paramint,paramDouble) VALUES(12,\"-0,7\");";
        Assert.assertEquals(expectedInsertResult, sqlh.insert(exampleClass5Instance));

        String expectedCreateTableResult = "CREATE TABLE IF NOT EXISTS ExampleClass5(paramint INTEGER,paramDouble REAL);";
        Assert.assertEquals(expectedCreateTableResult, sqlh.createTable(exampleClass5Instance));
    }


    @Test
    public void canCopeWithObjectCreatedFromNonDefaultConstructor(){
        ExampleClass5 exampleClass5Instance = new ExampleClass5(999d);
        String expectedInsertResult = "INSERT INTO ExampleClass5(paramint,paramDouble)" +
                " VALUES(12,\"999,0\");";
        Assert.assertEquals(expectedInsertResult, sqlh.insert(exampleClass5Instance));

        String expectedCreateTableResult = "CREATE TABLE IF NOT EXISTS ExampleClass5(paramint INTEGER,paramDouble REAL);";
        Assert.assertEquals(expectedCreateTableResult, sqlh.createTable(exampleClass5Instance));
    }

}

class ExampleClass5 {
    static public int paramint = 12;
    public Double paramDouble = -0.7d;
    static public Integer[] paramIntegerArray = {1,2};
    private String paramInteger = "mnie nie ma";
    Float paramLong = 1f;
    protected double paramfloat = 0.99999d;
    ExampleClass5(){

    }
    ExampleClass5(Double paramDouble){
        this.paramDouble = paramDouble;
    }
}