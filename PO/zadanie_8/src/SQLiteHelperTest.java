import org.junit.Assert;
import org.junit.Test;


public class SQLiteHelperTest {

    SQLiteHelper sqlh = new SQLiteHelper();

    @Test
    public void canAddKeyAdnotation(){
        String expectedString = "CREATE TABLE IF NOT EXISTS ExampleClass(paramint INTEGER," +
                "paramDouble REAL,primaryKey INTEGER NOT NULL PRIMARY KEY);";
        String generatedString = sqlh.createTable(new ExampleClass());
        Assert.assertEquals(expectedString, generatedString);
    }

    @Test
    public void canAddKeyAdnotationWithAutoIncrement(){
        class ExampleClass2{
            public String paramText;
            @KeyAnnotation(autoIncrement = true)
            public Integer primaryKey;
        }
        String expectedString = "CREATE TABLE IF NOT EXISTS ExampleClass2(paramText TEXT," +
                "primaryKey INTEGER PRIMARY KEY AUTOINCREMENT);";
        String generatedString = sqlh.createTable(new ExampleClass2());
        Assert.assertEquals(expectedString, generatedString);
    }

    @Test
    public void canAddIndexAdnotations(){
        class ExampleClass2{
            @IndexAnnotation(indexName = "uniqueIndexName", isUnique = true)
            public String indexedText;
            @IndexAnnotation(indexName = "indexName", isUnique = false)
            public int indexedInt;

        }
        String expectedString = "CREATE TABLE IF NOT EXISTS ExampleClass2(indexedText TEXT,indexedInt INTEGER);" +
                "CREATE UNIQUE INDEX uniqueIndexName ON ExampleClass2 (indexedText);" +
                "CREATE INDEX indexName ON ExampleClass2 (indexedInt);";
        String generatedString = sqlh.createTable(new ExampleClass2());
        Assert.assertEquals(expectedString, generatedString);
    }

    @Test
    public void canAddIndexAdnotationsForMultipleColumns(){
        class ExampleClass2{
            @IndexAnnotation(indexName = "uniqueIndexName", isUnique = true)
            public String indexedText1;
            @IndexAnnotation(indexName = "uniqueIndexName", isUnique = true)
            public String indexedText2;
            @IndexAnnotation(indexName = "uniqueIndexName", isUnique = true)
            public String indexedText3;
            @IndexAnnotation(indexName = "indexName", isUnique = false)
            public int indexedInt;

        }
        String expectedString = "CREATE TABLE IF NOT EXISTS ExampleClass2(indexedText1 TEXT," +
                "indexedText2 TEXT,indexedText3 TEXT,indexedInt INTEGER);" +
                "CREATE UNIQUE INDEX uniqueIndexName ON ExampleClass2 (indexedText1,indexedText2,indexedText3);" +
                "CREATE INDEX indexName ON ExampleClass2 (indexedInt);";
        String generatedString = sqlh.createTable(new ExampleClass2());
        Assert.assertEquals(expectedString, generatedString);
    }

    @Test
    public void canAddForeinKeyAdnotation(){
        class ExampleClass2{
            public String paramText;
            @ForeignKeyAnnotation(foreignKeyName = "Fkey", foreignColumnName = "FColName", foreignTableName = "FTabName")
            public Integer foreignKey;
        }
        String expectedString = "CREATE TABLE IF NOT EXISTS ExampleClass2(paramText TEXT,foreignKey INTEGER" +
                ",FOREIGN KEY (foreignKey) REFERENCES FTabName (FColName));";
        String generatedString = sqlh.createTable(new ExampleClass2());
        Assert.assertEquals(expectedString, generatedString);
    }

    @Test
    public void canAddMultipleForeinKeyAdnotation(){
        class ExampleClass2{
            @ForeignKeyAnnotation(foreignKeyName = "Fkey1", foreignColumnName = "FColName1", foreignTableName = "FTabName1")
            public String paramText;
            @ForeignKeyAnnotation(foreignKeyName = "Fkey", foreignColumnName = "FColName", foreignTableName = "FTabName")
            public Integer foreignKey;
        }
        String expectedString = "CREATE TABLE IF NOT EXISTS ExampleClass2(paramText TEXT,foreignKey INTEGER" +
                ",FOREIGN KEY (paramText) REFERENCES FTabName1 (FColName1)" +
                ",FOREIGN KEY (foreignKey) REFERENCES FTabName (FColName));";
        String generatedString = sqlh.createTable(new ExampleClass2());
        Assert.assertEquals(expectedString, generatedString);
    }

    @Test
    public void canGenerateInsertWithoutKeyAnnotation(){
        class ExampleClass2{
            @IndexAnnotation(isUnique = true, indexName = "abc")
            public String paramText = "aye";
            @ForeignKeyAnnotation(foreignKeyName = "Fkey", foreignColumnName = "FColName", foreignTableName = "FTabName")
            public Integer paramInt = 1;
        }
        String expectedString = "INSERT INTO ExampleClass2(paramText,paramInt) VALUES(\"aye\",1);";
        String generatedString = sqlh.insert(new ExampleClass2());
        Assert.assertEquals(expectedString, generatedString);
    }

    @Test
    public void canGenerateInsertOrUpdateWithoutAutoIncrement(){
        class ExampleClass2{
            public String paramText = "aye";
            public boolean parambool = true;
            @KeyAnnotation(autoIncrement = false)
            public Integer keyParam = 0;
            ExampleClass2(Integer param){
                this.keyParam =param;
            }
        }
        String expectedString = "INSERT INTO ExampleClass2(paramText,parambool,keyParam) VALUES(\"aye\",1,0);";
        String generatedString = sqlh.insert(new ExampleClass2(0));
        Assert.assertEquals(expectedString, generatedString);
        expectedString = "UPDATE ExampleClass2 SET paramText=\"aye\",parambool=1 WHERE keyParam=44;";
        generatedString = sqlh.insert(new ExampleClass2(44));
        Assert.assertEquals(expectedString, generatedString);
    }

    @Test
    public void canGenerateInserOrUpdatetWithAutoIncrement(){
        class ExampleClass2{
            public String paramText = "aye";
            public boolean parambool = true;
            @KeyAnnotation(autoIncrement = true)
            public Integer keyParam = 0;
            ExampleClass2(Integer param){
                this.keyParam =param;
            }
        }
        String expectedString = "INSERT INTO ExampleClass2(paramText,parambool) VALUES(\"aye\",1);";
        String generatedString = sqlh.insert(new ExampleClass2(0));
        Assert.assertEquals(expectedString, generatedString);
        expectedString = "UPDATE ExampleClass2 SET paramText=\"aye\",parambool=1" +
                " WHERE keyParam=44;";
        generatedString = sqlh.insert(new ExampleClass2(44));
        Assert.assertEquals(expectedString, generatedString);
    }

}

class ExampleClass {
    static public int paramint = 12;
    public Double paramDouble = -0.7d;
    @KeyAnnotation(autoIncrement = false)
    public int primaryKey;
    static public Integer[] paramIntegerArray = {1,2};
    private String paramInteger = "mnie nie ma";
    Float paramLong = 1f;
    protected double paramfloat = 0.99999d;
    ExampleClass(){
    }
    ExampleClass(Double paramDouble){
        this.paramDouble = paramDouble;
    }
}