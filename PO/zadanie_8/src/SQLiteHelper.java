import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

public class SQLiteHelper implements SQLiteHelperInterface {
    private static final String[] integerTypes = {"int", "long", "java.lang.Integer", "java.lang.Long"};
    private static final String[] realTypes = {"float", "double", "java.lang.Float", "java.lang.Double"};
    private static final String[] textTypes = {"java.lang.String"};
    private static final String[] booleanTypes = {"boolean", "java.lang.Boolean"};

    @Override
    public String createTable(Object o) {
        return "CREATE TABLE IF NOT EXISTS " + getClassName(o) + "(" +
                Arrays.stream(getPublicFields(o))
                        .filter(field -> getSQLiteType(field) != null)
                        .map(this::getCreateTableRecord)
                        .collect(Collectors.joining(",")) +
                Arrays.stream(getPublicFields(o))
                        .filter(field -> getSQLiteType(field) != null)
                        .filter(field -> field.getAnnotation(ForeignKeyAnnotation.class) != null)
                        .map(field -> "," + getForeignKeyAnnotationCreateRecord(field))
                        .collect(Collectors.joining()) +
        ");" + getIndexStatements(o);
    }

    @Override
    public String insert(Object o) {
        Field keyField = getKeyField(o);
        if (keyField == null)
            return getInsertRecord(o);
        if (!getSQLiteValue(keyField, o).equals("0"))
            return getUpdateRecordWithKey(o);
        else
            return (keyField.getAnnotation(KeyAnnotation.class).autoIncrement())
                    ? getInsertRecordWithAutoIncrementKey(o) : getInsertRecord(o);
    }

    private Field getKeyField(Object o) {
        Field keyField = null;
        for (Field field: getPublicFields(o)) {
            if (field.getAnnotation(KeyAnnotation.class) != null)
                keyField = field;
        }
        return keyField;
    }

    private String getInsertRecord(Object o){
        return "INSERT INTO " + getClassName(o) + "(" +
                Arrays.stream(getPublicFields(o))
                        .filter(field -> getSQLiteType(field) != null)
                        .map(Field::getName)
                        .collect(Collectors.joining(",")) +
                ") " + "VALUES(" +
                Arrays.stream(getPublicFields(o))
                        .filter(field -> getSQLiteType(field) != null)
                        .map(field -> getSQLiteValue(field, o))
                        .collect(Collectors.joining(",")) +
                ");";
    }

    private String getUpdateRecordWithKey(Object o){
        return "UPDATE " + getClassName(o) + " SET " +
                Arrays.stream(getPublicFields(o))
                        .filter(field -> getSQLiteType(field) != null)
                        .filter(field -> field.getAnnotation(KeyAnnotation.class) == null)
                        .map(field -> field.getName() + "=" + getSQLiteValue(field, o))
                        .collect(Collectors.joining(",")) +
                " WHERE " + getKeyField(o).getName() + "=" + getSQLiteValue(getKeyField(o), o) +";";
    }

    private String getInsertRecordWithAutoIncrementKey(Object o){
        return "INSERT INTO " + getClassName(o) + "(" +
                Arrays.stream(getPublicFields(o))
                        .filter(field -> getSQLiteType(field) != null)
                        .filter(field -> field.getAnnotation(KeyAnnotation.class) == null)
                        .map(Field::getName)
                        .collect(Collectors.joining(",")) +
                ") " + "VALUES(" +
                Arrays.stream(getPublicFields(o))
                        .filter(field -> getSQLiteType(field) != null)
                        .filter(field -> field.getAnnotation(KeyAnnotation.class) == null)
                        .map(field -> getSQLiteValue(field, o))
                        .collect(Collectors.joining(",")) +
                ");";
    }

    private Field[] getPublicFields(Object inspectedObject){
        Field[] publicFields = {};
        try {
            publicFields = Class.forName(inspectedObject.getClass().getName()).getFields();
        }
        catch ( ClassNotFoundException e ) { System.out.println(e); }
        return publicFields;
    }

    private String getClassName(Object inspectedObject){
        String className = "";
        try {
            className = Class.forName(inspectedObject.getClass().getName()).getSimpleName();
        }
        catch ( ClassNotFoundException e ) { System.out.println(e); }
        return className;
    }

    private String getIndexStatements(Object o){
        Field[] publicFields = getPublicFields(o);
        String className = getClassName(o);
        StringBuilder resultStatement = new StringBuilder(256);
        Set<IndexAnnotation> annotationSet = new LinkedHashSet<>();
        Arrays.stream(publicFields)
                .filter(field -> field.getAnnotation(IndexAnnotation.class) != null)
                .forEach(field -> annotationSet.add(field.getAnnotation(IndexAnnotation.class)));
        for (IndexAnnotation annotation: annotationSet){
            resultStatement
                    .append(annotation.isUnique() ? "CREATE UNIQUE INDEX " : "CREATE INDEX ")
                    .append(annotation.indexName())
                    .append(" ON ").append(className).append(" (")
                    .append(Arrays.stream(publicFields)
                        .filter(field -> getSQLiteType(field) != null)
                        .filter(field -> field.getAnnotation(IndexAnnotation.class) != null)
                        .filter(field -> field.getAnnotation(IndexAnnotation.class).indexName().equals(annotation.indexName()))
                        .map(Field::getName)
                        .collect(Collectors.joining(",")))
                    .append(");");
        }
        return resultStatement.toString();
    }

    private String getCreateTableRecord(Field field){
        if (null != field.getAnnotation(KeyAnnotation.class))
            return getKeyAnnotationCreateRecord(field);
        return field.getName() + " " + getSQLiteType(field);
    }

    private String getKeyAnnotationCreateRecord(Field field) {
        if (field.getAnnotation(KeyAnnotation.class).autoIncrement())
            return field.getName() + " " + getSQLiteType(field) + " PRIMARY KEY AUTOINCREMENT";
        else
            return field.getName() + " " + getSQLiteType(field) + " NOT NULL PRIMARY KEY";
    }

    private String getForeignKeyAnnotationCreateRecord(Field field){
        ForeignKeyAnnotation annotation = field.getAnnotation(ForeignKeyAnnotation.class);
        return "FOREIGN KEY ("+ field.getName() + ") " +
                "REFERENCES " + annotation.foreignTableName() + " (" + annotation.foreignColumnName() + ")";
    }

    private String getSQLiteValue(Field field, Object o){
        String sqliteValue = null;
        try {
            Object value = field.get(o);
            String fieldType = value.getClass().getTypeName();
            if (Arrays.asList(integerTypes).contains(fieldType))
                sqliteValue = value.toString();
            if (Arrays.asList(realTypes).contains(fieldType))
                sqliteValue = "\"" + value.toString().replace(".", ",") + "\"";
            if (Arrays.asList(textTypes).contains(fieldType))
                sqliteValue = "\"" + value + "\"";
            if (Arrays.asList(booleanTypes).contains(fieldType))
                sqliteValue = (Boolean)value ? "1" : "0";
        } catch( IllegalAccessException e ) { System.out.println(e);}
        return sqliteValue;
    }

    private static String getSQLiteType(Field field){
        String fieldType = field.getType().getTypeName();
        if (Arrays.asList(integerTypes).contains(fieldType) || Arrays.asList(booleanTypes).contains(fieldType))
            return "INTEGER";
        if (Arrays.asList(realTypes).contains(fieldType))
            return "REAL";
        if (Arrays.asList(textTypes).contains(fieldType))
            return "TEXT";
        return null;
    }
}
