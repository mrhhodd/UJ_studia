import javax.naming.OperationNotSupportedException;
import java.util.*;

public class VariablesDeclarations implements VariablesDeclarationsInterface {
    private Boolean isLocked = false;
    private List<DeclarationObserverInterface> activeObservers = new LinkedList<>();
    private Map<String, DataType> declaredVariables = new HashMap<>();

    @Override
    public void addObserver(DeclarationObserverInterface observer) {
        activeObservers.add(observer);
    }

    @Override
    public void addObserverAndSendInfo(DeclarationObserverInterface observer) {
        this.addObserver(observer);
        declaredVariables.forEach(observer::newVariable);
        if (isLocked())
            observer.declarationsLocked();
    }

    @Override
    public void removeObserver(DeclarationObserverInterface observer) {
        activeObservers.remove(observer);
    }

    @Override
    public void lockTypes() {
        this.isLocked = true;
        this.activeObservers.forEach(DeclarationObserverInterface::declarationsLocked);
    }

    @Override
    public boolean isLocked() {
        return this.isLocked;
    }

    @Override
    public Map<String, DataType> getDeclaredVariables() {
        return new HashMap<>(this.declaredVariables);
    }

    @Override
    public void link(String variableName, DataType type, boolean force)
            throws VariableAlreadyExistsException, OperationNotSupportedException, InvalidCastException {
        if (this.isLocked() || !Optional.ofNullable(type).isPresent())
            throw new OperationNotSupportedException();
        if (Optional.ofNullable(this.getDeclaredVariables().get(variableName)).isPresent() && !force)
            throw new VariableAlreadyExistsException();

        if (Optional.ofNullable(this.getDeclaredVariables().get(variableName)).isPresent() && force) {
            if (type == this.getDeclaredVariables().get(variableName)) {
                this.activeObservers.forEach(obs -> obs.removeVariable(variableName, type));
                this.activeObservers.forEach(obs -> obs.newVariable(variableName, type));
            } else
                this.tryToChangeVariable(variableName, this.getDeclaredVariables().get(variableName), type);
        }
        else
            this.activeObservers.forEach(obs -> obs.newVariable(variableName, type));
        this.declaredVariables.put(variableName, type);
    }

    private void tryToChangeVariable(String variableName, DataType oldType, DataType newType)
            throws InvalidCastException{
        boolean changeFailed = false;
        for (DeclarationObserverInterface observer : this.activeObservers) {
            try {
                observer.typeChange(variableName, oldType, newType);
            } catch (InvalidCastException e) {changeFailed = true;}
        }
        if (changeFailed)
            throw(new InvalidCastException());
    }

    @Override
    public void removeVariable(String variableName) {
        this.declaredVariables.remove(variableName);
        this.activeObservers.forEach(obs -> obs.removeVariable(variableName, this.declaredVariables.get(variableName)));
    }

    @Override
    public Optional<DataType> getType(String variableName) {
        return Optional.ofNullable(this.declaredVariables.get(variableName));
    }
}