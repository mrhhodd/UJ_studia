import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.naming.OperationNotSupportedException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class VariablesDeclarationsTest {

    VariablesDeclarationsInterface vd = new VariablesDeclarations();

    @Before
    public void testSetup(){
        try {
            vd.link("varBool1", VariablesDeclarationsInterface.DataType.BOOLEAN, false);
            vd.link("varInt1", VariablesDeclarationsInterface.DataType.INT, false);
            vd.link("varInt2", VariablesDeclarationsInterface.DataType.INT, false);
        } catch (VariablesDeclarationsInterface.VariableAlreadyExistsException |
                 OperationNotSupportedException |
                 VariablesDeclarationsInterface.InvalidCastException e){};
    }


    @Test(timeout = 500)
    public void declaredVariablesCantBeChangedFromOutside(){
        Map<String, VariablesDeclarationsInterface.DataType> firstVariables = vd.getDeclaredVariables();
        firstVariables.remove("varInt1");
        Assert.assertNotEquals(firstVariables, vd.getDeclaredVariables());
    }

    @Test(timeout = 500)
    public void getTypeReturnsValidType(){
        Assert.assertEquals(vd.getType("varBool1"),
                Optional.ofNullable(VariablesDeclarationsInterface.DataType.BOOLEAN));
        Assert.assertEquals(vd.getType("varInt1"),
                Optional.ofNullable(VariablesDeclarationsInterface.DataType.INT));
        Assert.assertEquals(vd.getType("varBool2"),
                Optional.ofNullable(null));
    }

    @Test(timeout = 500)
    public void canHandleRemovingNonExistentVariable(){
        vd.removeVariable("I dont exist");
    }



}
