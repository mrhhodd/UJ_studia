import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class ImageGenerator implements ImageGeneratorConfigurationInterface, ImageGeneratorInterface, ImageGeneratorPenInterface {
    private enum Direction {
        UP, LEFT, DOWN, RIGHT, STAY;
        static Direction getInversedDirection(Direction dir){
            Direction reversedDirection = STAY;
            switch (dir){
                case UP:
                    reversedDirection = DOWN;
                    break;
                case DOWN:
                    reversedDirection = UP;
                    break;
                case LEFT:
                    reversedDirection = RIGHT;
                    break;
                case RIGHT:
                    reversedDirection = LEFT;
                    break;
            }
            return reversedDirection;
        }
    }

    private class DrawingStep {
        Boolean previousState = canvas[cursorPositionY][cursorPositionX];
        Boolean previousColor = currentPenColor;
        PenState previousPenState = currentPenState;

        Direction newStepDirection = Direction.STAY;
        Boolean newColor = currentPenColor;
        PenState newPenState = currentPenState;

        DrawingStep(Direction newStepDirection){
            this.newStepDirection = newStepDirection;
        }

        DrawingStep(Boolean newColor, PenState newPenState){
            this.newColor = newColor;
            this.newPenState = newPenState;
        }
    }

    class LimitedLinkedList<E> extends LinkedList<E> {
        int maximumSize;

        LimitedLinkedList(int maximumSize) {
            this.maximumSize = maximumSize;
        }

        @Override
        public boolean add(E o) {
            while (this.size() >= maximumSize)
                super.remove();
            super.add(o);
            return true;
        }
    }

    public int cursorPositionX, cursorPositionY;
    public boolean canvas[][];
    public boolean currentPenColor = true;
    public PenState currentPenState = PenState.DOWN;

    public List<LinkedList<DrawingStep>> commandBuffer;
    public List<LinkedList<DrawingStep>> undoBuffer;

    @Override
    public void setCanvas(boolean[][] canvas) {
        this.canvas = canvas;
    }

    @Override
    public void maxUndoRedoRepeatCommands(int commands) {
        this.commandBuffer = new LimitedLinkedList<>(commands);
        this.undoBuffer = new LimitedLinkedList<>(commands);
    }

    @Override
    public void setInitialPosition(int col, int row) {
        this.cursorPositionX = row;
        this.cursorPositionY = col;
        drawIfPenIsDown();
    }

    @Override
    public void setPenState(PenState state) {
        changePenProperties(this.currentPenColor, state);
    }

    @Override
    public void setColor(boolean color) {
        changePenProperties(color, this.currentPenState);
    }

    private void changePenProperties(boolean newColor, PenState newPenState){
        LinkedList<DrawingStep> newSteps = new LinkedList<>();
        newSteps.add(new DrawingStep(newColor, newPenState));
        this.currentPenColor = newColor;
        this.currentPenState = newPenState;
        drawIfPenIsDown();
        this.commandBuffer.add(newSteps);
    }

    @Override
    public boolean getColor() {
        return this.currentPenColor;
    }

    @Override
    public PenState getPenState() {
        return this.currentPenState;
    }

    @Override
    public void up(int steps) {
        drawInDirection(steps, Direction.UP);
    }

    @Override
    public void down(int steps) {
        drawInDirection(steps, Direction.DOWN);
    }

    @Override
    public void left(int steps) {
        drawInDirection(steps, Direction.LEFT);
    }

    @Override
    public void right(int steps) {
        drawInDirection(steps, Direction.RIGHT);
    }

    private void drawInDirection(int numberOfSteps, Direction direction) {
        LinkedList<DrawingStep> newSteps = new LinkedList<>();
        for (int i = 0; i < numberOfSteps; i++) {
            changeCursorPositionInDirection(direction);
            newSteps.add(new DrawingStep(direction));
            drawIfPenIsDown();
        }
        this.commandBuffer.add(newSteps);
    }

    private void drawIfPenIsDown(){
        if (this.currentPenState == PenState.DOWN)
            this.canvas[this.cursorPositionY][cursorPositionX] = this.currentPenColor;
    }

    private void changeCursorPositionInDirection(Direction direction) {
        switch (direction) {
            case UP:
                this.cursorPositionX += 1;
                break;
            case DOWN:
                this.cursorPositionX -= 1;
                break;
            case RIGHT:
                this.cursorPositionY += 1;
                break;
            case LEFT:
                this.cursorPositionY -= 1;
        }
    }

    @Override
    public void undo(int commands) {
        int currentIndex;
        for (int i=0; i<commands; i++){
            currentIndex = this.commandBuffer.size() - 1;
            this.undoBuffer.add(this.commandBuffer.get(currentIndex));
            restorePreviousCellState(this.commandBuffer.get(currentIndex));
            this.commandBuffer.remove(currentIndex);
        }
    }

    private void restorePreviousCellState(LinkedList<DrawingStep> steps) {
        DrawingStep tmpStep;
        for (int i = steps.size() - 1; i >= 0; i--) {
            tmpStep = steps.get(i);
            this.canvas[this.cursorPositionY][this.cursorPositionX] = tmpStep.previousState;
            this.currentPenState = tmpStep.previousPenState;
            this.currentPenColor = tmpStep.previousColor;
            changeCursorPositionInDirection(Direction.getInversedDirection(tmpStep.newStepDirection));

        }
    }

    @Override
    public void redo(int commands) {
        int currentIndex;
        for (int i=0; i<commands; i++){
            currentIndex = this.undoBuffer.size() - 1;
            this.commandBuffer.add(this.undoBuffer.get(currentIndex));
            redoFromUndoBuffer(this.undoBuffer.get(currentIndex));
            this.undoBuffer.remove(currentIndex);
        }
    }

    private void redoFromUndoBuffer(LinkedList<DrawingStep> steps) {
        DrawingStep tmpStep;
        for (int i = 0; i < steps.size(); i++) {
            tmpStep = steps.get(i);
            this.currentPenState = tmpStep.newPenState;
            this.currentPenColor = tmpStep.newColor;
            changeCursorPositionInDirection(tmpStep.newStepDirection);
            drawIfPenIsDown();
        }
    }

    @Override
    public void repeat(int commands) {
        int currentIndex;
        LinkedList<DrawingStep> newDirections = new LinkedList<>();
        for (int i = commands; i > 0; i--) {
            currentIndex = this.commandBuffer.size() - i;
            newDirections.addAll(this.commandBuffer.get(currentIndex));
        }
        repeatSteps(newDirections);

    }

    private void repeatSteps(LinkedList<DrawingStep> templateSteps) {
        LinkedList<DrawingStep> newSteps = new LinkedList<>();
        for (DrawingStep step: templateSteps) {
            if (step.newStepDirection == Direction.STAY) {
                newSteps.add(new DrawingStep(step.newColor, step.newPenState));
                this.currentPenColor = step.newColor;
                this.currentPenState = step.newPenState;
            }
            else{
                changeCursorPositionInDirection(step.newStepDirection);
                newSteps.add(new DrawingStep(step.newStepDirection));
            }
            drawIfPenIsDown();
        }
        this.commandBuffer.add(newSteps);
    }

    static public void showCanvas(boolean[][] canvas) {
        System.out.println();
        for (int i = canvas[0].length - 1; i >= 0; i--) {
            StringBuilder sb = new StringBuilder();
            sb.append(i);
            sb.append(" |");
            for (int j = 0; j < canvas.length; j++) {
                sb.append(canvas[j][i] ? "*|" : " |");
            }
            System.out.println(sb);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("   ");
        for (int j = 0; j < canvas.length; j++) {
            sb.append(j);
            sb.append(" ");
        }
    }
}
