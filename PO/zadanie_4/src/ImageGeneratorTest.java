import org.junit.Test;
import org.junit.Before;
import org.junit.*;
import org.junit.Assert;


public class ImageGeneratorTest {

    private ImageGenerator ig;
    boolean[][] canvas;

    @Before
    public void testSetup() {
        this.ig = new ImageGenerator();
        this.ig.maxUndoRedoRepeatCommands(10);
        this.canvas = new boolean[10][10];
        this.ig.setCanvas(this.canvas);
    }


    @Test(timeout = 500)

    public void canDrawInAllDirections() {
        ig.setInitialPosition(2, 3);
        ig.up(3);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[2][4] && this.canvas[2][5] && this.canvas[2][6];
        ig.left(1);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[1][6];
        ig.down(2);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[1][5] && this.canvas[1][4];
        ig.right(2);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[2][4] && this.canvas[3][4];
    }

    @Test
    public void canRepeatStep() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][1] && this.canvas[0][2] && !this.canvas[0][3];
        ig.repeat(1);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][3] && this.canvas[0][4] && !this.canvas[0][5];
    }

    @Test
    public void canRepeatMultipleSteps() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ig.right(3);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][1] && this.canvas[0][2] && this.canvas[1][2] && this.canvas[2][2] && this.canvas[3][2];
        ig.repeat(2);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[3][3] && this.canvas[3][4] && this.canvas[4][4] && this.canvas[5][4] && this.canvas[6][4];
        assert ig.commandBuffer.size() == 3;
    }

    @Test
    public void canUndoOneCommandNonRepeatCommand() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ig.right(3);
        ImageGenerator.showCanvas(this.canvas);
        ig.undo(1);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][1] && this.canvas[0][2] && !this.canvas[1][2] && !this.canvas[2][2] && !this.canvas[3][2];
    }

    @Test
    public void canUndoMultipleNonRepeatCommands() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ig.right(3);
        ig.up(2);
        ig.left(3);
        ImageGenerator.showCanvas(this.canvas);
        ig.undo(2);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][1] && this.canvas[0][2] && this.canvas[1][2] && this.canvas[2][2] && this.canvas[3][2];
        assert !this.canvas[0][4] && !this.canvas[1][4] && !this.canvas[2][4] && !this.canvas[3][4] && !this.canvas[3][3];
    }

    @Test
    public void canRedoOneNonRepeatCommand() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ig.right(3);
        ig.undo(1);
        ImageGenerator.showCanvas(this.canvas);
        ig.redo(1);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][1] && this.canvas[0][2] && this.canvas[1][2] && this.canvas[2][2] && this.canvas[3][2];
    }

    @Test
    public void canRedoMultipleNonRepeatCommands() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ig.right(3);
        ig.up(2);
        ig.left(3);
        ImageGenerator.showCanvas(this.canvas);
        assert ig.undoBuffer.size() == 0;
        assert ig.commandBuffer.size() == 4;
        ig.undo(2);
        assert ig.undoBuffer.size() == 2;
        assert ig.commandBuffer.size() == 2;
        ImageGenerator.showCanvas(this.canvas);
        ig.redo(2);
        assert ig.undoBuffer.size() == 0;
        assert ig.commandBuffer.size() == 4;
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][1] && this.canvas[0][2] && this.canvas[1][2] && this.canvas[2][2] && this.canvas[3][2];
        assert this.canvas[0][4] && this.canvas[1][4] && this.canvas[2][4] && this.canvas[3][4] && this.canvas[3][3];
    }

    @Test
    public void canUndoRepeatCommand() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ig.right(3);
        ImageGenerator.showCanvas(this.canvas);
        ig.repeat(2);
        ImageGenerator.showCanvas(this.canvas);
        ig.undo(1);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][1] && this.canvas[0][2] && this.canvas[1][2] && this.canvas[2][2] && this.canvas[3][2];
        assert !this.canvas[3][3] && !this.canvas[3][4] && !this.canvas[4][4] && !this.canvas[5][4] && !this.canvas[6][4];
    }

    @Test
    public void canUndoMultipleCommandsWithRepeat() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ig.right(3);
        ig.up(2);
        ig.left(2);
        ImageGenerator.showCanvas(this.canvas);
        ig.repeat(3);
        ImageGenerator.showCanvas(this.canvas);
        ig.undo(2);
        ImageGenerator.showCanvas(this.canvas);
    }

    @Test
    public void canRedoRepeatCommand() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ig.right(3);
        ig.repeat(2);
        ig.undo(2);
        ImageGenerator.showCanvas(this.canvas);
        ig.redo(2);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][1] && this.canvas[0][2] && this.canvas[1][2] && this.canvas[2][2] && this.canvas[3][2];
        assert this.canvas[3][3] && this.canvas[3][4] && this.canvas[4][4] && this.canvas[5][4] && this.canvas[6][4];
    }

    @Test
    public void canUndoTwice() {
        ig.setInitialPosition(0, 0);
        ig.up(2);
        ig.right(6);
        ig.up(3);
        ig.left(2);
        ig.repeat(2);
        ImageGenerator.showCanvas(this.canvas);
        ig.undo(2);
        ig.undo(1);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][0] && this.canvas[0][1] && this.canvas[0][2];
        assert this.canvas[1][2] && this.canvas[2][2] && this.canvas[3][2] && this.canvas[4][2] && this.canvas[5][2] && this.canvas[6][2];

    }

    @Test
    public void bufferSizesAreFixedMaxSize() {
        ig.setInitialPosition(0, 0);
        ig.maxUndoRedoRepeatCommands(5);
        ig.up(4);
        ig.right(3);
        ig.repeat(2);
        ig.up(1);
        ig.down(5);
        assert ig.commandBuffer.size() == 5;
        assert ig.undoBuffer.size() == 0;
        ig.repeat(3);
        assert ig.commandBuffer.size() == 5;
        assert ig.undoBuffer.size() == 0;
        ig.undo(3);
        assert ig.commandBuffer.size() == 2;
        assert ig.undoBuffer.size() == 3;
        ig.undo(2);
        assert ig.commandBuffer.size() == 0;
        assert ig.undoBuffer.size() == 5;
        ig.redo(5);
        assert ig.commandBuffer.size() == 5;
        assert ig.undoBuffer.size() == 0;
    }

    @Test(timeout = 500)
    public void whilePenIsUpItLeavesNoTraceOnCanvas() {
        ig.setInitialPosition(0, 0);
        ig.setPenState(ImageGeneratorPenInterface.PenState.UP);
        ig.up(2);
        ig.right(3);
        ig.repeat(2);
        ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
        ImageGenerator.showCanvas(this.canvas);
        assert this.canvas[0][0] && this.canvas[6][4];
        assert !this.canvas[0][1] && !this.canvas[0][2] && !this.canvas[1][2] && !this.canvas[2][2] && !this.canvas[3][2];
        assert !this.canvas[3][3] && !this.canvas[3][4] && !this.canvas[4][4] && !this.canvas[5][4];
    }



    @Test(timeout = 500)
    public void canDrawInFalseColorWhileChangingColorAndSettingInitialPosition(){
            ig.setInitialPosition(0,0);
            ig.up(2);
            ig.right(3);
            ig.repeat(2);
            ig.setColor(false);
            assert !this.canvas[6][4];
            ImageGenerator.showCanvas(this.canvas);
            assert this.canvas[3][2];
            assert this.canvas[0][1] && this.canvas[0][2] && this.canvas[1][2] && this.canvas[2][2];
            assert this.canvas[3][3] && this.canvas[3][4] && this.canvas[4][4] && this.canvas[5][4];
    }

    @Test(timeout = 500)
    public void canUndoChangingColor(){
            ig.setInitialPosition(0,0);
            ig.up(2);
            ig.right(3);
            ig.repeat(2);
            ig.setColor(false);
            Assert.assertEquals(false, ig.getColor());
            ig.left(5);
            ig.undo(2);
            Assert.assertEquals(true, ig.getColor());
            ImageGenerator.showCanvas(this.canvas);
            assert this.canvas[0][0] && this.canvas[0][1] && this.canvas[0][2] && this.canvas[1][2] && this.canvas[2][2] && this.canvas[3][2];
            assert this.canvas[3][3] && this.canvas[3][4] && this.canvas[4][4] && this.canvas[5][4] && this.canvas[6][4];
    }

    @Test(timeout = 500)
    public void canRedoChangingColor(){
            ig.setInitialPosition(0,0);
            ig.setColor(false);
            Assert.assertEquals(false, ig.getColor());
            ig.up(5);
            ig.undo(2);
            Assert.assertEquals(true, ig.getColor());
            ig.redo(2);
            Assert.assertEquals(false, ig.getColor());
    }

    @Test(timeout = 500)
    public void canUndoChangingPenState(){
            ig.setInitialPosition(3,3);
            ig.up(3);
            ig.right(3);
            ig.setPenState(ImageGeneratorPenInterface.PenState.UP);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.UP);
            ig.down(5);
            ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.DOWN);
            ig.undo(1);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.UP);
            ig.up(6);
            ImageGenerator.showCanvas(this.canvas);
            assert this.canvas[3][3] && this.canvas[3][4] && this.canvas[3][5] && this.canvas[3][6];
            assert this.canvas[4][6] && this.canvas[5][6] && this.canvas[6][6];
            assert !this.canvas[6][2] && !this.canvas[6][3] && !this.canvas[6][4] && !this.canvas[6][5] && !this.canvas[6][7];
    }

    @Test(timeout = 500)
    public void canRedoChangingPenState(){
            ig.setInitialPosition(3,3);
            ig.setPenState(ImageGeneratorPenInterface.PenState.UP);
            ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
            ig.undo(1);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.UP);
            ig.redo(1);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.DOWN);
    }

    @Test(timeout = 500)
    public void canDrawLikeInExampleOnSite(){
            ig.setInitialPosition(2,0);
            ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
            ig.setColor(true);
            ig.up(4);
            ig.setColor(false);
            ig.down(2);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.DOWN);
            Assert.assertEquals(false, ig.getColor());
            assert canvas[2][0] && canvas[2][1];
            assert !canvas[2][2] && !canvas[2][3] && !canvas[2][4];
            ImageGenerator.showCanvas(canvas);
    }

    @Test(timeout = 500)
    public void canDrawLikeInExampleOnSite2(){
            ig.setInitialPosition(2,0);
            ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
            ig.setColor(true);
            ig.up(4);
            ImageGenerator.showCanvas(canvas);
            ig.setPenState(ImageGeneratorPenInterface.PenState.UP);
            ig.setColor(false);
            ImageGenerator.showCanvas(canvas);
            ig.down(2);
            ImageGenerator.showCanvas(canvas);
            ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.DOWN);
            Assert.assertEquals(false, ig.getColor());
            assert canvas[2][0] && canvas[2][1] && canvas[2][3] && canvas[2][4];
            assert !canvas[2][2];
            ImageGenerator.showCanvas(canvas);
    }

    @Test(timeout = 500)
    public void canRepeatSetColor(){
            ig.setInitialPosition(2,0);
            ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
            ig.setColor(false);
            ig.setColor(true);
            ig.up(4);
            ig.repeat(2);
            Assert.assertEquals(true, ig.getColor());
            ig.setColor(false);
            ig.undo(2);
            Assert.assertEquals(true, ig.getColor());
            ig.undo(2);
            Assert.assertEquals(false, ig.getColor());
    }

    @Test(timeout = 500)
    public void canRepeatSetPenState(){
            ig.setInitialPosition(2,0);
            ig.setPenState(ImageGeneratorPenInterface.PenState.UP);
            ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
            ig.up(4);
            ig.repeat(2);
            ImageGenerator.showCanvas(canvas);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.DOWN);
            ig.setPenState(ImageGeneratorPenInterface.PenState.UP);
            ig.undo(2);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.DOWN);
            ig.undo(2);
            Assert.assertEquals(ig.getPenState(), ImageGeneratorPenInterface.PenState.UP);
    }

    @Test(timeout = 500)
    public void dottedLine(){
            ig.setInitialPosition(2,0);
            ig.up(1);
            ig.setColor( false );
            ig.up(2);
            ig.setColor( true );
            ig.up(2);
            ImageGenerator.showCanvas(canvas);
            ig.repeat(4);
            ImageGenerator.showCanvas(canvas);
    }
}

