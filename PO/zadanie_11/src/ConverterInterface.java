
public interface ConverterInterface {
	/**
	 * Metoda pozwala ustawić obiekt odpowiedzialny za przechowywanie reguł
	 * konwersji typów.
	 * 
	 * @param conversions
	 *            reguły konwersji typów
	 */
	public void setTypeConversionInterface(TypeConversionsRulesInterface conversions);

	/**
	 * Metoda zwraca wynik konwersji danej typu long na int (o ile jest dozwolona).
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public int getAsInteger(long value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu double na int (o ile jest
	 * dozwolona). Konwersja dokonywana jest do najbliższej wartości całkowitej.
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public int getAsInteger(double value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu logicznego na int (o ile jest
	 * dozwolona). Wartości true odpowiadać ma 1 a false to 0.
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public int getAsInteger(boolean value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu int na long (o ile jest dozwolona).
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public long getAsLong(int value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu double na long (o ile jest
	 * dozwolona). Konwersja dokonywana jest do najbliższej wartości całkowitej.
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public long getAsLong(double value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu logicznego na long (o ile jest
	 * dozwolona). Wartości true ma odpowiadać 1L a wartości logicznej false 0L.
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public long getAsLong(boolean value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu int na double (o ile jest
	 * dozwolona).
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public double getAsDouble(int value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu long na double (o ile jest
	 * dozwolona).
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public double getAsDouble(long value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu logicznego na double (o ile jest
	 * dozwolona). Wartości true ma odpowiadać 1.0 a false to 0.0.
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public double getAsDouble(boolean value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu int na typ logiczny (o ile jest
	 * dozwolona). Wartości 0 ma odpowiadać false, każda inna wartość value to true.
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public boolean getAsBoolean(int value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu long na typ logiczny (o ile jest
	 * dozwolona). Wartości 0L ma odpowiadać false, każda inna wartość value to
	 * true.
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public boolean getAsBoolean(long value) throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Metoda zwraca wynik konwersji danej typu double na typ logiczny (o ile jest
	 * dozwolona). Wartości 0.0 ma odpowiadać false, każda inna wartość value to
	 * true.
	 * 
	 * @param value
	 *            wartość poddawana konwersji
	 * @return wynik konwersji o ile jest dozwolona
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             wyjątek zgłaszany gdy konwersja nie jest legalna
	 */
	public boolean getAsBoolean(double value) throws VariablesDeclarationsInterface.InvalidCastException;
}
