import java.util.Map;
import java.util.Optional;

import javax.naming.OperationNotSupportedException;

/**
 * Interfejs systemu umożliwiającego deklarację zmiennych. Ten system nie odpowiada
 * za przechowywanie wartości zmiennych lecz tylko powiązań nazw zmiennych z typami
 * danych.
 */
public interface VariablesDeclarationsInterface {

	/**
	 * Typ wyliczeniowy zawierający listę wszystkich wspieranych typów zmiennych.
	 */
	public enum DataType {
		INT, LONG, DOUBLE, BOOLEAN;
	}

	/**
	 * Interfejs obserwatora deklaracji - dzięki niemu zarejestrowani obserwatorzy
	 * są informowani o zmianach w deklaracjach zmiennych.
	 * 
	 */
	public interface DeclarationObserverInterface {
		/**
		 * Metoda informuje obserwatora o zadeklarowaniu nowej zmiennej.
		 * 
		 * @param variableName
		 *            nazwa nowej zmiennej
		 * @param type
		 *            typ danych
		 */
		public void newVariable(String variableName, DataType type);

		/**
		 * Metoda informuje obserwatora o usunięciu zmiennej.
		 * 
		 * @param variableName
		 *            nazwa usuniętej zmiennej
		 * @param type
		 *            typ danych jaki posiadała usunięta zmienna
		 */
		public void removeVariable(String variableName, DataType type);

		/**
		 * Metoda informuje obserwatora o zmianie typu zmiennej.
		 * 
		 * @param variableName
		 *            nazwa zmiennej, ktorej typ uległ zmianie
		 * @param oldType
		 *            stary typ zmiennej
		 * @param newType
		 *            nowy typ zmiennej
		 * @throws InvalidCastException
		 *             wyjątek zgłaszany gdy zmiana typu nie jest możliwa.
		 */
		public void typeChange(String variableName, DataType oldType, DataType newType) throws InvalidCastException;

		/**
		 * Proces deklaracji zmiennych został zablokowany - żadnych zmian w strukturze
		 * zmiennych już nie będzie.
		 */
		public void declarationsLocked();
	}

	/**
	 * Wyjątek generowany w przypadku próby deklaracji zmiennej o nazwie, która już
	 * istnieje w systemie.
	 */
	public class VariableAlreadyExistsException extends Exception {
		private static final long serialVersionUID = -1359266555417969831L;
	}

	/**
	 * Wyjątek generowany gdy konwersja pomiędzy typami danych jest niedozwolona.
	 *
	 */
	public class InvalidCastException extends Exception {
		private static final long serialVersionUID = 1437212007484257087L;
	}

	/**
	 * Metoda umożliwia deklarację zmiennej i zmianę typu zmiennej. Jeśli wywołanie
	 * metody link może doprowadzić do wyzwolenia odpowiednich metod przekazujących
	 * informację obserwatorom. I tak:
	 * <ul>
	 * <li>Jeśli zadeklarowano nową zmienną obserwatorzy informowani są o tym fakcie
	 * za pomocą metody newVariable.
	 * <li>Jeśli zmienna jest już zadeklarowana a parametr force otrzymał wartość
	 * false, to pojawa się wyjątek VariableAlreadyExistsException. Obserwatorzy nie
	 * są informowani.
	 * <li>Jeśli zmienna jest już zadeklarowana a parametr farce otrzymał wartość
	 * true, to:
	 * <ul>
	 * <li>Jeśli typ jest taki sam jak użyto wcześniej, to obserwatorzy informowani
	 * są za pomocą dwóch metod: removeVariable oraz newVariable - spowoduje to
	 * ponowne przypisanie zmiennej wartości początkowej.
	 * <li>Jeśli typ jest inny niż używany poprzednio, obserwatorzy informowani są
	 * za pomocą metody typeChange.
	 * </ul>
	 * </ul>
	 * 
	 * @param variableName
	 *            nazwa zmiennej
	 * @param type
	 *            typ danych przechowywanych w zmiennej
	 * @param force
	 *            wymuszenie zmiany typu lub ponownej deklaracji istniejącej
	 *            zmiennej. Wymuszona zmiana typu zawsze jest zapamiętywana nawet
	 *            jeśli dowolny z obserwatorów uzna ją za nielegalną (zgłosi wyjątek
	 *            InvalidCastException).
	 * @throws VariableAlreadyExistsException
	 *             zmienna juz istnieje
	 * @throws OperationNotSupportedException
	 *             operacja nie jest mozliwa do wykonania z powodu uruchomienia
	 *             blokady lub jako type wskazano NULL.
	 * 
	 * @throws InvalidCastException
	 *             z punktu widzenia co najmniej jednego obserwatora wymuszona
	 *             zmiana typu nie jest możliwa. W tej wersji systemu wyjątek ten
	 *             pojawia się wyłącznie wtedy, gdy zostanie zgłoszony przez
	 *             obserwatora - w takim przypadku metoda link przekazuje informację
	 *             do dalszych obserwatorów i kończy pracę zgłaszając omawiany
	 *             wyjątek. <br>
	 * 			Nawet jeśli dojdzie do zgłoszenia InvalidCastException wymuszona
	 *             zmiana typu jest zapamiętywana - za rozwiązanie problemu
	 *             odpowiada użytkownik. Sugerowane rozwiązanie: usunięcie zmiennej
	 *             i ponowna deklaracja.
	 */
	public void link(String variableName, DataType type, boolean force)
			throws VariableAlreadyExistsException, OperationNotSupportedException, InvalidCastException;

	/**
	 * Metoda usuwa zmienną o podanej nazwie (o ile istnieje). Informuje o tym
	 * fakcie obserwatorów.
	 * 
	 * @param variableName
	 *            nazwa usuwanej zmiennej
	 */
	public void removeVariable(String variableName);

	/**
	 * Metoda blokuje możliwość zmiany struktury zmiennych. Od chwili uruchomienia
	 * lockTypes nie będzie możliwe ani zadeklarowanie nowej zmiennej ani zmiana
	 * typu (nawet na taki sam) już istniejącej. Metoda link kończy się od chwili
	 * wykonania tej metody wyjątkiem OperationNotSupportedException.
	 */
	public void lockTypes();

	/**
	 * Metoda pozwalająca sprawdzić czy baza zmiennych jest zablokowana
	 * 
	 * @return true - blokada założona, false - brak blokady, to jest ustawienie
	 *         początkowe systemu
	 */
	public boolean isLocked();

	/**
	 * Metoda zwraca typ zmiennej o podanej nazwie, o ile taka zmienna istnieje.
	 * Jeśli jej nie ma zwracany jest obiekt Optional zawierający NULL.
	 * 
	 * @param variableName
	 *            nazwa zmiennej
	 * @return typ zmiennej variableName
	 */
	public Optional<DataType> getType(String variableName);

	/**
	 * Metoda zwraca mapę zawierającą informację o wszystkich zmiennych jakie
	 * zostały do chwili wykonania tej metody zadeklarowane. Uwaga: nie wolno
	 * zwracać oryginalnej mapy (o ile taka istnieje) aby użytkownik nie wprowadził
	 * bez wiedzy systemu nowej zmiennej.
	 * 
	 * @return mapa nazwa zmiennej na typ
	 */
	public Map<String, DataType> getDeclaredVariables();

	/**
	 * Metoda dodaje obserwatora.
	 * 
	 * @param observer
	 *            obiekt - obserwator
	 */
	public void addObserver(DeclarationObserverInterface observer);

	/**
	 * Metoda dodaje obserwatora i przekazuje do niego za pomocą metodu newVariable
	 * i declarationsLocked (o ile jest taka potrzeba) aktualny stan zadeklarowanych
	 * zmiennych.
	 * 
	 * @param observer
	 *            obserwator
	 */
	public void addObserverAndSendInfo(DeclarationObserverInterface observer);

	/**
	 * Metoda usuwa obserwatora. Od tej chwili obserwator nie jest informowany o
	 * zmianach w strukturze zmiennych.
	 * 
	 * @param observer
	 *            obserwator do usuniecia
	 */
	public void removeObserver(DeclarationObserverInterface observer);
}
