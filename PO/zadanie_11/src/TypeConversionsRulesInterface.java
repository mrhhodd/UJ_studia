
public interface TypeConversionsRulesInterface {
	/**
	 * Konwersja typu z from na to jest dozwolona.
	 * 
	 * @param from
	 *            wyjściowy typ danych
	 * @param to
	 *            docelowy typ danych
	 */
	public void allow(VariablesDeclarationsInterface.DataType from, VariablesDeclarationsInterface.DataType to);

	/**
	 * Konwersja typu z from na to jest zabroniona.
	 * 
	 * @param from
	 *            wyjściowy typ danych
	 * @param to
	 *            docelowy typ danych
	 */
	public void deny(VariablesDeclarationsInterface.DataType from, VariablesDeclarationsInterface.DataType to);

	/**
	 * Metoda zwraca prawde jesli konwersja typu z from na to jest dozwolona.
	 * 
	 * @param from
	 *            wyjściowy typ danych
	 * @param to
	 *            docelowy typ danych
	 * @return true - konwersja dozwolona, false - konwersja zabroniona
	 */
	public boolean isAllowed(VariablesDeclarationsInterface.DataType from, VariablesDeclarationsInterface.DataType to);

	/**
	 * Metoda pozwala sprawdzić czy konwersja z from do to jest niedozwolona. Jeśli
	 * konwersja jest legalna metoda kończy pracę bez sygnalizowania wyjątku,
	 * nielegalność konwersji typu sygnalizowana jest za pomocą wyjątku.
	 * 
	 * @param from
	 *            wyjściowy typ danych
	 * @param to
	 *            docelowy typ danych
	 * @throws VariablesDeclarationsInterface.InvalidCastException
	 *             - wyjątek używany do sygnalizacji nielegalności testowanej
	 *             konwersji.
	 */
	public void isNotAllowed(VariablesDeclarationsInterface.DataType from, VariablesDeclarationsInterface.DataType to)
			throws VariablesDeclarationsInterface.InvalidCastException;

	/**
	 * Reset ustawień do wartości domyślnych. Za ustawienia domyślne uznawane są
	 * takie, które bez wymuszenia konwersji typu automatycznie zostaną
	 * przeprowadzone przez Java np. int na double. Uwaga: z oczywistych względów
	 * konwersja typu na ten sam typ jest także dozwolona.
	 */
	public void setDefault();
}
