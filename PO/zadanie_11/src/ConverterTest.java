import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConverterTest {
    Converter converter = new Converter();
    TypeConversionsRulesInterface TCR = new TypeConversionsRules();

    private static final VariablesDeclarationsInterface.DataType INT = VariablesDeclarationsInterface.DataType.INT;
    private static final VariablesDeclarationsInterface.DataType DOUBLE = VariablesDeclarationsInterface.DataType.DOUBLE;
    private static final VariablesDeclarationsInterface.DataType LONG = VariablesDeclarationsInterface.DataType.LONG;
    private static final VariablesDeclarationsInterface.DataType BOOLEAN = VariablesDeclarationsInterface.DataType.BOOLEAN;

    boolean[] boolValues = {false, true};
    int[] intValues = {1, 2000000000, -300000000};
    long[] longValues = {1L, 20000000000L, -3000000000L};
    double[] doubleValues = {1D, 2.222D, -7.777D};

    @Before()
    public void canSetConversionRules(){
        converter.setTypeConversionInterface(TCR);
    }

    @Test(timeout = 500)
    public void canConvertToINT(){
        int exceptionCaught = 0;
        TCR.deny(BOOLEAN, INT);
        TCR.deny(LONG, INT);
        TCR.deny(DOUBLE, INT);
        for (boolean value: boolValues) {
            try {
                converter.getAsInteger(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(2, exceptionCaught);
        for (long value: longValues) {
            try {
                converter.getAsInteger(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(5, exceptionCaught);
        for (double value: doubleValues) {
            try {
                converter.getAsInteger(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(8, exceptionCaught);

        TCR.allow(BOOLEAN, INT);
        try {
            Assert.assertEquals(0, converter.getAsInteger(boolValues[0]));
            Assert.assertEquals(1, converter.getAsInteger(boolValues[1]));
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(8, exceptionCaught);

        TCR.allow(LONG, INT);
        try {
            Assert.assertEquals(1, converter.getAsInteger(longValues[0]));
            Assert.assertEquals((int)longValues[1], converter.getAsInteger(longValues[1]));
            Assert.assertEquals((int)longValues[2], converter.getAsInteger(longValues[2]));
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(8, exceptionCaught);

        TCR.allow(DOUBLE, INT);
        try {
            Assert.assertEquals((int)doubleValues[0], converter.getAsInteger(doubleValues[0]));
            Assert.assertEquals((int)doubleValues[1], converter.getAsInteger(doubleValues[1]));
            Assert.assertEquals(-8, converter.getAsInteger(doubleValues[2]));
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(8, exceptionCaught);
    }

    @Test(timeout = 500)
    public void canConvertToLONG(){
        int exceptionCaught = 0;
        TCR.deny(BOOLEAN, LONG);
        TCR.deny(INT, LONG);
        TCR.deny(DOUBLE, LONG);
        for (boolean value: boolValues) {
            try {
                converter.getAsLong(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(2, exceptionCaught);
        for (long value: intValues) {
            try {
                converter.getAsLong(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(5, exceptionCaught);
        for (double value: doubleValues) {
            try {
                converter.getAsLong(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(8, exceptionCaught);

        TCR.allow(BOOLEAN, LONG);
        try {
            Assert.assertEquals(0L, converter.getAsLong(boolValues[0]));
            Assert.assertEquals(1L, converter.getAsLong(boolValues[1]));
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(8, exceptionCaught);

        TCR.allow(INT, LONG);
        try {
            Assert.assertEquals(1, converter.getAsLong(intValues[0]));
            Assert.assertEquals(2000000000L, converter.getAsLong(intValues[1]));
            Assert.assertEquals(-300000000L, converter.getAsLong(intValues[2]));
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(8, exceptionCaught);

        TCR.allow(DOUBLE, LONG);
        try {
            Assert.assertEquals((long)doubleValues[0], converter.getAsLong(doubleValues[0]));
            Assert.assertEquals((long)doubleValues[1], converter.getAsLong(doubleValues[1]));
            Assert.assertEquals(-8L, converter.getAsLong(doubleValues[2]));
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(8, exceptionCaught);
    }

    @Test(timeout = 500)
    public void canConvertToDOUBLE(){
        int exceptionCaught = 0;
        TCR.deny(BOOLEAN, DOUBLE);
        TCR.deny(INT, DOUBLE);
        TCR.deny(LONG, DOUBLE);
        for (boolean value: boolValues) {
            try {
                converter.getAsDouble(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(2, exceptionCaught);
        for (int value: intValues) {
            try {
                converter.getAsDouble(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(5, exceptionCaught);
        for (long value: longValues) {
            try {
                converter.getAsDouble(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(8, exceptionCaught);

        TCR.allow(BOOLEAN, DOUBLE);
        try {
            Assert.assertEquals(0.0, converter.getAsDouble(boolValues[0]), 0);
            Assert.assertEquals(1.0, converter.getAsDouble(boolValues[1]), 0);
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(8, exceptionCaught);

        TCR.allow(INT, DOUBLE);
        try {
            Assert.assertEquals(1.0, converter.getAsDouble(intValues[0]), 0);
            Assert.assertEquals(2000000000.0, converter.getAsDouble(intValues[1]), 0);
            Assert.assertEquals(-300000000.0, converter.getAsDouble(intValues[2]), 0);
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(8, exceptionCaught);

        TCR.allow(LONG, DOUBLE);
        try {
            Assert.assertEquals(1.0, converter.getAsDouble(longValues[0]), 0);
            Assert.assertEquals(20000000000.0, converter.getAsDouble(longValues[1]), 0);
            Assert.assertEquals(-3000000000.0, converter.getAsDouble(longValues[2]), 0);
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(8, exceptionCaught);
    }

    @Test(timeout = 500)
    public void canConvertToBOOLEAN(){
        int exceptionCaught = 0;
        TCR.deny(DOUBLE, BOOLEAN);
        TCR.deny(INT, BOOLEAN);
        TCR.deny(LONG, BOOLEAN);
        for (double value: doubleValues) {
            try {
                converter.getAsBoolean(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(3, exceptionCaught);
        for (int value: intValues) {
            try {
                converter.getAsBoolean(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(6, exceptionCaught);
        for (long value: longValues) {
            try {
                converter.getAsBoolean(value);
            } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        }
        Assert.assertEquals(9, exceptionCaught);

        TCR.allow(DOUBLE, BOOLEAN);
        try {
            Assert.assertEquals(true, converter.getAsBoolean(doubleValues[0]));
            Assert.assertEquals(true, converter.getAsBoolean(doubleValues[1]));
            Assert.assertEquals(true, converter.getAsBoolean(doubleValues[2]));
            Assert.assertEquals(false, converter.getAsBoolean(0.0D));
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(9, exceptionCaught);

        TCR.allow(INT, BOOLEAN);
        try {
            Assert.assertEquals(true, converter.getAsBoolean(intValues[0]));
            Assert.assertEquals(true, converter.getAsBoolean(intValues[1]));
            Assert.assertEquals(true, converter.getAsBoolean(intValues[2]));
            Assert.assertEquals(false, converter.getAsBoolean(0));
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(9, exceptionCaught);

        TCR.allow(LONG, BOOLEAN);
        try {
            Assert.assertEquals(true, converter.getAsBoolean(longValues[0]));
            Assert.assertEquals(true, converter.getAsBoolean(longValues[1]));
            Assert.assertEquals(true, converter.getAsBoolean(longValues[2]));
            Assert.assertEquals(false, converter.getAsBoolean(0L));
        } catch (VariablesDeclarationsInterface.InvalidCastException e) {exceptionCaught++;}
        Assert.assertEquals(9, exceptionCaught);
    }
}
