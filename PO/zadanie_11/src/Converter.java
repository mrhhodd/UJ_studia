import java.util.*;

public class Converter implements ConverterInterface {
    private TypeConversionsRulesInterface conversionRules;
    @Override
    public void setTypeConversionInterface(TypeConversionsRulesInterface conversions) {
        this.conversionRules = conversions;
    }

    @Override
    public boolean getAsBoolean(int value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.INT, TypeConversionsRules.BOOLEAN);
        return value != 0;
    }

    @Override
    public boolean getAsBoolean(long value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.LONG, TypeConversionsRules.BOOLEAN);
        return value != 0L;
    }

    @Override
    public boolean getAsBoolean(double value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.DOUBLE, TypeConversionsRules.BOOLEAN);
        return value != 0.0;
    }

    @Override
    public double getAsDouble(int value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.INT, TypeConversionsRules.DOUBLE);
        return (double)value;
    }

    @Override
    public double getAsDouble(long value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.LONG, TypeConversionsRules.DOUBLE);
        return (double)value;
    }

    @Override
    public double getAsDouble(boolean value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.BOOLEAN, TypeConversionsRules.DOUBLE);
        return value ? 1.0 : 0.0;
    }

    @Override
    public int getAsInteger(long value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.LONG, TypeConversionsRules.INT);
        return (int)value;
    }

    @Override
    public int getAsInteger(double value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.DOUBLE, TypeConversionsRules.INT);
        return (int)Math.round(value);
    }

    @Override
    public int getAsInteger(boolean value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.BOOLEAN, TypeConversionsRules.INT);
        return value ? 1 : 0;
    }

    @Override
    public long getAsLong(int value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.INT, TypeConversionsRules.LONG);
        return (long)value;
    }

    @Override
    public long getAsLong(double value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.DOUBLE, TypeConversionsRules.LONG);
        return Math.round(value);
    }

    @Override
    public long getAsLong(boolean value) throws VariablesDeclarationsInterface.InvalidCastException {
        this.conversionRules.isNotAllowed(TypeConversionsRules.BOOLEAN, TypeConversionsRules.LONG);
        return value ? 1L : 0L;
    }
}

class TypeConversionsRules implements TypeConversionsRulesInterface {
    static final VariablesDeclarationsInterface.DataType INT = VariablesDeclarationsInterface.DataType.INT;
    static final VariablesDeclarationsInterface.DataType LONG = VariablesDeclarationsInterface.DataType.LONG;
    static final VariablesDeclarationsInterface.DataType DOUBLE = VariablesDeclarationsInterface.DataType.DOUBLE;
    static final VariablesDeclarationsInterface.DataType BOOLEAN = VariablesDeclarationsInterface.DataType.BOOLEAN;

    private Map<VariablesDeclarationsInterface.DataType, Set<VariablesDeclarationsInterface.DataType>> conversionsAllowed;

    TypeConversionsRules(){
        setDefault();
    }

    @Override
    public void setDefault() {
        conversionsAllowed = new HashMap<>();
        conversionsAllowed.put(INT, new HashSet<>());
        conversionsAllowed.put(LONG, new HashSet<>());
        conversionsAllowed.put(DOUBLE, new HashSet<>());
        conversionsAllowed.put(BOOLEAN, new HashSet<>());

        this.allow(INT, INT);
        this.allow(INT, LONG);
        this.allow(INT, DOUBLE);
        this.allow(LONG, LONG);
        this.allow(LONG, DOUBLE);
        this.allow(DOUBLE, DOUBLE);
        this.allow(BOOLEAN, BOOLEAN);
    }

    @Override
    public void deny(VariablesDeclarationsInterface.DataType from, VariablesDeclarationsInterface.DataType to) {
        conversionsAllowed.get(from).remove(to);
    }

    @Override
    public void allow(VariablesDeclarationsInterface.DataType from, VariablesDeclarationsInterface.DataType to) {
        conversionsAllowed.get(from).add(to);
    }

    @Override
    public boolean isAllowed(VariablesDeclarationsInterface.DataType from, VariablesDeclarationsInterface.DataType to) {
        return this.conversionsAllowed.get(from).contains(to);
    }

    @Override
    public void isNotAllowed(VariablesDeclarationsInterface.DataType from, VariablesDeclarationsInterface.DataType to)
            throws VariablesDeclarationsInterface.InvalidCastException {
        if (!this.isAllowed(from, to))
            throw new VariablesDeclarationsInterface.InvalidCastException();
    }
}