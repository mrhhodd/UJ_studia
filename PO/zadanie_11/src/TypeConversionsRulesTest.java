import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TypeConversionsRulesTest {

    TypeConversionsRules TCR = new TypeConversionsRules();
    private static final VariablesDeclarationsInterface.DataType INT = VariablesDeclarationsInterface.DataType.INT;
    private static final VariablesDeclarationsInterface.DataType DOUBLE = VariablesDeclarationsInterface.DataType.DOUBLE;
    private static final VariablesDeclarationsInterface.DataType LONG = VariablesDeclarationsInterface.DataType.LONG;
    private static final VariablesDeclarationsInterface.DataType BOOLEAN = VariablesDeclarationsInterface.DataType.BOOLEAN;

//    @Test(timeout = 500)
//    public void debugNumericConversions(){
//        int i = 1;
//        long l = 2L;
//        double d = 0.3D;
//        boolean b = false;
//        i = i;
//        i = l;
//        i = b;
//        i = d;
//
//        l = i;
//        l = d;
//        l = l;
//        l = b;
//
//        d = i;
//        d = l;
//        d = d;
//        d = b;
//
//        b = i;
//        b = l;
//        b = d;
//        b = b;
//    }

    @Test(timeout = 500)
    public void defaultIsAllowedForINTWorks(){
        Assert.assertEquals(true,  TCR.isAllowed(INT, INT));
        Assert.assertEquals(true,  TCR.isAllowed(INT, LONG));
        Assert.assertEquals(false, TCR.isAllowed(INT, BOOLEAN));
        Assert.assertEquals(true,  TCR.isAllowed(INT, DOUBLE));
    }
    @Test(timeout = 500)
    public void defaultIsAllowedForLONGWorks(){
        Assert.assertEquals(false, TCR.isAllowed(LONG, INT));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, LONG));
        Assert.assertEquals(false, TCR.isAllowed(LONG, BOOLEAN));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, DOUBLE));
    }
    @Test(timeout = 500)
    public void defaultIsAllowedForDOUBLEWorks(){
        Assert.assertEquals(false, TCR.isAllowed(DOUBLE, INT));
        Assert.assertEquals(false, TCR.isAllowed(DOUBLE, LONG));
        Assert.assertEquals(false, TCR.isAllowed(DOUBLE, BOOLEAN));
        Assert.assertEquals(true,  TCR.isAllowed(DOUBLE, DOUBLE));
    }
    @Test(timeout = 500)
    public void defaultIsAllowedForBOOLEANWorks(){
        Assert.assertEquals(false, TCR.isAllowed(BOOLEAN, INT));
        Assert.assertEquals(false, TCR.isAllowed(BOOLEAN, LONG));
        Assert.assertEquals(true,  TCR.isAllowed(BOOLEAN, BOOLEAN));
        Assert.assertEquals(false, TCR.isAllowed(BOOLEAN, DOUBLE));
    }

    @Test(timeout = 500)
    public void defaultIsNotAllowedRaisesValidNumberOfExceptions() {
        Map<VariablesDeclarationsInterface.DataType, Integer> exceptionCount = new HashMap<>();
        exceptionCount.put(INT, 0);
        exceptionCount.put(LONG, 0);
        exceptionCount.put(DOUBLE, 0);
        exceptionCount.put(BOOLEAN, 0);
        Map<VariablesDeclarationsInterface.DataType, Integer> expectedResults = new HashMap<>();
        expectedResults.put(INT, 1);
        expectedResults.put(LONG, 2);
        expectedResults.put(DOUBLE, 3);
        expectedResults.put(BOOLEAN, 3);

        for (VariablesDeclarationsInterface.DataType sourceDT: VariablesDeclarationsInterface.DataType.values()) {
            for (VariablesDeclarationsInterface.DataType targetDT : VariablesDeclarationsInterface.DataType.values()) {
                try {
                    TCR.isNotAllowed(sourceDT, targetDT);
                } catch (VariablesDeclarationsInterface.InvalidCastException e) {
                    exceptionCount.put(sourceDT, exceptionCount.get(sourceDT) + 1);
                }
            }
            System.out.println("SourceDT: " + sourceDT + " count: " + exceptionCount.get(sourceDT));
            Assert.assertEquals((int)expectedResults.get(sourceDT), (int)exceptionCount.get(sourceDT));
        }
    }

    @Test(timeout = 500)
    public void canAllowNewConversion(){
        TCR.allow(DOUBLE, LONG);

        Assert.assertEquals(false, TCR.isAllowed(DOUBLE, INT));
        Assert.assertEquals(true,  TCR.isAllowed(DOUBLE, LONG));
        Assert.assertEquals(false, TCR.isAllowed(DOUBLE, BOOLEAN));
        Assert.assertEquals(true,  TCR.isAllowed(DOUBLE, DOUBLE));
    }

    @Test(timeout = 500)
    public void allowingNewConversionTwiceHasNoEffect(){
        TCR.allow(LONG, DOUBLE);
        TCR.allow(LONG, DOUBLE);
        this.defaultIsAllowedForLONGWorks();
    }

    @Test(timeout = 500)
    public void canDenyNewConversion(){
        TCR.deny(LONG, DOUBLE);

        Assert.assertEquals(false, TCR.isAllowed(LONG, INT));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, LONG));
        Assert.assertEquals(false, TCR.isAllowed(LONG, BOOLEAN));
        Assert.assertEquals(false, TCR.isAllowed(LONG, DOUBLE));
    }

    @Test(timeout = 500)
    public void denyingNewConversionTwiceHasNoEffect(){
        TCR.deny(LONG, DOUBLE);
        TCR.deny(LONG, DOUBLE);

        Assert.assertEquals(false, TCR.isAllowed(LONG, INT));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, LONG));
        Assert.assertEquals(false, TCR.isAllowed(LONG, BOOLEAN));
        Assert.assertEquals(false, TCR.isAllowed(LONG, DOUBLE));
    }


    @Test(timeout = 500)
    public void canAllowThenDenyNewConversion(){
        TCR.allow(LONG, BOOLEAN);

        Assert.assertEquals(false, TCR.isAllowed(LONG, INT));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, LONG));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, BOOLEAN));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, DOUBLE));

        TCR.deny(LONG, BOOLEAN);
        this.defaultIsAllowedForLONGWorks();
    }

    @Test(timeout = 500)
    public void setDefaultReturnsEverythingBackToDefault(){
        TCR.allow(LONG, BOOLEAN);
        TCR.allow(LONG, INT);

        Assert.assertEquals(true, TCR.isAllowed(LONG, INT));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, LONG));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, BOOLEAN));
        Assert.assertEquals(true,  TCR.isAllowed(LONG, DOUBLE));

        TCR.setDefault();
        this.defaultIsAllowedForLONGWorks();
    }
}
