import org.junit.Test;
import org.junit.Before;
import org.junit.*;
import org.junit.Assert;


public class ImageGeneratorTest {
  private ImageGenerator ig;
  boolean[][] canvas;

  @Before
  public void testSetup() {
    this.ig = new ImageGenerator();
    this.ig.maxUndoRedoRepeatCommands(10);
    this.canvas = new boolean[10][10];
    this.ig.setCanvas(this.canvas);
  }

  @Test
  public void canGetPenStyle(){
    Assert.assertEquals(ig.getPenStyle(), ImageGeneratorPenStyleInterface.PenStyle.SOLID);
  }

  @Test
  public void canSetPenStyle(){
    for(ImageGeneratorPenStyleInterface.PenStyle style: ImageGeneratorPenStyleInterface.PenStyle.values()) {
      ig.setPenStyle(style);
      Assert.assertEquals(ig.getPenStyle(), style);
    }
  }

  @Test
  public void canUndoPenStyle(){
    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.INVERSE);
    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.INVERSE);
    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.XOR);
    ig.undo(1);
    Assert.assertEquals(ig.getPenStyle(), ImageGeneratorPenStyleInterface.PenStyle.INVERSE);
    ig.undo(1);
    Assert.assertEquals(ig.getPenStyle(), ImageGeneratorPenStyleInterface.PenStyle.INVERSE);
    ig.redo(2);
    Assert.assertEquals(ig.getPenStyle(), ImageGeneratorPenStyleInterface.PenStyle.XOR);
  }

  @Test
  public void canRepeatUndoRedoPenStyle(){
    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.SOLID);
    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.INVERSE);
    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.AND);
    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.XOR);
    ig.repeat(3);
    Assert.assertEquals(ig.getPenStyle(), ImageGeneratorPenStyleInterface.PenStyle.XOR);
    ig.undo(2);
    Assert.assertEquals(ig.getPenStyle(), ImageGeneratorPenStyleInterface.PenStyle.AND);
    ig.redo(2);
    Assert.assertEquals(ig.getPenStyle(), ImageGeneratorPenStyleInterface.PenStyle.XOR);
  }

  @Test
  public void complexTestExample1(){
    ig.setInitialPosition(2,4);
    ig.setInitialPosition(4,4);
    ig.setInitialPosition(0,4);
    ImageGenerator.showCanvas(canvas);

    ig.right(2);
    ImageGenerator.showCanvas(canvas);
    assert canvas[0][4] && canvas[1][4] && canvas[2][4] && !canvas[3][4] && canvas[4][4];

    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.INVERSE);
    ImageGenerator.showCanvas(canvas);
    assert canvas[0][4] && canvas[1][4] && !canvas[2][4] && !canvas[3][4] && canvas[4][4];

    ig.right(3);
    ImageGenerator.showCanvas(canvas);
    assert canvas[0][4] && canvas[1][4] && !canvas[2][4] && canvas[3][4] && !canvas[4][4] && canvas[5][4];

    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.AND);
    ImageGenerator.showCanvas(canvas);
    assert canvas[0][4] && canvas[1][4] && !canvas[2][4] && canvas[3][4] && !canvas[4][4] && canvas[5][4];

    ig.right(3);
    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.OR);
    ImageGenerator.showCanvas(canvas);
    assert canvas[0][4] && canvas[1][4] && !canvas[2][4] && canvas[3][4] && !canvas[4][4] && canvas[5][4]
      && !canvas[6][4] && !canvas[7][4] && canvas[8][4];

  }

//  @Test
//  public void question726(){
//    ig.setInitialPosition(0,4);
//    ig.setPenState(ImageGeneratorPenInterface.PenState.UP);
//    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.INVERSE);
//    ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
//    ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
//    assert !canvas[0][4];
//  }

  @Test
  public void answer726(){
    ig.setInitialPosition(0,4);
    ig.setPenState(ImageGeneratorPenInterface.PenState.UP);
    ig.setPenStyle(ImageGeneratorPenStyleInterface.PenStyle.INVERSE);
    ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
    ig.setPenState(ImageGeneratorPenInterface.PenState.UP);
    ig.setPenState(ImageGeneratorPenInterface.PenState.DOWN);
    assert canvas[0][4];


  }

}

