import java.util.LinkedHashMap;

/**
 * Klasa reprezentujaca kwadratowa macierz rzadka.
 */
class SparseMatrix {
  class SparseMatrixKey {
    private final int i;
    private final int j;

    SparseMatrixKey(int i, int j) {
      this.i = i;
      this.j = j;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof SparseMatrixKey)) return false;
      SparseMatrixKey tmpKey = (SparseMatrixKey) o;
      return i == tmpKey.i && j == tmpKey.j;
    }

    @Override
    public int hashCode() {
      long longHashCode = (Integer.MAX_VALUE + 1L) * this.i + this.j;
      return Long.hashCode(longHashCode);
    }
  }

	private int size;
  private LinkedHashMap<SparseMatrixKey, Double> nonZeroValues =  new LinkedHashMap<>();
	/**
	 * Konstruktor klasy SparceMatrix. Wymusza podanie rozmiaru macierzy. Poprawne
	 * indeksowanie od 0 do size - 1.
	 * 
	 * @param size
	 *            rozmiar macierzy
	 */
	public SparseMatrix(int size) {
		this.size = size;
	}

	/**
	 * Metoda pozwala na pobranie wartosci znajdujacej sie w komorce o indeksie
	 * [i][j] macierzy.
	 * 
	 * @param i
	 *            pierwszy indeks macierzy od 0 do size - 1
	 * @param j
	 *            drugi indeks macierzy od 0 do size - 1
	 * @return wartosc komorki o indeksie [i][j]
	 */
	double get(int i, int j) {
    SparseMatrixKey key = new SparseMatrixKey(i,j);
    return (nonZeroValues.containsKey(key)) ? nonZeroValues.get(key) : 0;
	}

	/**
	 * Metoda zwraca rozmiar macierzy.
	 * 
	 * @return rozmiar macierzy
	 */
	int getSize() {
		return size;
	}

	/**
	 * Metoda pozwala na ustawienie wartosci macierzy dla wskazanych indeksach
	 * [i][j].
	 * 
	 * @param i
	 *            pierwszy indeks macierzy
	 * @param j
	 *            drugi indeks macierzy
	 * @param value
	 *            wartosc wstawiana do macierzy. Moze byc nia zero!
	 */
	void set(int i, int j, double value) {
    SparseMatrixKey key = new SparseMatrixKey(i,j);
    if (value != 0) nonZeroValues.put(key, value);
    else if (nonZeroValues.containsKey(key))
      nonZeroValues.remove(key);
	}

	/**
	 * Metoda zwraca sume macierzy rzadkich.
	 * 
	 * @param a
	 *            pierwsza z macierzy
	 * @param b
	 *            druga z macierzy
	 * @return wynik dodawnia macierzy a + b
	 */
	public static SparseMatrix add(SparseMatrix a, SparseMatrix b) {
    SparseMatrix newMatrix = new SparseMatrix(a.getSize());
    newMatrix.nonZeroValues = a.nonZeroValues;
		// TODO: dodac zachowanie dla zerowych elementow PO dodawaniu
    for ( SparseMatrixKey nonZeroValueKey: b.nonZeroValues.keySet() ){
      double valueB = b.nonZeroValues.get(nonZeroValueKey);
      if ( a.nonZeroValues.containsKey(nonZeroValueKey) ) {
        double valueA = a.nonZeroValues.get(nonZeroValueKey);
        newMatrix.set(nonZeroValueKey.i, nonZeroValueKey.j, valueA + valueB);
      }
      else {
        newMatrix.nonZeroValues.put(nonZeroValueKey, valueB);
      }
    }
    return newMatrix;
  }

	/**
	 * Metoda zwraca standardowy iloczyn macierzy.
	 * 
	 * C = AB
	 * <br>
	 * c<sub>ij</sub> = &sum; <sup>size-1</sup><sub>k = 0</sub> a<sub>ik</sub> b<sub>kj</sub>
	 * 
	 * @param a
	 *            pierwsza z macierzy
	 * @param b
	 *            druga z macierzy
	 * @return wynik mnozenia macierzy przez macierz
	 */
	public static SparseMatrix multiply(SparseMatrix a, SparseMatrix b) {
		SparseMatrix newMatrix = new SparseMatrix(a.getSize());
    double oldValue, newValue;
    for (SparseMatrixKey nonZeroValueKeyA: a.nonZeroValues.keySet()){
      int rowKeyA = nonZeroValueKeyA.i;
      int columnKeyA = nonZeroValueKeyA.j;
      for (SparseMatrixKey nonZeroValueKeyB: b.nonZeroValues.keySet()) {
        int rowKeyB = nonZeroValueKeyB.i;
        int columnKeyB = nonZeroValueKeyB.j;
        if (columnKeyA == rowKeyB) {
          oldValue = newMatrix.get(rowKeyA, columnKeyB);
          newValue = oldValue + a.get(rowKeyA, columnKeyA) * b.get(rowKeyB, columnKeyB);
          newMatrix.set(rowKeyA, columnKeyB, newValue);
        }
      }
    }
    return newMatrix;
	}
}


