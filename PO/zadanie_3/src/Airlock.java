import java.util.*;

public class Airlock implements AirlockInterface {
    private State currentState;
    private List<State> stateHistory;
    private Map<State, Integer> stateUsageCounter;

    public void setState(State state){
        this.currentState = state;
        initializeStateHistory(state);
        initializeStateUsageCounter(state);
    }

    private void initializeStateUsageCounter(State state) {
        this.stateUsageCounter = new HashMap<>();
        for (State st: State.values())
            this.stateUsageCounter.put(st, 0);
        this.incrementUsageCounter(state);
    }

    private void initializeStateHistory(State state) {
        this.stateHistory = new ArrayList<>();
        this.stateHistory.add(state);
    }

    private void incrementUsageCounter(State state){
        this.stateUsageCounter.put(state, this.stateUsageCounter.get(state) + 1);
    }

    public State getState(){
        return this.currentState;
    }

    public Set<State> newStates(){
        Set<State> possibleNewStates = new HashSet<>();
        if (currentState == Airlock.State.DISASTER)
            return possibleNewStates;
        return getPossibleNewStates(possibleNewStates);
    }

    private Set<State> getPossibleNewStates(Set<State> possibleNewStates) {
        possibleNewStates.addAll(Arrays.asList(State.values()));
        possibleNewStates.remove(this.currentState);
        if (this.currentState == State.EXTERNAL_DOOR_OPENED)
            possibleNewStates.remove(State.INTERNAL_DOOR_OPENED);
        else if (this.currentState == State.INTERNAL_DOOR_OPENED)
            possibleNewStates.remove(State.EXTERNAL_DOOR_OPENED);
        else
            possibleNewStates.remove(State.DISASTER);
        return possibleNewStates;
    }

    public List<State> getHistory(){
        return this.stateHistory;
    }

    public Map<State, Integer> getUsageCounters(){
        return this.stateUsageCounter;
    }

    public void closeInternalDoor(){
        setNewState(State.INTERNAL_DOOR_CLOSED);
    }

    public void closeExternalDoor(){
        setNewState(State.EXTERNAL_DOOR_CLOSED);
    }

    public void openInternalDoor(){
        setNewState(State.INTERNAL_DOOR_OPENED);
    }

    public void openExternalDoor(){
        setNewState(State.EXTERNAL_DOOR_OPENED);
    }

    private void setNewState(State newState){
        State validatedNewState = getValidatedNewState(newState);
        this.currentState = validatedNewState;
        this.stateHistory.add(validatedNewState);
        this.incrementUsageCounter(validatedNewState);
    }

    private State getValidatedNewState(State newState) {
        if (this.currentState == State.EXTERNAL_DOOR_OPENED && newState == State.INTERNAL_DOOR_OPENED
            || this.currentState == State.INTERNAL_DOOR_OPENED && newState == State.EXTERNAL_DOOR_OPENED
            || this.currentState == State.DISASTER)
            return State.DISASTER;
        else
            return newState;
    }
}
