import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AirlockTest {

    Airlock al;

    @BeforeEach
    public void canCreateAirlockObject(){
        al = new Airlock();
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
    }

    @Test
    public void canSetInitialState(){
        for (Airlock.State newState: AirlockInterface.State.values()){
            al.setState(newState);
            assert al.getState() == newState;
        }
    }

    @Test
    public void stateHistoryIsClearedAfterInitialization(){
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        assert al.getHistory().size() == 1;
        assert al.getHistory().get(0) == Airlock.State.EXTERNAL_DOOR_CLOSED;
        al.closeExternalDoor();
        al.closeExternalDoor();
        al.closeExternalDoor();
        al.closeExternalDoor();
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        assert al.getHistory().size() == 1;
        assert al.getHistory().get(0) == Airlock.State.EXTERNAL_DOOR_CLOSED;
    }

    @Test
    public void stateHistoryContainsValidHistoryStateAfterFewSetStates(){
        al.closeExternalDoor();
        al.openExternalDoor();
        al.closeExternalDoor();
        al.openExternalDoor();
        Airlock.State[] expectedStateHistoryArray = {
                Airlock.State.EXTERNAL_DOOR_CLOSED,
                Airlock.State.EXTERNAL_DOOR_CLOSED,
                Airlock.State.EXTERNAL_DOOR_OPENED,
                Airlock.State.EXTERNAL_DOOR_CLOSED,
                Airlock.State.EXTERNAL_DOOR_OPENED};
        List<Airlock.State>expectedStateHistory = Arrays.asList(expectedStateHistoryArray);
        assertEquals(al.getHistory(),expectedStateHistory);
    }

    @Test
    public void usageCountersIsClearedAfterInitialization(){
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        Map<Airlock.State, Integer> expectedStateCounters = new HashMap<>();
        expectedStateCounters.put(AirlockInterface.State.DISASTER, 0);
        expectedStateCounters.put(AirlockInterface.State.INTERNAL_DOOR_CLOSED, 0);
        expectedStateCounters.put(AirlockInterface.State.EXTERNAL_DOOR_CLOSED, 1);
        expectedStateCounters.put(AirlockInterface.State.INTERNAL_DOOR_OPENED, 0);
        expectedStateCounters.put(AirlockInterface.State.EXTERNAL_DOOR_OPENED, 0);
        assertEquals(expectedStateCounters, al.getUsageCounters());
        al.closeExternalDoor();
        al.closeExternalDoor();
        al.closeExternalDoor();
        al.closeExternalDoor();
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        assertEquals(expectedStateCounters, al.getUsageCounters());
    }

    @Test
    public void usageCountersIsIncrementedCorrectly(){
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        Map<Airlock.State, Integer> expectedStateCounters = new HashMap<>();
        expectedStateCounters.put(AirlockInterface.State.DISASTER, 0);
        expectedStateCounters.put(AirlockInterface.State.INTERNAL_DOOR_CLOSED, 0);
        expectedStateCounters.put(AirlockInterface.State.EXTERNAL_DOOR_CLOSED, 5);
        expectedStateCounters.put(AirlockInterface.State.INTERNAL_DOOR_OPENED, 0);
        expectedStateCounters.put(AirlockInterface.State.EXTERNAL_DOOR_OPENED, 6);
        al.closeExternalDoor();
        al.openExternalDoor();
        al.closeExternalDoor();
        al.closeExternalDoor();
        al.closeExternalDoor();
        al.openExternalDoor();
        al.openExternalDoor();
        al.openExternalDoor();
        al.openExternalDoor();
        al.openExternalDoor();
        assertEquals(expectedStateCounters, al.getUsageCounters());
    }

    @Test
    public void newStatesReturnsCorrectValuesForAllStates(){
        for (Airlock.State currentState: Airlock.State.values()) {
            Set<Airlock.State> expectedNewStates = new HashSet<>();
            al.setState(currentState);
            if (currentState == Airlock.State.DISASTER)
                assertEquals(expectedNewStates, al.newStates());
            else {
                expectedNewStates.addAll(Arrays.asList(Airlock.State.values()));
                expectedNewStates.remove(currentState);
                if (currentState == AirlockInterface.State.EXTERNAL_DOOR_OPENED)
                    expectedNewStates.remove(Airlock.State.INTERNAL_DOOR_OPENED);
                else if (currentState == AirlockInterface.State.INTERNAL_DOOR_OPENED)
                    expectedNewStates.remove(Airlock.State.EXTERNAL_DOOR_OPENED);
                else
                    expectedNewStates.remove(Airlock.State.DISASTER);

                assertEquals(expectedNewStates, al.newStates());
            }
            System.out.println(currentState + " : " + expectedNewStates);
        }
    }

    @Test
    public void openingBothDoorsLocksStateInDisaster(){
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        al.openExternalDoor();
        al.openInternalDoor();
        assert al.getState() == Airlock.State.DISASTER;
        al.closeInternalDoor();
        assert al.getState() == Airlock.State.DISASTER;
        al.closeExternalDoor();
        assert al.getState() == Airlock.State.DISASTER;
        al.openExternalDoor();
        assert al.getState() == Airlock.State.DISASTER;
        al.openInternalDoor();
        assert al.getState() == Airlock.State.DISASTER;

        Map<Airlock.State, Integer> expectedStateCounters = new HashMap<>();
        expectedStateCounters.put(AirlockInterface.State.DISASTER, 5);
        expectedStateCounters.put(AirlockInterface.State.INTERNAL_DOOR_CLOSED, 0);
        expectedStateCounters.put(AirlockInterface.State.EXTERNAL_DOOR_CLOSED, 1);
        expectedStateCounters.put(AirlockInterface.State.INTERNAL_DOOR_OPENED, 0);
        expectedStateCounters.put(AirlockInterface.State.EXTERNAL_DOOR_OPENED, 1);
        assertEquals(expectedStateCounters, al.getUsageCounters());

        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        al.openInternalDoor();
        al.openExternalDoor();
        assert al.getState() == Airlock.State.DISASTER;
    }

    @Test
    public void newStatesReturnsEmptySetAfterDisasterIsReached(){
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        al.openExternalDoor();
        al.openInternalDoor();
        Set<AirlockInterface.State> expectedNewStates = new HashSet<>();
        assertEquals(expectedNewStates, al.newStates());
    }

    @Test
    public void canSetStateAfterDisaster(){
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        al.openExternalDoor();
        al.openExternalDoor();
        al.openExternalDoor();
        al.openExternalDoor();
        al.openInternalDoor();
        al.setState(Airlock.State.EXTERNAL_DOOR_CLOSED);
        Map<Airlock.State, Integer> expectedStateCounters = new HashMap<>();
        expectedStateCounters.put(AirlockInterface.State.DISASTER, 0);
        expectedStateCounters.put(AirlockInterface.State.INTERNAL_DOOR_CLOSED, 0);
        expectedStateCounters.put(AirlockInterface.State.EXTERNAL_DOOR_CLOSED, 1);
        expectedStateCounters.put(AirlockInterface.State.INTERNAL_DOOR_OPENED, 0);
        expectedStateCounters.put(AirlockInterface.State.EXTERNAL_DOOR_OPENED, 0);
        assertEquals(expectedStateCounters, al.getUsageCounters());
    }
}
