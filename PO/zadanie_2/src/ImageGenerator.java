import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class ImageGenerator implements ImageGeneratorConfigurationInterface,
        ImageGeneratorInterface {
    private enum Direction {
        UP, LEFT, DOWN, RIGHT;
        static Direction getInversedDirection(Direction dir){
            Direction reversedDirection = null;
            switch (dir){
                case UP:
                    reversedDirection = DOWN;
                    break;
                case DOWN:
                    reversedDirection = UP;
                    break;
                case LEFT:
                    reversedDirection = RIGHT;
                    break;
                case RIGHT:
                    reversedDirection = LEFT;
                    break;
            }
            return reversedDirection;
        }
    }

    private class DrawingStep {

        List<Direction> stepList = new LinkedList<>();
        List<Boolean> previousStates = new LinkedList<>();

        DrawingStep(List<Direction> stepList, List<Boolean> previousStates){
            this.stepList = stepList;
            this.previousStates = previousStates;
        }

        void rememberState(boolean oldState){
            this.previousStates.add(oldState);
        }
    }

    class LimitedLinkedList<E> extends LinkedList<E> {
        private int maximumSize;

        public LimitedLinkedList(int maximumSize) {
            this.maximumSize = maximumSize;
        }

        @Override
        public boolean add(E o) {
            while (this.size() >= maximumSize)
                super.remove();
            super.add(o);
            return true;
        }
    }

    private int cursorPositionX, cursorPositionY;
    private boolean canvas[][];

    private List<DrawingStep> commandBuffer;
    private List<DrawingStep> undoBuffer;

    public ImageGenerator() {
    }

    @Override
    public void setCanvas(boolean[][] canvas) {
        this.canvas = canvas;
    }

    @Override
    public void maxUndoRedoRepeatCommands(int commands) {
        this.commandBuffer = new LimitedLinkedList<DrawingStep>(commands);
        this.undoBuffer = new LimitedLinkedList<DrawingStep>(commands);
    }

    @Override
    public void setInitialPosition(int col, int row) {
        this.cursorPositionX = row;
        this.cursorPositionY = col;
        this.canvas[cursorPositionY][cursorPositionX] = true;
    }

    @Override
    public void up(int steps) {
        drawInDirection(steps, Direction.UP);
    }

    @Override
    public void down(int steps) {
        drawInDirection(steps, Direction.DOWN);
    }

    @Override
    public void left(int steps) {
        drawInDirection(steps, Direction.LEFT);
    }

    @Override
    public void right(int steps) {
        drawInDirection(steps, Direction.RIGHT);
    }

    private void drawInDirection(int numberOfSteps, Direction direction) {
        List<Direction> newSteps = new LinkedList<>();
        List<Boolean> previousStates = new LinkedList<>();
        for (int i = 0; i < numberOfSteps; i++)
            newSteps.add(direction);
        drawAndAddToBuffer(new DrawingStep(newSteps, previousStates));
    }

    private void drawAndAddToBuffer(DrawingStep step) {
        int stepCount = step.stepList.size();
        for (int i = 0; i < stepCount; i++) {
            changeCursorPositionInDirection(step.stepList.get(i));
            step.rememberState(this.canvas[this.cursorPositionY][this.cursorPositionX]);
            this.canvas[this.cursorPositionY][cursorPositionX] = true;
        }
        this.commandBuffer.add(step);
    }

    private void changeCursorPositionInDirection(Direction direction) {
        switch (direction) {
            case UP:
                this.cursorPositionX += 1;
                break;
            case DOWN:
                this.cursorPositionX -= 1;
                break;
            case RIGHT:
                this.cursorPositionY += 1;
                break;
            case LEFT:
                this.cursorPositionY -= 1;
        }
    }

    @Override
    public void undo(int commands) {
        int initialCommandBufferLastIndex =  this.commandBuffer.size() - 1;
        for (int i=0; i<commands; i++){
            this.undoBuffer.add(this.commandBuffer.get(initialCommandBufferLastIndex - i));
            restorePreviousCellState(this.commandBuffer.get(initialCommandBufferLastIndex - i));
            this.commandBuffer.remove(initialCommandBufferLastIndex - i);
        }
    }

    private void restorePreviousCellState(DrawingStep step) {
        int stepCount = step.stepList.size();
        for (int i = stepCount - 1; i >= 0; i--) {
            this.canvas[this.cursorPositionY][cursorPositionX] = step.previousStates.get(i);
            changeCursorPositionInDirection(Direction.getInversedDirection(step.stepList.get(i)));
        }
    }

    @Override
    public void redo(int commands) {
        int LastUndoCommandIndex = this.undoBuffer.size();
        for (int i = 1; i <= commands; i++) {
            drawAndAddToBuffer(this.undoBuffer.get(LastUndoCommandIndex - i));
            this.undoBuffer.remove(LastUndoCommandIndex - i);
        }
    }

    @Override
    public void repeat(int commands) {
        int bufferSize = commandBuffer.size();
        List<Direction> newSteps = new LinkedList<>();
        List<Boolean> previousStates = new LinkedList<>();
        for (int i = commands; i > 0; i--) {
            newSteps.addAll(commandBuffer.get(bufferSize - i).stepList);
            previousStates.addAll(commandBuffer.get(bufferSize - i).previousStates);
        }
        drawAndAddToBuffer(new DrawingStep(newSteps, previousStates));
    }

    public void showCanvas() {
        for (int i = this.canvas[0].length - 1; i >= 0; i--) {
            StringBuilder sb = new StringBuilder();
            sb.append(i);
            sb.append(" |");
            for (int j = 0; j < this.canvas.length; j++) {
                sb.append(this.canvas[j][i] ? "*|" : " |");
            }
            System.out.println(sb);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("   ");
        for (int j = 0; j < this.canvas.length; j++) {
            sb.append(j);
            sb.append(" ");
        }
        System.out.println(sb);
    }

    public void showCanvas(boolean[][] canvas) {
        for (int i = canvas[0].length - 1; i >= 0; i--) {
            StringBuilder sb = new StringBuilder();
            sb.append(i);
            sb.append(" |");
            for (int j = 0; j < canvas.length; j++) {
                sb.append(canvas[j][i] ? "*|" : " |");
            }
            System.out.println(sb);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("   ");
        for (int j = 0; j < canvas.length; j++) {
            sb.append(j);
            sb.append(" ");
        }
    }
}
