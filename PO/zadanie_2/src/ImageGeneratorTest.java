//import org.junit.Before;
//import org.junit.Test;
//
//import java.time.Duration;
//
//import static org.junit.jupiter.api.Assertions.assertTimeout;
//
//
//public class ImageGeneratorTest {
//
//    private ImageGenerator ig;
//
//    @Before
//    public void testSetup(){
//        this.ig = new ImageGenerator();
//        this.ig.maxUndoRedoRepeatCommands(10);
//        this.ig.setCanvas(new boolean[10][10]);
//    }
//
//
//    @Test
//    public void canDrawInAllDirections(){
//        ig.setInitialPosition(2,3);
//        ig.up(3);
//        ig.showCanvas();
//        assert ig.canvas[2][4] && ig.canvas[2][5] && ig.canvas[2][6];
//        ig.left(1);
//        ig.showCanvas();
//        assert ig.canvas[1][6];
//        ig.down(2);
//        ig.showCanvas();
//        assert ig.canvas[1][5] && ig.canvas[1][4];
//        ig.right(2);
//        ig.showCanvas();
//        assert ig.canvas[2][4] && ig.canvas[3][4];
//    }
//
//    @Test
//    public void canRepeatStep(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.showCanvas();
//        assert ig.canvas[0][1] && ig.canvas[0][2] && !ig.canvas[0][3];
//        ig.repeat(1);
//        ig.showCanvas();
//        assert ig.canvas[0][3] && ig.canvas[0][4] && !ig.canvas[0][5];
//    }
//
//    @Test
//    public void canRepeatMultipleSteps(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.right(3);
//        ig.showCanvas();
//        assert ig.canvas[0][1] && ig.canvas[0][2] && ig.canvas[1][2] && ig.canvas[2][2] && ig.canvas[3][2];
//        ig.repeat(2);
//        ig.showCanvas();
//        assert ig.canvas[3][3] && ig.canvas[3][4] && ig.canvas[4][4] && ig.canvas[5][4] && ig.canvas[6][4];
//        assert ig.commandBuffer.size() == 3;
//    }
//
//    @Test public void canUndoOneCommandNonRepeatCommand(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.right(3);
//        ig.showCanvas();
//        ig.undo(1);
//        ig.showCanvas();
//        assert ig.canvas[0][1] && ig.canvas[0][2] && !ig.canvas[1][2] && !ig.canvas[2][2] && !ig.canvas[3][2];
//    }
//
//    @Test public void canUndoMultipleNonRepeatCommands(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.right(3);
//        ig.up(2);
//        ig.left(3);
//        ig.showCanvas();
//        ig.undo(2);
//        ig.showCanvas();
//        assert ig.canvas[0][1] && ig.canvas[0][2] && ig.canvas[1][2] && ig.canvas[2][2] && ig.canvas[3][2];
//        assert !ig.canvas[0][4] && !ig.canvas[1][4] && !ig.canvas[2][4] && !ig.canvas[3][4] && !ig.canvas[3][3];
//    }
//
//    @Test public void canRedoOneNonRepeatCommand(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.right(3);
//        ig.undo(1);
//        ig.showCanvas();
//        ig.redo(1);
//        ig.showCanvas();
//        assert ig.canvas[0][1] && ig.canvas[0][2] && ig.canvas[1][2] && ig.canvas[2][2] && ig.canvas[3][2];
//    }
//
//    @Test public void canRedoMultipleNonRepeatCommands(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.right(3);
//        ig.up(2);
//        ig.left(3);
//        ig.showCanvas();
//        assert ig.undoBuffer.size() == 0;
//        assert ig.commandBuffer.size() == 4;
//        ig.undo(2);
//        assert ig.undoBuffer.size() == 2;
//        assert ig.commandBuffer.size() == 2;
//        ig.showCanvas();
//        ig.redo(2);
//        assert ig.undoBuffer.size() == 0;
//        assert ig.commandBuffer.size() == 4;
//        ig.showCanvas();
//        assert ig.canvas[0][1] && ig.canvas[0][2] && ig.canvas[1][2] && ig.canvas[2][2] && ig.canvas[3][2];
//        assert ig.canvas[0][4] && ig.canvas[1][4] && ig.canvas[2][4] && ig.canvas[3][4] && ig.canvas[3][3];
//    }
//
//    @Test public void canUndoRepeatCommand(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.right(3);
//        ig.showCanvas();
//        ig.repeat(2);
//        ig.showCanvas();
//        ig.undo(1);
//        ig.showCanvas();
//        assert ig.canvas[0][1] && ig.canvas[0][2] && ig.canvas[1][2] && ig.canvas[2][2] && ig.canvas[3][2];
//        assert !ig.canvas[3][3] && !ig.canvas[3][4] && !ig.canvas[4][4] && !ig.canvas[5][4] && !ig.canvas[6][4];
//    }
//
//    @Test
//    public void canUndoMultipleCommandsWithRepeat(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.right(3);
//        ig.up(2);
//        ig.left(2);
//        ig.showCanvas();
//        ig.repeat(3);
//        ig.showCanvas();
//        ig.undo(2);
//        ig.showCanvas();
//    }
//
//    @Test
//    public void canRedoRepeatCommand(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.right(3);
//        ig.repeat(2);
//        ig.undo(2);
//        ig.showCanvas();
//        ig.redo(2);
//        ig.showCanvas();
//        assert ig.canvas[0][1] && ig.canvas[0][2] && ig.canvas[1][2] && ig.canvas[2][2] && ig.canvas[3][2];
//        assert ig.canvas[3][3] && ig.canvas[3][4] && ig.canvas[4][4] && ig.canvas[5][4] && ig.canvas[6][4];
//    }
//
//    @Test
//    public void canUndoTwice(){
//        ig.setInitialPosition(0,0);
//        ig.up(2);
//        ig.right(6);
//        ig.up(3);
//        ig.left(2);
//        ig.repeat(2);
//        ig.showCanvas();
//        ig.undo(2);
//        ig.undo(1);
//        ig.showCanvas();
//        assert ig.canvas[0][0] && ig.canvas[0][1] && ig.canvas[0][2];
//        assert ig.canvas[1][2] && ig.canvas[2][2] && ig.canvas[3][2] && ig.canvas[4][2] && ig.canvas[5][2] && ig.canvas[6][2];
//
//    }
//
//    @Test
//    public void bufferSizesAreFixedMaxSize(){
//        ig.setInitialPosition(0,0);
//        ig.maxUndoRedoRepeatCommands(5);
//        ig.up(4);
//        ig.right(3);
//        ig.repeat(2);
//        ig.up(1);
//        ig.down(5);
//        assert ig.commandBuffer.size() == 5;
//        assert ig.undoBuffer.size() == 0;
//        ig.repeat(3);
//        assert ig.commandBuffer.size() == 5;
//        assert ig.undoBuffer.size() == 0;
//        ig.undo(3);
//        assert ig.commandBuffer.size() == 2;
//        assert ig.undoBuffer.size() == 3;
//        ig.undo(2);
//        assert ig.commandBuffer.size() == 0;
//        assert ig.undoBuffer.size() == 5;
//        ig.redo(5);
//        assert ig.commandBuffer.size() == 5;
//        assert ig.undoBuffer.size() == 0;
//    }
//
//    @Test()
//    public void oramusUndoTest1(){
//        assertTimeout(Duration.ofMillis(500), ()-> {
//            ig.setInitialPosition(2, 1);
//            ig.maxUndoRedoRepeatCommands(5);
//            ig.up(2);
//            ig.right(1);
//            ig.repeat(2);
//            ig.showCanvas();
//            ig.undo(1);
//            ig.showCanvas();
//        });
//    }
//
//    @Test()
//    public void oramusUndoTest2(){
//        assertTimeout(Duration.ofMillis(500), ()-> {
//            ig.setInitialPosition(2, 1);
//            ig.maxUndoRedoRepeatCommands(5);
//            ig.up(2);
//            ig.right(1);
//            ig.repeat(2);
//            ig.showCanvas();
//            ig.undo(2);
//            ig.showCanvas();
//        });
//    }
//
//    @Test()
//    public void oramusUndoTest3(){
//        assertTimeout(Duration.ofMillis(500), ()-> {
//            ig.setInitialPosition(2, 1);
//            ig.maxUndoRedoRepeatCommands(5);
//            ig.setInitialPosition(2, 1);
//            ig.up(2);
//            ig.setInitialPosition(0, 2);
//            ig.right(4);
//            ig.showCanvas();
//            ig.undo(1);
//            ig.showCanvas();
//        });
//    }
//}
//
