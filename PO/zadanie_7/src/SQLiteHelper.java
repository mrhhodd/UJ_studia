import java.lang.reflect.*;
import java.util.Arrays;

public class SQLiteHelper implements SQLiteHelperInterface {
    private final String[] integerTypes = {"int", "long", "java.lang.Integer", "java.lang.Long"};
    private final String[] realTypes = {"float", "double", "java.lang.Float", "java.lang.Double"};
    private final String[] textTypes = {"java.lang.String"};
    private final String[] booleanTypes = {"boolean", "java.lang.Boolean"};

    @Override
    public String createTable(Object o) {
        String createTableCommand = "";
        try {
            Class inspectedClass = Class.forName(o.getClass().getName());
            String className = inspectedClass.getSimpleName();
            Field[] publicFields = inspectedClass.getFields();
            return getCreateTableCommand(className, publicFields);
        }
        catch ( ClassNotFoundException e ) { System.out.println(e); }
        return createTableCommand;
    }

    @Override
    public String insert(Object o) {
        String insertCommand = "";
        try {
            Class inspectedClass = Class.forName(o.getClass().getName());
            String className = inspectedClass.getSimpleName();
            Field[] publicFields = inspectedClass.getFields();
            insertCommand = getInsertCommand(className, publicFields, o);
        }
        catch ( Exception e ) { System.out.println(e); }
        return insertCommand;
    }

    private String getInsertCommand(String tableName, Field[] tablePublicFields, Object o){
        StringBuilder columnNames = new StringBuilder(200);
        StringBuilder values = new StringBuilder(200);
        Object value;
        for (Field element : tablePublicFields){
            if (getSQLiteType(element) == null) continue;
            columnNames.append(element.getName());
            columnNames.append(",");
            try {
                value = element.get(o);
                values.append(getSQLiteValue(value));
                values.append(",");
            }
            catch ( IllegalAccessException e ) { System.out.println(e);}
        }
        columnNames.deleteCharAt(columnNames.lastIndexOf(","));
        values.deleteCharAt(values.lastIndexOf(","));
        return "INSERT INTO " + tableName + "(" + columnNames.toString() + ") " + "VALUES(" + values.toString() + ");";
    }

    private String getSQLiteValue(Object o){
        String fieldType = o.getClass().getTypeName();
        String sqliteValue = null;
        if (Arrays.asList(integerTypes).contains(fieldType))
            sqliteValue = o.toString();
        if (Arrays.asList(realTypes).contains(fieldType))
            sqliteValue = "\"" + o.toString().replace(".", ",") + "\"";
        if (Arrays.asList(textTypes).contains(fieldType))
            sqliteValue = "\"" + o + "\"";
        if (Arrays.asList(booleanTypes).contains(fieldType))
            sqliteValue = (Boolean)o ? "1" : "0";
        return sqliteValue;
    }

    private String getCreateTableCommand(String tableName, Field[] tablePublicFields) {
        StringBuilder columns = new StringBuilder(200);
        for (Field element : tablePublicFields){
            if (getSQLiteType(element) == null) continue;
            columns.append(element.getName());
            columns.append(" ");
            columns.append(getSQLiteType(element));
            columns.append(",");
        }
        columns.deleteCharAt(columns.lastIndexOf(","));
        return "CREATE TABLE IF NOT EXISTS " + tableName + "(" + columns.toString() + ");";
    }

    private String getSQLiteType(Field field){
        String fieldType = field.getType().getTypeName();
        String sqliteType = null;
        if (Arrays.asList(integerTypes).contains(fieldType) || Arrays.asList(booleanTypes).contains(fieldType))
            sqliteType = "INTEGER";
        if (Arrays.asList(realTypes).contains(fieldType))
            sqliteType = "REAL";
        if (Arrays.asList(textTypes).contains(fieldType))
            sqliteType = "TEXT";
        return sqliteType;
    }

}
