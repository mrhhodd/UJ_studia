import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Random;

public class TreeHelper implements TreeHelperInterface {
    static Random randomGenerator = new Random();

    @Override
    public int treeHeight(NodeInterface root) {
        return analyzeTree(root)[0];
    }

    @Override
    public int nodes(NodeInterface root) {
        return analyzeTree(root)[1];
    }

    @Override
    public int leafs(NodeInterface root) {
        return analyzeTree(root)[2];
    }

    private int[] analyzeTree(NodeInterface root){
        LinkedList<NodeInterface> childrenNodes = new LinkedList<>();
        LinkedList<NodeInterface> currentNodes;
        childrenNodes.add(root);
        int treeHeight = 0;
        int nodesCount = 0;
        int leafsCount = 0;

        while(childrenNodes.size() != 0) {
            nodesCount += childrenNodes.size();
            currentNodes = new LinkedList<>();
            currentNodes.addAll(childrenNodes);
            childrenNodes = new LinkedList<>();
            treeHeight++;
            for(NodeInterface node: currentNodes) {
                if (node.requiredChildNodes() == 0)
                    leafsCount++;
                childrenNodes.addAll(this.getChildrenNodes(node));
            }
        }
        return new int[]{treeHeight, nodesCount, leafsCount};
    }

    private LinkedList<NodeInterface> getChildrenNodes(NodeInterface parentNode){
        LinkedList<NodeInterface> childrenNodes = new LinkedList<>();
        for(int i=0; i < parentNode.requiredChildNodes(); i++){
            try {
                childrenNodes.add(parentNode.getChildNode(i));
            } catch (Exception e) {}
        }
        return childrenNodes;
    }

    @Override
    public NodeInterface construct(int nodes, int depth, int leafs, NodePrototypes prototypes) {
        LinkedList<Integer> treePlan = getTreePlan(nodes, depth);
        NodeInterface treeRoot = null;
        try {
            treeRoot = populateTree(treePlan, leafs, prototypes);
        } catch (StructureException e){ System.out.println(e);}
        return treeRoot;
    }

    private HashMap<String, LinkedList<String>> getSortedPrototypes(NodePrototypes prototypes){
        HashMap<String, LinkedList<String>> sortedPrototypes = new HashMap<>();
        sortedPrototypes.put("FUNCTIONS", new LinkedList<>(prototypes.getFunctions()));
        sortedPrototypes.put("BIFUNCTIONS", new LinkedList<>(prototypes.getBiFunctions()));
        LinkedList<String> values = new LinkedList<>();
        values.addAll(prototypes.getConstants());
        values.addAll(prototypes.getVariables());
        sortedPrototypes.put("VALUES", values);

        return  sortedPrototypes;
    }

    private NodeInterface getNewRoot(LinkedList<Integer> treePlan, NodePrototypes prototypes){
        HashMap<String, LinkedList<String>> sortedPrototypes = getSortedPrototypes(prototypes);
        NodeInterface newRoot;
        if (treePlan.size() == 1)
            newRoot = prototypes.get(sortedPrototypes.get("VALUES")
                    .get(randomGenerator.nextInt(sortedPrototypes.get("VALUES").size())));
        else if (treePlan.get(1) == 1) {
            newRoot = prototypes.get(sortedPrototypes.get("FUNCTIONS")
                    .get(randomGenerator.nextInt(sortedPrototypes.get("FUNCTIONS").size())));
        } else {
            newRoot = prototypes.get(sortedPrototypes.get("BIFUNCTIONS")
                    .get(randomGenerator.nextInt(sortedPrototypes.get("BIFUNCTIONS").size())));
        }
        return newRoot;
    }

    private NodeInterface populateTree(LinkedList<Integer> treePlan, int expectedLeafs,
                                       NodePrototypes prototypes) throws StructureException{
        LinkedList<LinkedList<NodeInterface>> tree = new LinkedList<>();
        tree.add(new LinkedList<>());
        tree.get(0).add(getNewRoot(treePlan, prototypes));
        HashMap<String, LinkedList<String>> sortedPrototypes = getSortedPrototypes(prototypes);
        LinkedList<NodeInterface> newNodes;

        int remainingLeafs = expectedLeafs - treePlan.getLast();
        for (int level=1; level < treePlan.size(); level++) {
            newNodes = new LinkedList<>();
            int inserted_leafs = 0;
            int bifunctions_count = 0;
            int functions_count = 0;
            tree.add(new LinkedList<>());
            if (treePlan.size() == level + 1)
                for (NodeInterface node : tree.get(level - 1)) {
                    for (int i = 0; i < node.requiredChildNodes(); i++)
                        node.setChildNode(i, prototypes.get(sortedPrototypes.get("VALUES")
                                .get(randomGenerator.nextInt(sortedPrototypes.get("VALUES").size()))));
                }
            else {
                for (int i = 0; i < remainingLeafs; i++) {
                    if (treePlan.get(level + 1 ) <= 2 * (treePlan.get(level) - inserted_leafs - 1)) {
                        newNodes.add(prototypes.get(sortedPrototypes.get("VALUES")
                                .get(randomGenerator.nextInt(sortedPrototypes.get("VALUES").size()))));
                        inserted_leafs++;
                    }
                }
                remainingLeafs -= inserted_leafs;
                bifunctions_count = treePlan.get(level + 1) - (treePlan.get(level) - inserted_leafs);
                functions_count = treePlan.get(level) - inserted_leafs - bifunctions_count;
                for (int i = 0; i < bifunctions_count; i++) {
                    newNodes.add(prototypes.get(sortedPrototypes.get("BIFUNCTIONS")
                            .get(randomGenerator.nextInt(sortedPrototypes.get("BIFUNCTIONS").size()))));
                }
                for (int i = 0; i < functions_count; i++){
                    newNodes.add(prototypes.get(sortedPrototypes.get("FUNCTIONS")
                            .get(randomGenerator.nextInt(sortedPrototypes.get("FUNCTIONS").size()))));
                }
                for (NodeInterface node : tree.get(level - 1)) {
                    for (int i = 0; i < node.requiredChildNodes(); i++) {
                        node.setChildNode(i, newNodes.getLast());
                        tree.get(level).add(newNodes.getLast());
                        newNodes.removeLast();
                    }
                }
            }
        }
        NodeInterface newRoot = tree.get(0).get(0);
        return newRoot;
    }

    private LinkedList<Integer> getTreePlan(int nodes, int depth) {
        LinkedList<Integer> result = new LinkedList<>();
        for (int i=0; i<=depth; i++)
            result.add(1);
        int remainingNodes = nodes - depth;

        while(remainingNodes > 0){
            for (int level = depth - 1; level > 0; level-- ){
                if (2 * result.get(level - 1) > result.get(level)){
                    result.set(level, result.get(level) + 1 );
                    remainingNodes--;
                }
                if (remainingNodes <= 0) break;
            }
        }
        result.removeLast();
        return result;
    }

    @Override
    public boolean areEquivalents(NodeInterface root1, NodeInterface root2) {
        int numberOfChildNodes = root1.requiredChildNodes();
        if (numberOfChildNodes != root2.requiredChildNodes())
            return false;
        if (numberOfChildNodes == 0)
            return root1.getMathSymbol().equals(root2.getMathSymbol());
        if (root2.getMathSymbol().equals( root1.getMathSymbol())) {
            try {
                if (numberOfChildNodes == 1)
                    return areEquivalents(root1.getChildNode(0), root2.getChildNode(0));
                if (root1.hasSymmetry() && root2.hasSymmetry())
                    return areEquivalents(root1.getChildNode(0), root2.getChildNode(0))
                            && areEquivalents(root1.getChildNode(1), root2.getChildNode(1)) ||
                            areEquivalents(root1.getChildNode(1), root2.getChildNode(0))
                                    && areEquivalents(root1.getChildNode(0), root2.getChildNode(1));
                return areEquivalents(root1.getChildNode(0), root2.getChildNode(0))
                        && areEquivalents(root1.getChildNode(1), root2.getChildNode(1));
            } catch (StructureException e) {}
        }
        return false;
    }
}