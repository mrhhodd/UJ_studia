import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

public class TreeHelperTest {

    TreeHelper th = new TreeHelper();
    NodePrototypes prototypes = NodePrototypes.getNodePrototypes();
    NodeInterface sin, mult, plusC, plusCSymm;
    @Before
    public void testSetup() throws StructureException, MathError{
        prototypes.addPrototype(new BiFunctionNode("+", (x, y) -> x + y, true));
        prototypes.addPrototype(new BiFunctionNode("-", (x, y) -> x - y, false));
        prototypes.addPrototype(new BiFunctionNode("*", (x, y) -> x * y, true));
        prototypes.addPrototype(new FunctionNode("sin", (x) -> Math.sin(x)));
        prototypes.addPrototype(new FunctionNode("cos", (x) -> Math.cos(x)));
        prototypes.addPrototype(new ConstantNode("2", 2));
        prototypes.addPrototype(new VariableNode("x"));
        prototypes.addPrototype(new VariableNode("y"));

        sin = prototypes.get("sin");
        sin.setChildNode(0, prototypes.get("x"));

        mult = prototypes.get("*");
        mult.setChildNode(0, prototypes.get("2"));
        mult.setChildNode(1, sin);

        plusC = prototypes.get("+");
        plusC.setChildNode(0, sin);
        plusC.setChildNode(1, mult);

        plusCSymm = prototypes.get("+");
        plusCSymm.setChildNode(0, mult);
        plusCSymm.setChildNode(1, sin);
    }

    @Test(timeout = 500)
    public void canCreateTreeHelperInstance(){th = new TreeHelper();}

    @Test(timeout = 500)
    public void canGetCorrectTreeHeight(){
        Assert.assertEquals(4, th.treeHeight(plusC));
    }

    @Test(timeout = 500)
    public void canGetCorrectNodesNumber() {

        Assert.assertEquals(7, th.nodes(plusC));
    }
    @Test(timeout = 500)
    public void canGetCorrectLeafsumber(){

        Assert.assertEquals(3, th.leafs(plusC));
    }

    @Test(timeout = 500)
    public void canDetectSimpleEquivalentTree(){
        Assert.assertEquals(true, th.areEquivalents(plusC, plusCSymm));
    }

    @Test(timeout = 500)
    public void canDetectOneNodeDeepEquivalentTree() throws StructureException, MathError{
        NodeInterface cos = prototypes.get("cos");
        NodeInterface cosSymm = prototypes.get("cos");
        cos.setChildNode(0, plusC);
        cosSymm.setChildNode(0, plusCSymm);

        Assert.assertEquals(true, th.areEquivalents(cos, cosSymm));
    }

    @Test(timeout = 500)
    public void canDetectEquivalentTreeWithSameRoots() throws StructureException, MathError{
        Assert.assertEquals(true, th.areEquivalents(prototypes.get("2"), prototypes.get("2")));
        Assert.assertEquals(false, th.areEquivalents(prototypes.get("x"), prototypes.get("2")));
        Assert.assertEquals(true, th.areEquivalents(plusC, plusC));
    }

    @Test(timeout = 500)
    public void canConstructSimpleTree() throws StructureException, MathError{
        NodeInterface newRoot = th.construct(3,2, 2, prototypes);
        Assert.assertEquals(3, th.nodes(newRoot));
        Assert.assertEquals(2, th.leafs(newRoot));
        Assert.assertEquals(2, th.treeHeight(newRoot));
    }

    @Test(timeout = 500)
    public void canConstructComplexTree() throws StructureException, MathError{
        NodeInterface newRoot = th.construct(9,4, 4, prototypes);
        Assert.assertEquals(9, th.nodes(newRoot));
        Assert.assertEquals(4, th.leafs(newRoot));
        Assert.assertEquals(4, th.treeHeight(newRoot));
    }

    @Test(timeout = 5000)
    public void canConstructComplexTree100Times() throws StructureException, MathError{
        NodeInterface newRoot = null;
        for (int i=0; i<100; i++) {
            newRoot = th.construct(10, 4, 4, prototypes);
            Assert.assertEquals(10, th.nodes(newRoot));
            Assert.assertEquals(4, th.leafs(newRoot));
            Assert.assertEquals(4, th.treeHeight(newRoot));
        }
        System.out.print(newRoot);
    }

}
