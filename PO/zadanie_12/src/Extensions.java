import java.util.*;


public class Extensions implements ExtensionsInterface {
    private Integer nextId = Integer.MIN_VALUE;
    private Map<Integer, Device> devicesRegister = new HashMap<>();
    private Map<Integer, Integer[]> extensionCordRegister = new HashMap<>();

    @Override
    public int addDevice(Device device) {
        devicesRegister.put(nextId, device);
        return this.nextId++;
    }


    @Override
    public int addExtensionCord(ExtensionCord extensionCord) {
        extensionCordRegister.put(nextId, new Integer[extensionCord.getNumberOfSockets()]);
        return nextId++;
    }

    @Override
    public void connectDevice(int deviceID, int destinationID, int socket) throws WrongSocketException {
        connect(deviceID, destinationID, socket);
    }

    @Override
    public void connectExtensionCord(int cordID, int destinationID, int socket) throws WrongSocketException {
        connect(cordID, destinationID, socket);
    }

    private void connect(int sourceID, int destinationID, int socket) throws WrongSocketException {
        if ( socket < 0 || socket > extensionCordRegister.get(destinationID).length - 1 ||
                extensionCordRegister.get(destinationID)[socket] != null)
            throw new WrongSocketException();
        extensionCordRegister.get(destinationID)[socket] = sourceID;
    }

    @Override
    public boolean disconnect(int destinationID, int socket) throws WrongSocketException {
        if ( socket < 0 || socket > extensionCordRegister.get(destinationID).length - 1)
            throw new WrongSocketException();
        if (extensionCordRegister.get(destinationID)[socket] == null)
            return false;
        else{
            extensionCordRegister.get(destinationID)[socket] = null;
            return true;
        }
    }

    @Override
    public Optional<Set<Integer>> overload(double powerLimit) {
        Set<Integer> overloadedExtensionCordIds = new HashSet<>();
        for (Integer cordID : extensionCordRegister.keySet()){
            if (getCombinedPower(cordID) > powerLimit)
                overloadedExtensionCordIds.add(cordID);
        }
        return overloadedExtensionCordIds.size() != 0 ? Optional.of(overloadedExtensionCordIds) : Optional.empty();
    }

    private double getCombinedPower(Integer cordId){
        double combinedPower = 0;
        for (Integer socketId: extensionCordRegister.get(cordId)){
            if (socketId == null )
                continue;
            if (devicesRegister.containsKey(socketId))
                combinedPower += devicesRegister.get(socketId).powerConsumption();
            else
                combinedPower += getCombinedPower(socketId);
        }
        return combinedPower;
    }
}
