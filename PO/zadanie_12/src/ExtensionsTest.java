import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


public class ExtensionsTest {
    ExtensionsInterface ext = new Extensions();

    @Test(timeout = 500)
    public void canAddDevice(){
        ext.addDevice(new TestDevice(10));
    }

    @Test(timeout = 5000)
    public void canAddLotsOfDevicesWithUniqueID(){
        long expectedValue = 1000000;
        Set<Integer> idBag = new HashSet<>();
        for (long i=0; i<expectedValue; i++)
            idBag.add(ext.addDevice(new TestDevice(10)));
        Assert.assertEquals(expectedValue, idBag.size());
    }
    @Test(timeout = 500)
    public void canAddExtensionCord(){
        ext.addExtensionCord(new TestExtensionCord(5));
    }

    @Test(timeout = 5000)
    public void canAddLotsOfExtensionCordsWithUniqueID(){
        long expectedValue = 1000000;
        Set<Integer> idBag = new HashSet<>();
        for (long i=0; i<expectedValue; i++)
            idBag.add(ext.addExtensionCord(new TestExtensionCord(10)));
        Assert.assertEquals(expectedValue, idBag.size());
    }

    @Test(timeout = 500)
    public void connectingDeviceToWrongSocketIdRaisesExcepion(){
        int extensionCordId = ext.addExtensionCord(new TestExtensionCord(5));
        int exceptionsCaught = 0;
        try {
            ext.connectDevice(1234, extensionCordId, -1);
        } catch (ExtensionsInterface.WrongSocketException e) {exceptionsCaught++;}
        Assert.assertEquals(1, exceptionsCaught);
        try {
            ext.connectDevice(1234, extensionCordId, 0);
            ext.connectDevice(1234, extensionCordId, 1);
            ext.connectDevice(1234, extensionCordId, 2);
            ext.connectDevice(1234, extensionCordId, 3);
            ext.connectDevice(1234, extensionCordId, 4);
            ext.connectDevice(1234, extensionCordId, 5);
        } catch (ExtensionsInterface.WrongSocketException e) {exceptionsCaught++;}
        Assert.assertEquals(2, exceptionsCaught);
    }
    @Test(timeout = 500)
    public void connectingDeviceToSameSocketTwiceRaisesException() {
        int extensionCordId = ext.addExtensionCord(new TestExtensionCord(3));
        int exceptionsCaught = 0;
        try {
            ext.connectDevice(1234, extensionCordId, 2);
            ext.connectDevice(1234, extensionCordId, 2);
        } catch (ExtensionsInterface.WrongSocketException e) {exceptionsCaught++;}
        Assert.assertEquals(1, exceptionsCaught);
    }

    @Test(timeout = 500)
    public void disconnectingDeviceFromWrongSocketIdRaisesExcepion(){
        int extensionCordId = ext.addExtensionCord(new TestExtensionCord(4));
        int exceptionsCaught = 0;
        try {
            ext.disconnect( extensionCordId, -1);
        } catch (ExtensionsInterface.WrongSocketException e) {exceptionsCaught++;}
        Assert.assertEquals(1, exceptionsCaught);
        try {
            ext.disconnect(extensionCordId, 0);
            ext.disconnect(extensionCordId, 1);
            ext.disconnect(extensionCordId, 2);
            ext.disconnect(extensionCordId, 3);
            ext.disconnect(extensionCordId, 4);
        } catch (ExtensionsInterface.WrongSocketException e) {exceptionsCaught++;}
        Assert.assertEquals(2, exceptionsCaught);
    }

    @Test(timeout = 500)
    public void successfullDisconnectionReturnsTrue() {
        int exceptionsCaught = 0;
        int extensionCord1 = ext.addExtensionCord(new TestExtensionCord(3));
        int device1 = ext.addDevice(new TestDevice(123));
        int device2 = ext.addDevice(new TestDevice(43));
        int extensionCord2 = ext.addExtensionCord(new TestExtensionCord(4));
        try {
            ext.connectDevice(device1, extensionCord1, 0);
            ext.connectDevice(device2, extensionCord1, 1);
            ext.connectExtensionCord(extensionCord2, extensionCord1, 2);
            Assert.assertEquals(true, ext.disconnect(extensionCord1, 0));
            Assert.assertEquals(true, ext.disconnect(extensionCord1, 1));
            Assert.assertEquals(true, ext.disconnect(extensionCord1, 2));
        } catch (ExtensionsInterface.WrongSocketException e) {}
        Assert.assertEquals(0, exceptionsCaught);
    }

    @Test(timeout = 500)
    public void disconnectingFromEmptySocketReturnsFalse() {
        int exceptionsCaught = 0;
        int extensionCord1 = ext.addExtensionCord(new TestExtensionCord(3));
        int device1 = ext.addDevice(new TestDevice(123));
        try {
            ext.connectDevice(device1, extensionCord1, 0);
            Assert.assertEquals(true, ext.disconnect(extensionCord1, 0));
            Assert.assertEquals(false, ext.disconnect(extensionCord1, 0));
            Assert.assertEquals(false, ext.disconnect(extensionCord1, 0));
            Assert.assertEquals(false, ext.disconnect(extensionCord1, 1));
            Assert.assertEquals(false, ext.disconnect(extensionCord1, 2));
        } catch (ExtensionsInterface.WrongSocketException e) {}
        Assert.assertEquals(0, exceptionsCaught);
    }

    @Test(timeout = 500)
    public void overloadForEmptyExtensionsReturnsNull(){
        Assert.assertEquals(Optional.empty(), ext.overload(250));
    }

    @Test(timeout = 500)
    public void overloadForNonOverloadedFlatExtensionsReturnsNull(){
        int extensionCord1 = ext.addExtensionCord(new TestExtensionCord(3));
        int device1 = ext.addDevice(new TestDevice(150));
        int device2 = ext.addDevice(new TestDevice(39));
        try {
            ext.connectDevice(device1, extensionCord1, 0);
            ext.connectDevice(device2, extensionCord1, 1);
        } catch (ExtensionsInterface.WrongSocketException e) {}
        Assert.assertEquals(Optional.empty(), ext.overload(250));
    }

    @Test(timeout = 500)
    public void overloadForNonOverloadedAdvancedExtensionsReturnsNull(){
        int extensionCord1 = ext.addExtensionCord(new TestExtensionCord(3));
        int device1 = ext.addDevice(new TestDevice(50));
        int device2 = ext.addDevice(new TestDevice(39));
        int device3 = ext.addDevice(new TestDevice(29));
        int device4 = ext.addDevice(new TestDevice(129));
        int extensionCord2 = ext.addExtensionCord(new TestExtensionCord(6));
        int extensionCord3 = ext.addExtensionCord(new TestExtensionCord(7));
        try {
            ext.connectDevice(device1, extensionCord1, 0);
            ext.connectExtensionCord(extensionCord2, extensionCord1,2);
            ext.connectDevice(device2, extensionCord2, 0);
            ext.connectDevice(device3, extensionCord2, 3);
            ext.connectExtensionCord(extensionCord3, extensionCord2,5);
            ext.connectDevice(device4, extensionCord3, 3);
        } catch (ExtensionsInterface.WrongSocketException e) {}
        Assert.assertEquals(Optional.empty(), ext.overload(250));
    }


    @Test(timeout = 500)
    public void overloadForBorderlineOverloadedFlatExtensionsReturnsProperSet(){
        Set<Integer> expectedIDSet = new HashSet<Integer>();
        int extensionCord1 = ext.addExtensionCord(new TestExtensionCord(3));
        int device1 = ext.addDevice(new TestDevice(150));
        int device2 = ext.addDevice(new TestDevice(100));
        int device3 = ext.addDevice(new TestDevice(1));
        expectedIDSet.add(extensionCord1);
        try {
            ext.connectDevice(device1, extensionCord1, 0);
            ext.connectDevice(device2, extensionCord1, 1);
            ext.connectDevice(device3, extensionCord1, 2);
        } catch (ExtensionsInterface.WrongSocketException e) {}
        Assert.assertEquals(Optional.of(expectedIDSet), ext.overload(250));
    }

    @Test(timeout = 500)
    public void overloadForOverloadedFlatExtensionsReturnsNull(){
        Set<Integer> expectedIDSet = new HashSet<Integer>();
        int extensionCord1 = ext.addExtensionCord(new TestExtensionCord(3));
        int device1 = ext.addDevice(new TestDevice(150));
        int device2 = ext.addDevice(new TestDevice(39));
        expectedIDSet.add(extensionCord1);
        try {
            ext.connectDevice(device1, extensionCord1, 0);
            ext.connectDevice(device2, extensionCord1, 1);
        } catch (ExtensionsInterface.WrongSocketException e) {}
        Assert.assertEquals(Optional.of(expectedIDSet), ext.overload(100));
    }

    @Test(timeout = 500)
    public void overloadForOverloadedAdvancedExtensionsReturnsNull(){
        Set<Integer> expectedIDSet = new HashSet<Integer>();
        int extensionCord1 = ext.addExtensionCord(new TestExtensionCord(3));
        int device1 = ext.addDevice(new TestDevice(50));
        int device2 = ext.addDevice(new TestDevice(39));
        int device3 = ext.addDevice(new TestDevice(29));
        int device4 = ext.addDevice(new TestDevice(129));
        int extensionCord2 = ext.addExtensionCord(new TestExtensionCord(6));
        int extensionCord3 = ext.addExtensionCord(new TestExtensionCord(7));
        expectedIDSet.add(extensionCord1);
        expectedIDSet.add(extensionCord2);
        expectedIDSet.add(extensionCord3);
        try {
            ext.connectDevice(device1, extensionCord1, 0);
            ext.connectExtensionCord(extensionCord2, extensionCord1,2);
            ext.connectDevice(device2, extensionCord2, 0);
            ext.connectDevice(device3, extensionCord2, 3);
            ext.connectExtensionCord(extensionCord3, extensionCord2,5);
            ext.connectDevice(device4, extensionCord3, 3);
            Assert.assertEquals(Optional.of(expectedIDSet), ext.overload(100));

            ext.disconnect(extensionCord1, 2);
            ext.disconnect(extensionCord1, 0);
            ext.connectDevice(device1, extensionCord1, 1);
            ext.connectDevice(extensionCord2, extensionCord1, 0);
            Assert.assertEquals(Optional.of(expectedIDSet), ext.overload(100));

            expectedIDSet.remove(extensionCord3);
            Assert.assertEquals(Optional.of(expectedIDSet), ext.overload(150));

            expectedIDSet.add(extensionCord3);
            ext.disconnect(extensionCord1, 1);
            ext.connectDevice(device1, extensionCord3, 4);
            Assert.assertEquals(Optional.of(expectedIDSet), ext.overload(100));
        } catch (ExtensionsInterface.WrongSocketException e) {}
    }
}

class TestExtensionCord implements ExtensionsInterface.ExtensionCord {
    private int nubmerOfSockets = 0;
    TestExtensionCord(int nubmerOfSockets){
        this.nubmerOfSockets = nubmerOfSockets;
    }

    @Override
    public int getNumberOfSockets() {
        return this.nubmerOfSockets;
    }
}

class TestDevice implements ExtensionsInterface.Device {
    private double powerConsumption = 0;
    TestDevice(double powerConsumption){
        this.powerConsumption = powerConsumption;
    }
    @Override
    public double powerConsumption() {
        return this.powerConsumption;
    }
}