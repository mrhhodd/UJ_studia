import java.util.Optional;
import java.util.Set;

/**
 * Interfejs systemu okablowania dostarczajacego energii elektrycznej.
 *
 */
public interface ExtensionsInterface {
	/**
	 * Interfejs urzadzenia elektrycznego o pewnym niezerowym poborze pradu.
	 */
	public interface Device {
		/**
		 * Metoda zwraca zapotrzebowanie na prad elektryczny urzadzenia. Jednostki
		 * umowne.
		 * 
		 * @return zapotrzebowanie na prad elektryczny.
		 */
		public double powerConsumption();
	}

	public interface ExtensionCord {
		/**
		 * Liczba gniazdek, do ktorych mozna podlaczyc urzadzenia elektryczne lub
		 * kolejne przedluzacze z kolejnymi gniazdkami.
		 * 
		 * @return liczba gniazek
		 */
		public int getNumberOfSockets();
	}

	public class WrongSocketException extends Exception {
		private static final long serialVersionUID = 9211772771467294535L;
	}

	/**
	 * Metoda dodaje przedluzacz do systemu. Przedluzacz uzyskuje unikalny numer
	 * identyfikacyjny.
	 * 
	 * @param extensionCord
	 *            dodawany przedluzacz
	 * @return numer identyfikacyjny przedluzacza
	 */
	public int addExtensionCord(ExtensionCord extensionCord);

	/**
	 * Metoda dodaje do systemu urzadzenie elektryczne. Urzadzenie otrzymuje
	 * unikalny numer identyfikacyjny.
	 * 
	 * @param device
	 *            dodawane urzadzenie elektryczne
	 * @return numer identyfikacyjny urzadzenia
	 */
	public int addDevice(Device device);

	/**
	 * Metoda podpina przedluzacz o numerze cordID do przedluzacza o numerze
	 * destinationID. W przedluzaczu, do ktorego inny sie podlacza, uzywane jest
	 * gniazdo o numerze socket. W przypadku blednego numeru socket (mniejszy od 0
	 * lub wiekszy od getNumberOfSockets-1 zglaszany jest wyjatek.
	 * 
	 * @param cordID
	 *            identyfikator podlaczanego przedluzacza
	 * @param destinationID
	 *            numer przedluzacza, do ktorego sie wpinamy
	 * @param socket
	 *            numer gniazdka, do ktorego wpinany jest przedluzacz.
	 * @throws WrongSocketException
	 *             wyjatek zglaszany w przypadku blednego okreslenia numeru socket.
	 */
	public void connectExtensionCord(int cordID, int destinationID, int socket) throws WrongSocketException;

	/**
	 * Metoda odpina przedluzacz lub urządzenie od przedluzacza o numerze
	 * destinationID. W przedluzaczu, z ktorego inny obiekt jest odłączany, uzywane jest
	 * gniazdo o numerze socket. W przypadku blednego numeru socket (mniejszy od 0
	 * lub wiekszy od getNumberOfSockets-1) zglaszany jest wyjatek.
	 * 
	 * @param destinationID
	 *            numer przedluzacza, od ktorego inny wpinamy
	 * @param socket
	 *            numer gniazdka, od ktorego wypinany jest przedluzacz lub urządzenie.
	 * @return true - dane były poprawne i operacja została wykonana poprawnie,
	 *         false - niczego nie rozłączono (gniazdo było nieużywane).
	 * @throws WrongSocketException
	 *             wyjatek zglaszany w przypadku blednego okreslenia numeru socket.
	 */
	public boolean disconnect(int destinationID, int socket) throws WrongSocketException;

	/**
	 * Metoda podpina urzadzenie o numerze deviceID do przedluzacza o numerze
	 * destinationID. W przedluzaczu uzywane jest gniazdo o numerze socket. W
	 * przypadku blednego numeru socket (mniejszy od 0 lub wiekszy od
	 * getNumberOfSockets-1 zglaszany jest wyjatek.
	 * 
	 * @param deviceID
	 *            identyfikator podlaczanego urzadzenia elektycznego
	 * @param destinationID
	 *            numer przedluzacza, do ktorego sie wpinamy
	 * @param socket
	 *            numer gniazdka, do ktorego wpinane jest urzadzenie.
	 * @throws WrongSocketException
	 *             wyjatek zglaszany w przypadku blednego okreslenia numeru socket.
	 */
	public void connectDevice(int deviceID, int destinationID, int socket) throws WrongSocketException;

	/**
	 * Metoda sprawdzajaca czy w obwodzie nie ma przeciazen. Zakladamy, ze maksymane
	 * obciazenie wszystkich przedluzaczy wynosi powerLimit jednostek umownych
	 * (takich samych jakie uzywane sa w przypadku metody powerConsumption. Zwracane
	 * sa identyfikatory wszystkich przedluzaczy, w ktorych poplynie prad wiekszy od
	 * powerLimit. Przeciazenia wyliczane sa na podstawie sumy obciazen danego
	 * przedluzacza generowane przez podlaczone do niego urzadzenia elektryczne i
	 * pochodzace z innych podlaczonych przedluzaczy.
	 * 
	 * @param powerLimit
	 *            ograniczenie pradowe przedluzaczy
	 * @return obiekt klasy Optional zawierajacy zbior identyfikator przeciazonych
	 *         przedluzaczy lub null jesli nie ma przeciazen w obwodzie.
	 */
	public Optional<Set<Integer>> overload(double powerLimit);
}
