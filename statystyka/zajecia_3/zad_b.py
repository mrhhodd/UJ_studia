from random import random
from math import log
import operator

##### Stałe ###############
SIMULATIONS_COUNT = 1000  # Ilość symulacji
TIME_RANGE = 10000        # Wielkość osi czasu
TICK_SIZE = 1             # Gęstość podziału osi czasu
LAMBDA_A = 1. / 45         # Tempo przychodzenia zadań
LAMBDA_B = 1. / 35         # Tempo obsługi zada ń
###########################

# Generowanie osi czasu
t = [i * TICK_SIZE for i in (range(int(TIME_RANGE / TICK_SIZE)))]


# Generacja liczb z rozkładu poisonna
def random_poisson_number(lambada=1.):
    return - log(random()) / lambada


def run_simulation():
    # Generowanie czasów przychodzenia zadań
    time_of_incoming_tasks = [random_poisson_number(LAMBDA_A)]
    while True:
        time_of_incoming_tasks.append(random_poisson_number(LAMBDA_A)
                                      + time_of_incoming_tasks[-1])
        if time_of_incoming_tasks[-1] > TIME_RANGE:
            time_of_incoming_tasks.pop()
            break

    # Generowanie czasów obsługi kolejnych zadań
    time_of_handling_tasks = [random_poisson_number(LAMBDA_B)
                              + time_of_incoming_tasks[0]]
    for task_id in range(1, len(time_of_incoming_tasks)):
        new_handling_time = random_poisson_number(LAMBDA_B)
        if time_of_incoming_tasks[task_id] < time_of_handling_tasks[task_id - 1]:
            busy_time = time_of_handling_tasks[task_id - 1] - time_of_incoming_tasks[task_id]
        else:
            busy_time = 0
        time_of_handling_tasks.append(new_handling_time
                                      + time_of_incoming_tasks[task_id]
                                      + busy_time)

    # Obliczanie średniej ilości zadań w systemie
    queue_size = [0] * len(t)
    incoming_task_number = 0
    handling_task_number = 0
    for time in range(1, len(t)):
        queue_size[time] = queue_size[time - 1]
        if incoming_task_number < len(time_of_incoming_tasks) and \
                        time_of_incoming_tasks[incoming_task_number] < t[time]:
            queue_size[time] += 1
            incoming_task_number += 1
        if handling_task_number < len(time_of_handling_tasks) and \
                        time_of_handling_tasks[handling_task_number] < t[time]:
            queue_size[time] -= 1
            handling_task_number += 1

    mean_tasks_count = sum(queue_size)/float(len(queue_size))

    # Obliczanie średniego czasu spędzonego przez zadanie w systemie
    time_in_queue = list(map(operator.sub, time_of_handling_tasks, time_of_incoming_tasks))
    mean_task_time = sum(time_in_queue)/float(len(time_in_queue))
    return mean_task_time, mean_tasks_count


if __name__ == '__main__':
    mean_task_times = []
    mean_tasks_count = []
    # Uruchom symulacje SIMULATIONS_COUNT razy
    # Zwróć średni czas spędzony przez zadanie w systemie i średnią liczbę zadań w systemie
    for i in range(SIMULATIONS_COUNT):
        mean_task_time, mean_task_count = run_simulation()
        mean_task_times.append(mean_task_time)
        mean_tasks_count.append(mean_task_count)

    overall_mean_task_time = sum(mean_task_times)/float(SIMULATIONS_COUNT)
    overall_mean_task_count = sum(mean_tasks_count)/float(SIMULATIONS_COUNT)

    # Wyswietl wynik z dokładnością do 3 miejsca po przecinku:
    print("E(R) * LambdaA = E(k)")
    print("%.3f * %.3f = %.3f" % (overall_mean_task_time, LAMBDA_A, overall_mean_task_count))
    print("%.3f = %.3f" % (overall_mean_task_time * LAMBDA_A, overall_mean_task_count))

