from random import random
from math import log
from matplotlib import pyplot as plt

##### Stałe ########
TIME_RANGE = 1000  # Wielkość osi czasu
TICK_SIZE = 0.1       # Gęstość podziału osi czasu
MAX_TASKS = 5     # Ilość przychodzących zadan
LAMBDA_A = 1 / 20   # Tempo przychodzenia zadań
LAMBDA_B = 1 / 20  # Tempo obsługi zadań
####################

# Generacja liczb z rozkładu poisonna
def random_poisson_number(lambada=1.):
    return - log(random()) / lambada


if __name__ == '__main__':

    # Generowanie osi czasu
    t = [i * TICK_SIZE for i in (range(int(TIME_RANGE / TICK_SIZE)))]

    # Generowanie czasów przychodzenia zadań
    time_of_incoming_tasks = [random_poisson_number(LAMBDA_A)]
    while True:
        if len(time_of_incoming_tasks) == MAX_TASKS:
            break
        time_of_incoming_tasks.append(random_poisson_number(LAMBDA_A)
                                      + time_of_incoming_tasks[-1])
        if time_of_incoming_tasks[-1] > TIME_RANGE:
            time_of_incoming_tasks.pop()
            break

    # Generowanie czasów obsługi kolejnych zadań
    time_of_handling_tasks = [random_poisson_number(LAMBDA_B)
                              + time_of_incoming_tasks[0]]
    for task_id in range(1, len(time_of_incoming_tasks)):
        new_handling_time = random_poisson_number(LAMBDA_B)
        if time_of_incoming_tasks[task_id] < time_of_handling_tasks[task_id - 1]:
            busy_time = time_of_handling_tasks[task_id - 1] - time_of_incoming_tasks[task_id]
        else:
            busy_time = 0
        time_of_handling_tasks.append(new_handling_time
                                      + time_of_incoming_tasks[task_id]
                                      + busy_time)

    # Generowanie wektorów przedstawiających wielkość kolejki i czas wykonywania zadań
    queue_size = [0] * len(t)
    done_tasks = [0] * len(t)
    incoming_task_number = 0
    handling_task_number = 0
    for time in range(1, len(t)):
        queue_size[time] = queue_size[time - 1]
        done_tasks[time] = done_tasks[time - 1]
        if incoming_task_number < len(time_of_incoming_tasks) and \
                        time_of_incoming_tasks[incoming_task_number] < t[time]:
            queue_size[time] += 1
            incoming_task_number += 1
        if handling_task_number < len(time_of_handling_tasks) and \
                        time_of_handling_tasks[handling_task_number] < t[time]:
            queue_size[time] -= 1
            handling_task_number += 1
            done_tasks[time] += 1

    # rysowanie wykresów
    plt.plot(t, queue_size, "r.")
    plt.xlabel("Czas")
    plt.ylabel("Ilość zadań w kolejce")
    plt.title("Liczba zadań w kolejce - Wykres A-1")
    plt.yticks(range(MAX_TASKS+1))

    plt.figure()
    plt.plot(t, done_tasks, "b.")
    plt.xlabel("Czas")
    plt.ylabel("Ilość obsłużonych zadań")
    plt.yticks(range(MAX_TASKS+1))
    plt.title("Czas oczekiwania na wykonanie - Wykres A-2")

    plt.show()
