import random
import matplotlib.pyplot as plt

class Game(object):
    def __init__(self, kapital_a):
        self.kapital_a = kapital_a
        self.kapital_b = 100 - kapital_a
        self.number_of_rounds = 0
        self.a_win_prob = 0.5
        self.a_won = None

    def play(self):
        self.number_of_rounds += 1
        if random.uniform(0, 1) < self.a_win_prob:
            self.kapital_a += 1
            self.kapital_b -= 1
        else:
            self.kapital_a -= 1
            self.kapital_b += 1
        if self.kapital_a == 0:
            self.a_won = False
        elif self.kapital_a == 100:
            self.a_won = True


no_games = 100
kapitaly_a = range(1, 100)
ruin_probability_simulated = []
ruin_probability_analitycal = []

for kap in kapitaly_a:
    won_games = 0
    for i in range(no_games):
        g = Game(kap)
        while g.a_won is None:
            g.play()
        if g.a_won:
            won_games += 1

    ruin_probability_simulated.append( 1 - float(won_games) / no_games)

for kap in kapitaly_a:
    g = Game(kap)
    ruin_probability_analitycal.append(1 - float(g.kapital_a) / 100. )

plt.plot(kapitaly_a, ruin_probability_analitycal, color="green")
plt.plot(kapitaly_a, ruin_probability_simulated, color="red")
plt.show()