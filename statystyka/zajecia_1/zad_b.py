import random
import matplotlib.pyplot as plt

class Game(object):
    def __init__(self, a_win_prob):
        self.kapital_a = 50
        self.kapital_b = 50
        self.number_of_rounds = 0
        self.a_win_prob = a_win_prob
        self.a_won = None

    def play(self):
        self.number_of_rounds += 1
        if random.uniform(0, 1) < self.a_win_prob:
            self.kapital_a += 1
            self.kapital_b -= 1
        else:
            self.kapital_a -= 1
            self.kapital_b += 1
        if self.kapital_a == 0:
            self.a_won = False
        elif self.kapital_a == 100:
            self.a_won = True


no_games = 500
probabilities = [prob / 200. for prob in range(90, 110)]
ruin_probability_simulated = []
ruin_probability_analitycal = []

for probability in probabilities:
    won_games = 0
    for i in range(no_games):
        g = Game(probability)
        while g.a_won is None:
            g.play()
        if g.a_won:
            won_games += 1

    ruin_probability_simulated.append( 1 - float(won_games) / no_games)

for prob in probabilities:
    if prob == 0.5:
        ruin_probability_analitycal.append(0.5)
        continue
    r_n_l = (((1 - prob) / prob)**50 - ((1 - prob) / prob)**100)
    r_n_m = ( 1 - ((1 - prob) / prob)**100)
    r_n = r_n_l / r_n_m
    ruin_probability_analitycal.append(r_n)

plt.plot(probabilities, ruin_probability_analitycal, color="green")
plt.plot(probabilities, ruin_probability_simulated, color="red")
plt.show()