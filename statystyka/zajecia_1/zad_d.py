import random
import numpy
import matplotlib.pyplot as plt


class Game(object):
    def __init__(self, capital_a, capital_b, a_win_prob):
        self.capital_a = capital_a
        self.capital_b = capital_b
        self.a_win_prob = a_win_prob
        self.a_won = False
        self.b_won = False
        self.number_of_rounds = 0

        self.play_game()

    def play_game(self):
        while self._game_has_no_winner():
            self._play()
            self._check_for_winners()

    def _game_has_no_winner(self):
        return not (self.a_won or self.b_won)

    def _play(self):
        self.number_of_rounds += 1
        if random.uniform(0, 1) < self.a_win_prob:
            self.capital_a += 1
            self.capital_b -= 1
        else:
            self.capital_a -= 1
            self.capital_b += 1

    def _check_for_winners(self):
        if self.capital_a == 0:
            self.b_won = True
        elif self.capital_b == 0:
            self.a_won = True


def run_mode_1():
    a_win_probabilities = [0.2, 0.5, 0.8]
    no_of_games = 10000

    for a_win_probal in a_win_probabilities:
        number_of_rounds_to_finish_game = []
        for i in range(no_of_games):
            g = Game(capital_a=50, capital_b=50, a_win_prob=a_win_probal)
            number_of_rounds_to_finish_game.append(g.number_of_rounds)

        print("[A_WIN_PROB=%s] Average number of rounds = %s" % (a_win_probal,
                                                                 float(sum(number_of_rounds_to_finish_game))
                                                                 / len(number_of_rounds_to_finish_game)))

        plt.figure()
        weights = numpy.ones_like(number_of_rounds_to_finish_game) / float(len(number_of_rounds_to_finish_game))
        plt.hist(number_of_rounds_to_finish_game, 20,
                 label="A_WIN_PROB=%s" % a_win_probal, weights=weights)
    plt.show()


def run_mode_2():
    a_win_probabilities = [0.2, 0.8]
    tmp_list = []
    weights = []
    no_of_games_for_0_2_and_0_8 = 20000
    no_of_games_for_0_5 = 2000
    for a_win_probal in a_win_probabilities:
        number_of_rounds_to_finish_game = []
        for i in range(no_of_games_for_0_2_and_0_8):
            g = Game(capital_a=50, capital_b=50, a_win_prob=a_win_probal)
            number_of_rounds_to_finish_game.append(g.number_of_rounds)

        print("[A_WIN_PROB=%s] Average number of rounds = %s" % (a_win_probal,
                                                                 float(sum(number_of_rounds_to_finish_game))
                                                                 / len(number_of_rounds_to_finish_game)))
        tmp_list.append(number_of_rounds_to_finish_game)
        weights.append(numpy.ones_like(number_of_rounds_to_finish_game) / float(len(number_of_rounds_to_finish_game)))

    plt.hist(tmp_list, 20, label=["A_WIN_PROB=0.2", "A_WIN_PROB=0.8"], weights=weights)
    plt.legend()

    number_of_rounds_to_finish_game = []
    for i in range(no_of_games_for_0_5):
        g = Game(capital_a=50, capital_b=50, a_win_prob=0.5)
        number_of_rounds_to_finish_game.append(g.number_of_rounds)
    avg = (float(sum(number_of_rounds_to_finish_game))
           / len(number_of_rounds_to_finish_game))
    print("[A_WIN_PROB=0.5] Average number of rounds = %s" % avg)

    plt.figure()
    weights = numpy.ones_like(number_of_rounds_to_finish_game) / float(len(number_of_rounds_to_finish_game))
    plt.hist(number_of_rounds_to_finish_game, 20, label="A_WIN_PROB=0.5", weights=weights)
    plt.legend()

    plt.show()


if __name__ == "__main__":
    run_mode_2()
    # run_mode_1()
