import random
import numpy
import matplotlib.pyplot as plt


class Game(object):
    def __init__(self, capital_a, capital_b, a_win_prob):
        self.capital_a = capital_a
        self.capital_b = capital_b
        self.a_win_prob = a_win_prob
        self.a_won = False
        self.b_won = False
        self.number_of_rounds = 0
        self.a_win_counter = 0
        self.b_win_counter = 0


    def _game_has_no_winner(self):
        return not (self.a_won or self.b_won)

    def _play(self):
        self.number_of_rounds += 1
        if random.uniform(0, 1) < self.a_win_prob:
            self.capital_a += 1
            self.capital_b -= 1
            self.a_win_counter += 1
        else:
            self.capital_a -= 1
            self.capital_b += 1
            self.b_win_counter += 1


    def _check_for_winners(self):
        if self.capital_a == 0:
            self.b_won = True
        elif self.capital_b == 0:
            self.a_won = True


def run():
    a_win_probability = 0.2
    number_of_games = 4



    for i in range(number_of_games):
        g = Game(capital_a=50, capital_b=50, a_win_prob=a_win_probability)
        games_won_by_a = []
        while g._game_has_no_winner():
            g._play()
            g._check_for_winners()
            games_won_by_a.append(g.a_win_counter)

        game_numbers = range(1, len(games_won_by_a) + 1)
        plt.figure()
        plt.xlabel('Numer rozgrywki')
        plt.ylabel('Liczba wygranych')
        plt.yticks(range((g.a_win_counter + 1) * 2 / 2))
        plt.plot(game_numbers, games_won_by_a)#  , "-o")
        plt.title("Game #%s" % (i + 1))

    plt.show()

if __name__ == "__main__":
    run()

