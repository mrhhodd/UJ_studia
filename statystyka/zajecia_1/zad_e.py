import random
import numpy
import matplotlib.pyplot as plt


class Game(object):
    def __init__(self, capital_a, capital_b, a_win_prob):
        self.capital_a = capital_a
        self.capital_b = capital_b
        self.a_win_prob = a_win_prob
        self.a_won = False
        self.b_won = False
        self.number_of_rounds = 0

        self.play_game()

    def play_game(self):
        while self._game_has_no_winner():
            self._play()
            self._check_for_winners()

    def _game_has_no_winner(self):
        return not (self.a_won or self.b_won)

    def _play(self):
        self.number_of_rounds += 1
        if random.uniform(0, 1) < self.a_win_prob:
            self.capital_a += 1
            self.capital_b -= 1
        else:
            self.capital_a -= 1
            self.capital_b += 1

    def _check_for_winners(self):
        if self.capital_a == 0:
            self.b_won = True
        elif self.capital_b == 0:
            self.a_won = True


def run():
    a_win_probabilities = [i / 100. for i in range(1, 101)]
    number_of_games_per_probability = 1000
    max_game_lenghts = []
    for a_win_probability in a_win_probabilities:
        game_lengths = []
        for i in range(number_of_games_per_probability):
            g = Game(capital_a=50, capital_b=50, a_win_prob=a_win_probability)
            game_lengths.append(g.number_of_rounds)
        max_game_lenghts.append(max(game_lengths))

    plt.figure()
    plt.yscale('log')
    plt.xlabel('Probability of winning round by player A')
    plt.ylabel('Longest game in %s tries' % number_of_games_per_probability)
    plt.plot(a_win_probabilities, max_game_lenghts, 'rx')
    plt.show()


if __name__ == "__main__":
    run()
