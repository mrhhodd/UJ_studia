from __future__ import division
from pprint import pprint
import random

import math
import matplotlib.pyplot as plt


def get_random_poisonn_number(lambada):
    return - math.log(random.random()) / lambada


def run_e():
    list_of_runs = []
    no_of_runs = 10000
    t1 = []
    t20 = []
    t90 = []
    for i in range(no_of_runs):
        run = [get_random_poisonn_number(1)]
        while True:
            new_time = get_random_poisonn_number(1) + run[-1]
            run.append(new_time)
            if len(t1) == i and new_time > 1:
                t1.append(len(run))
            if len(t20) == i and new_time > 20:
                t20.append(len(run))
            if len(t90) == i and new_time > 90:
                t90.append(len(run))
            if new_time > 150:
                break
        list_of_runs.append(run)

    for t, values in zip([1, 20, 90], [t1, t20, t90]):
        xx = [i for i in range(t + 40)]
        plt.figure()
        hist_max = max(plt.hist(values)[0])
        yy = [math.pow(t, x) / math.factorial(x) * math.exp(-1 * t) for x in xx]
        scale = hist_max / max(yy)
        scaled_yy = [y * scale for y in yy]
        plt.plot(xx, scaled_yy)
    plt.show()


if __name__ == "__main__":
    run_e()
