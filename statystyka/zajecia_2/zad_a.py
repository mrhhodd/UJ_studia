import matplotlib.pyplot as plt


class MatrixTools(object):
    @staticmethod
    def multiply_matrix(matrix_1, matrix_2):
        size = len(matrix_2)
        result_matrix = [[0] * size, [0] * size, [0] * size]
        for i in range(size):
            for j in range(size):
                for k in range (size):
                    result_matrix[i][j] += matrix_1[i][k] * matrix_2[k][j]
                result_matrix[i][j] = round(result_matrix[i][j], 10)
        return result_matrix

    @staticmethod
    def substract_matrix(matrix_1, matrix_2):
        size = len(matrix_2)
        result_matrix = [[0] * size, [0] * size, [0] * size]
        for i in range(size):
            for j in range(size):
                result_matrix[i][j] = round(matrix_2[i][j] - matrix_1[i][j], 10)
        return result_matrix

    @staticmethod
    def get_max_abs_value(matrix):
        return max([max([abs(value) for value in row]) for row in matrix])

    @staticmethod
    def show_matrix(matrix):
        for i in range(len(matrix)):
            print(matrix[i])


def run_a():
    matrix_p = [
        [0.64, 0.32, 0.04],
        [0.40, 0.50, 0.10],
        [0.25, 0.50, 0.25],
    ]
    matrix_history = []
    matrix_history.append(matrix_p)

    while True:
        result_matrix = MatrixTools.multiply_matrix(matrix_history[-1], matrix_p)
        matrix_history.append(result_matrix)
        if MatrixTools.get_max_abs_value(
                MatrixTools.substract_matrix(matrix_history[-2], matrix_history[-1])) < 0.00001:
            break

    MatrixTools.show_matrix(result_matrix)
    for i in range(len(matrix_p[0])):
        for j in range(len(matrix_p)):
            values = [value for value in [matrix[i][j] for matrix in matrix_history]]
            plt.figure()
            plt.plot(values)
            plt.axhline(values[-1], linestyle="dotted", color='r')
            plt.xlabel("n")
            plt.title("[P^n]%s%s" % (i, j))
    plt.show()

if __name__ == "__main__":
    run_a()