from pprint import pprint
import random
import matplotlib.pyplot as plt


class MatrixTools(object):
    @staticmethod
    def multiply_matrix(matrix_1, matrix_2):
        size = len(matrix_2)
        result_matrix = [[0] * size, [0] * size, [0] * size]
        for i in range(size):
            for j in range(size):
                for k in range(size):
                    result_matrix[i][j] += matrix_1[i][k] * matrix_2[k][j]
                result_matrix[i][j] = round(result_matrix[i][j], 10)
        return result_matrix

    @staticmethod
    def substract_matrix(matrix_1, matrix_2):
        size = len(matrix_2)
        result_matrix = [[0] * size, [0] * size, [0] * size]
        for i in range(size):
            for j in range(size):
                result_matrix[i][j] = round(matrix_2[i][j] - matrix_1[i][j], 10)
        return result_matrix

    @staticmethod
    def get_max_abs_value(matrix):
        return max([max([abs(value) for value in row]) for row in matrix])

    @staticmethod
    def show_matrix(matrix):
        for i in range(len(matrix)):
            print(matrix[i])


def run_a():
    matrix_p = [
        [0.64, 0.32, 0.04],
        [0.40, 0.50, 0.10],
        [0.25, 0.50, 0.25],
    ]
    matrix_history = []
    matrix_history.append(matrix_p)

    while True:
        result_matrix = MatrixTools.multiply_matrix(matrix_history[-1], matrix_p)
        matrix_history.append(result_matrix)
        if MatrixTools.get_max_abs_value(
                MatrixTools.substract_matrix(matrix_history[-2], matrix_history[-1])) < 0.00001:
            break
    return matrix_history


def run_b():
    starting_nodes = [0, 1, 2]
    matrix_p = [
        [0.64, 0.32, 0.04],
        [0.40, 0.50, 0.10],
        [0.25, 0.50, 0.25],
    ]
    history_matrix = [[0] * 3, [0] * 3, [0] * 3]
    number_of_state_changes = 10000

    for starting_node in starting_nodes:
        new_state = starting_node
        for i in range(number_of_state_changes):
            history_matrix[starting_node][new_state] += 1
            random_number = random.random()
            if random_number < matrix_p[new_state][0]:
                new_state = 0
            elif random_number < matrix_p[new_state][0] + matrix_p[new_state][1]:
                new_state = 1
            else:
                new_state = 2

    history_matrix = [float(value) / number_of_state_changes for row in history_matrix for value in row]
    return history_matrix


if __name__ == "__main__":
    calculated_matrix = run_a()[-1]
    simulated_matrix = run_b()

    print("Calculated Matrix:")
    print("[P]^N:")
    pprint(calculated_matrix)
    print("\nSimulated Matrix:")
    print("For x0=0:")
    pprint(simulated_matrix[:3])
    print("For x0=1:")
    pprint(simulated_matrix[3:6])
    print("For x0=2:")
    pprint(simulated_matrix[6:])
