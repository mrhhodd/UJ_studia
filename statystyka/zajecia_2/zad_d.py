from pprint import pprint
import random

import matplotlib.pyplot as plt


class MatrixTools(object):
    @staticmethod
    def multiply_matrix(matrix_1, matrix_2):
        size = len(matrix_2)
        result_matrix = [[0] * size, [0] * size, [0] * size]
        for i in range(size):
            for j in range(size):
                for k in range(size):
                    result_matrix[i][j] += matrix_1[i][k] * matrix_2[k][j]
                result_matrix[i][j] = round(result_matrix[i][j], 10)
        return result_matrix

    @staticmethod
    def substract_matrix(matrix_1, matrix_2):
        size = len(matrix_2)
        result_matrix = [[0] * size, [0] * size, [0] * size]
        for i in range(size):
            for j in range(size):
                result_matrix[i][j] = round(matrix_2[i][j] - matrix_1[i][j], 10)
        return result_matrix

    @staticmethod
    def get_max_abs_value(matrix):
        return max([max([abs(value) for value in row]) for row in matrix])

    @staticmethod
    def show_matrix(matrix):
        for i in range(len(matrix)):
            print(matrix[i])


def run_c():
    no_of_users = 100
    no_of_steps = 10000
    log_in_prob = 0.2
    initial_no_of_logged_users = 0

    state_history = [0] * (no_of_users + 1)
    logged_users_history = []
    no_of_logged_users = initial_no_of_logged_users
    aggregated_state_history = []
    for i in range(no_of_steps):
        log_out_prob = 0.9 - (0.008 * no_of_logged_users)
        logged_users_history.append(no_of_logged_users)
        state_history[no_of_logged_users] += 1
        aggregated_state_history.append([step/float(i + 1) for step in state_history])
        for i in range(no_of_users - logged_users_history[-1]):
            if random.random() < log_in_prob:
                no_of_logged_users += 1
        for i in range(logged_users_history[-1]):
            if random.random() < log_out_prob:
                no_of_logged_users -= 1

    return aggregated_state_history


if __name__ == "__main__":
    printed_values = [11, 15, 19, 24, 35, 39]
    aggregated_state_history = run_c()
    no_of_states = len(aggregated_state_history[-1])

    for i in printed_values:
        values = [state[i] for state in aggregated_state_history]
        plt.figure()
        plt.plot(values)
        plt.axhline(values[-1], linestyle="dotted", color='r')
        plt.xlabel("N")
        plt.title("exp_PI %s" % i)
    plt.figure()
    plt.bar(range(no_of_states), aggregated_state_history[-1])
    plt.xlabel("Number of users logged")
    plt.ylabel("State probability")
    plt.show()

    pprint(zip(range(no_of_states), aggregated_state_history[-1]))